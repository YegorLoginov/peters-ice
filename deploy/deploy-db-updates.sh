#!/bin/bash

# Exit on error
set -e

SERVER_WEBROOT=$1

# Check we have all the variables we need
if [ -z ${SERVER_WEBROOT+x} ]; then
    echo "ERROR: Config not set."
    exit 1
fi

# Move into the webroot
cd "${SERVER_WEBROOT}"

# Backup database
# wp db export <path>

# Enable/disable plugins
wp adrenalin sync_plugins

# Import ACF config
wp adrenalin sync_acf

# Update W3 Total Cache settings
wp w3-total-cache import ../config/system/w3-total-cache.json

# Import Wordfence config
wp adrenalin sync_wordfence
wp adrenalin update_wordfence_files

# Import other config
# wp option update <key> <val>

# Update database (eg. plugin updates)
# ???

# Clear cache
wp adrenalin clear_all
ln -nfs ../config/.htaccess .htaccess
# Update text
# wp search-replace --dry-run <old> <new>