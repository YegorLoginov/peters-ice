#!/bin/bash

# Exit on error
set -e

HASH=$1

git status --porcelain

if [[ `git status --porcelain` ]]; then
    # Changes
    git add . 
    git commit -m "deploy and sync bitbucket commit ${HASH}"
    git push origin master
else
  # No changes
  echo "nothing changed";
fi