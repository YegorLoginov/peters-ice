#!/bin/bash

# Exit on error
set -e

# The absolute path of the directory that the repository is cloned into.
BITBUCKET_CLONE_DIR=$1

# Server details
SERVER_USER_NAME=$2
SERVER_HOST=$3
SERVER_DOCROOT=$4

# Check we have all the variables we need
if [ -z ${BITBUCKET_CLONE_DIR+x} ] || [ -z ${SERVER_USER_NAME+x} ] || [ -z ${SERVER_HOST+x} ] || [ -z ${SERVER_DOCROOT+x} ]; then
    echo "ERROR: Config not set."
    exit 1
fi

echo "Checking the webroot exists on the server..."
ssh "${SERVER_USER_NAME}@${SERVER_HOST}" "[ -d ${SERVER_DOCROOT} ]"

echo "Syncing files..."
rsync -rv --delete --checksum \
  --exclude=".git/" \
  --exclude="config/wp-config.php" \
  --exclude="config/.htaccess" \
  --exclude="dbs/" \
  --exclude="deploy/" \
  --exclude="fed/" \
  --exclude="web/app/cache/" \
  --exclude="web/assets/" \
  --exclude="web/license.txt" \
  --exclude="web/readme.html" \
  --exclude="web/wordfence-waf.php" \
  --exclude=".env" \
  --exclude=".env.sample" \
  --exclude=".gitignore" \
  --exclude="bitbucket-pipelines.yml" \
  --exclude="composer.json" \
  --exclude="composer.lock" \
  --exclude="docker-compose.yml" \
  --exclude="Dockerfile" \
  --exclude="makefile" \
  --exclude="README.MD" \
  "${BITBUCKET_CLONE_DIR}/" "${SERVER_USER_NAME}@${SERVER_HOST}:${SERVER_DOCROOT}"
