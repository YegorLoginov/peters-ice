# Adrenalin Media FED Boilerplate

The FED boilerplate is a base for Adrenalin Media front end projects. The boilerplate is built on webpack and is based on the official Create React App generator (https://github.com/facebook/create-react-app).

The aim of this boilerplate is to standardise the FED toolset, project configuration and structure.

# Core Features

-   [ES6/Babel transpiling, compilation and linting](#markdown-header-javascript)
-   [Util.js helper methods](#markdown-header-utiljs)
-   [SASS (SCSS) Compilation and linting](#markdown-header-sass)
-   [TWIG templating compilation](#markdown-header-twig)
-   [Iconfont (SVG icons)](#markdown-header-iconfont)
-   [Image compression](#markdown-header-image-compression)
-   [Favicon/Metadata generation](#markdown-header-favicon-generation)

# Getting Started

If starting a new repository from the boilerplate you can fork directly and create a new repo on the fly by clicking 'Fork this repository' in the create menu (+ button) in bitbucket.

If you need to add this boilerplate to an existing repository you can download the repo as a zip and extract the contents to a suitable folder in the repo.

Once checked out or downloaded, `cd` to the root directory with the package.json file and install the node dependencies with yarn:

    yarn install

Once the modules have successfully installed you can fire up a local development server with:

    yarn start

### Building for production

The boilerplate includes a build task to prep the site for production (html site/app and export to cms directory).

Building will concatenate and minify the Javascript and CSS and deploy them to the static folder in `/build`. It will also run an image compression task on all images within the public/build directories.

The build task will also copy the `/build/static`, `/build/assets` and `/build/meta` directories to the `cmsBuildDirectory` folder path found in the package.json.

To run the build task run -

    yarn build

The `/build` directory is ignored from git and only visible in the project after running the build script.

# Folder Structure

High level configuration and dependencies for the project are managed via the `package.json` file in the root directory. The project's name, version, build scripts (tasks) and module dependencies are managed here.

## Scripts folder

Custom node scripts (tasks) are located in the `/scripts/` directory. This includes custom scripts for the Twig.js compilation, Iconfont generation, favicons/meta etc. These tasks are referenced in the package.json file for each of the main build scripts, i.e -

```json
"scripts": {
        "start": "npm-run-all -p iconfont-watch assets-watch twig-watch start-script",
        "build": "npm-run-all -s iconfont assets twig build-script build-cms",
        "start-script": "node scripts/start.js",
        "build-script": "node scripts/build.js",
        "test": "node scripts/test.js",
        "twig": "node scripts/twig.js",
        "twig-watch": "node scripts/twig.js -w",
        "iconfont": "node scripts/iconfont.js",
        "iconfont-watch": "node scripts/iconfont.js -w",
        "assets": "node scripts/assets.js",
        "assets-watch": "node scripts/assets.js -w",
        "favicons": "node scripts/favicons.js",
        "build-cms": "node scripts/cms.js",
        "flow-start": "flow start",
        "flow-stop": "flow stop",
        "flow-status": "flow status",
        "flow-coverage": "flow coverage"
    },
```

## Src folder

Most front end development should happen in the `/src/` directory. The main entry point for JS and CSS in the site is the `/src/index.js` file. Inside this index.js file is the reference for the css include along with JS module includes for the root of the application.

## Public folder

The `/public` folder contains the static files that will be used in by the site html files, images, fonts, manifest files etc.

HTML files in the `/public` directory will be read by the local development server (http://localhost:3000/home.html) and deployed into the `/build` folder on `yarn build`.

In cases where Twig.js is being used to generate html files, outputted html will automatically be generated and built to the `/public` directory so only the .twig files should be edited.

## Build folder

As described in the [building for production process](#markdown-header-building-for-production) the build folder is an outputted production ready folder containing everything the site needs to be deployed to a server. This folder is suitable for deployment of flat html sites or single page applications (React apps).

# Javascript

All front end Javascript development should be done within the `/src/js/` directory. ES6 should be used when writing JavaScript within the boilerplate, however standard Javascript (ES5) will still work. ES6 modules should be used to develop and import specific classes and components into files.

An example of importing a React component into the `/src/index.js` file via es6 modules -

```js
import App from "./js/containers/App";
```

Webpack will automatically handle the bundling and minification of JS files as long as they are imported through the `/src/index.js` entry point . Configuration for webpack is found within the the /config/webpack.config.dev.js and /config/webpack.config.prod.js files.

### ESLint

ESLint comes preconfigured for all JS files within the boilerplate to ensure coding standards and reduce errors. The babel `react-app` (https://www.npmjs.com/package/babel-preset-react-app) preset is used and extended upon as the default linting setup. The preset and rules are managed within the `package.json` using the `babel.presets` array.

# Util.js

Included in this boilerpolate, there is a set of helper functions found in `/src/js/util/index.js`. All of the utils outlined below can be imported using:

```js
import Utils from "/src/js/util";
```

Alternatively, you can import only the specific helper methods you will be using. For example:

```js
import { pubsub, cookie } from "/src/js/util";
```

---

### Utils.createUniqueID

Create unique ID is a function that will return a unique identifier as a string. It is guaranteed to be unique within the scope of the document it is called from.

```js
const uid = createUniqueID();
```

---

### Utils.pubsub

`pubsub` is a set of methods used to enable effective app-wide communication. In a nutshell, pubsub works by allowing you to publish a topic and having any other part of your application subscribe to it. This allows you to decouple events from actions in your code.

**Publishing a topic**

To publish a topic, call `publish()` on the pubsub object. `publish()` takes 2 parameters:

1. The name of the topic as a string
2. Any data you would like to pass to subscriptions

**Subscribing to a topic**

To subscribe to a topic, call `subscribe()` on the pubsub object. `subscribe()` takes 2 parameters:

1. A string with the name of the topic to subscribe to
2. A callback function that will be called each time `publish()` is run for that topic. This callback is passed a single data parameter that is passed from `publish()`

_Note: you can have multiple subscriptions to a single topic._

**Remove a subscription**

`subscribe()` returns an object containing a single method: `remove()`. Calling this will remove the subscription.

**Usage**

```js
import { pubsub } from "/src/js/util";

// Subscribe an action to a topic
const subscription = pubsub.subscribe("some-topic", (topic, data) => {
    console.log('This is running in response to "some-topic" being published');
});

// Publish a topic
pubsub.publish("some-topic", "data passed to subscriptions");

// If/When finished with a subscription, you can unsubscribe using
subscription.remove();
```

---

### Utils.dom

`dom` is a collection of methods to do with working with and manipulating DOM elements.

**Adding vendor prefixed CSS using JS**

To add a vendor prefixed CSS property to a dom element, call `prefix()` on the `dom` object. `prefix()` Takes 3 parameters:

1. The DOM Node element you would like to add the CSS property to
2. The CSS property name
3. The CSS property value

Usage:

```js
import { dom } from "/src/js/util";

const element = document.querySelector(".selector");

dom.prefix(element, "backdrop-filter", "blur(10px)");

/*

This results in the following CSS properties being applied:

-webkit-backdrop-filter: blur(10px);
-moz-backdrop-filter: blur(10px);
-ms-backdrop-filter: blur(10px);
-o-backdrop-filter: blur(10px);
backdrop-filter: blur(10px);

*/
```

**Listening for transition/animation end**

Using one of:

-   `dom.onTransitionEnd()` - To run a callback after a transition finishes on an element (runs once)
-   `dom.onTransitionEndAlways()` - To run a callback after a transition finishes on an element
-   `dom.onAnimationEnd()` - To run a callback after an animation finishes on an element (runs once)
-   `dom.onAnimationEndAlways()` - To run a callback after an animation finishes on an element

Alllows you to run a function after a transition or animation ends on the passed in element.

Each of the above methods take 2 parameters:

1. The DOM Node element you would like to listen for animation/transition finshing on
2. A callback function to run once animation/transition is complete

Usage:

```js
import { dom } from "/src/js/util";

const element = document.querySelector(".selector");

// Add a class to trigger a transition
element.classList.add("clicked");

// Remove the class once the transition has finished
dom.onTransitionEnd(element, () => {
    element.classList.remove("clicked");
});
```

---

### Utils.cookie

`Utils.cookie` is a collection of methods for easily working with cookies.

**Adding a new cookie / Replacing an existing cookie**

To replace or a add a new cookie, call `cookie.set()` - this method takes 3 parameters:

1. The Name of the cookie
2. The Value of the cookie
3. (Optional) Number of days until the cookie expires. Leaving this blank will cause the cookie to never expire.

**Getting an existing cookie**

To retrieve an existing cookie, call `cookie.get()` - this method takes 1 parameter:

1. The name of the cookie

`cookie.get()` returns the value of the cookie, or `null` of it is not found.

Usage:

```js
import { cookie } from "/src/js/util";

cookie.set("test-cookie", "some value");

cookie.get("test-cookie"); // returns 'some value'
```

---

### Utils.ratio

`Utils.ratio` is a collection of methods for working with ratios of DOM nodes.

**Generating a height based on a ratio of an elements width**

To generate a height value based on a ratio of an elements width, call `ratio.getRatio()` - this method takes 2 parameters:

1. A ratio expressed as a denominator
2. (Optional) A DOM Node element. If left blank, the viewport width will be used to determine the ratio

Usage:

```js
import { ratio } from "/src/js/util";

const element = document.querySelector(".selector");

// Set the element width to 100px
element.style.width = "100px";

// Generate a height based on a 2:1 ratio
ratio.getRatio(2, element); // In this case, would return 50
```

# SASS

SASS/SCSS compilation and folder structure comes pre-reconfigured for all sass/scss files in the `/src/scss/` directory.

The entry point for importing scss/sass/css files is `/src/scss/index.scss`. This file will compile and output to `/src/scss/index.css` which is then imported into the sites main entry point - `/src/index.js`

### Stylelint

CSS/SASS stylelint comes preconfigured for all `.scss` files within the boilerplate to ensure coding standards and reduce errors. The ruleset for the stylelint is found in the `/.styleintrc` file whilst ignored files should be added to the `/.stylelintignore` file.

# Twig

Twig is a simple and easy to read templating language used to both generate static html flats and build templates for .twig supporting CMSs.

The boiler plate includes a script for a javascript Twig compiler (https://github.com/twigjs/twig.js/wiki) which will automatically interpret .twig files in the `/src/twig/` directory.

Full documentation for Twig can be found at https://twig.symfony.com/doc/2.x/ with https://twig.symfony.com/doc/2.x/templates.html being the most relevant doc for the sake of front end templating.

Twig syntax highlighting is available for most IDEs to help with development:

-   _Atom_ via the [PHP-twig for atom](https://github.com/reesef/php-twig)
-   _Visual Studio Code_ via the [Twig pack](https://marketplace.visualstudio.com/items?itemName=bajdzis.vscode-twig-pack)
-   _Sublime Text_ via the [Twig bundle](https://github.com/Anomareh/PHP-Twig.tmbundle)
-   _Notepad++_ via the [Notepad++ Twig Highlighter](https://github.com/Banane9/notepadplusplus-twig)

## Twig structure

By default all twig files in the root twig directory `/src/twig` will be compiled and exported as html files to the `/public` directory where they will be served by the local dev server from. These same compiled html files in the `/public` directory are then prepped for production and moved to the `/build` folder on running the build task outlined in the [build process](#markdown-header-building-for-production).

### Atomic design

The twig file breakup is based around the concept of atomic design, which is essentially breaking down templates and pages into components with varying levels of granularity.

There are a few resources available around the web to go into more depth on the concepts involved in atomic design but for a bit of an overview you can refer to this post -  
http://bradfrost.com/blog/post/atomic-web-design/

### /src/twig/

Files at the root of the twig folder should be viewed as representations of pages (cms output) and ideally only represent data and not templates themselves.

An example of this can be seen in the `/src/twig/twig.twig` file which extends the `default.twig` template in the `/src/twig/templates/` directory -

```twig
{% extends "./templates/default.twig" %}
```

This file also contains an example of a data variable mockup which is a representation of page content, below is an example of the data variable with `latestNews` being an array of article cards -

```twig
{% set data = {
    latestNews: [
        {
            'title': 'First Card',
            'link': {
                'text': 'This is link text!',
                'href': '/'
            }
        },
        {
            'title': 'Second Card',
            'button': {
                'text': 'Next',
                'className': 'button--next'
            }
        },
        {
            'title': 'Third Card',
            'id': '34dse33'
        }
    ]
} %}
```

The view block is the area of the twig.twig page that will be extended into the default.twig template. Below you can see an include to a latest news component is what is going to be extended into the default.twig template -

```twig
{% block view %}

    {% include './organisms/latest-news.twig' %}

{% endblock %}
```

Inside the default.twig template we can see the view block is positioned in between a nav and footer include -

```twig
{# Master Template with head #}
{% extends twigDir ~ "templates/master.twig" %}

{# 'page' block is included in body of master.twig #}
{% block page %}

    {% include twigDir ~ 'organisms/nav.twig' %}

    {% block view %}{% endblock %}

    {% include twigDir ~ 'organisms/footer.twig' %}

{% endblock %}
```

The default template is then extended into a master.twig template which contains the highest level html including the html, head and body tags -

```twig
<body>
    <div class="page page--{{ bodyClass }}">
        {% block page %}{% endblock %}
    </div>
</body>
```

### /src/twig/constants/global.twig

The global.twig file represents data that is site wide or global (social links, site domains, global ctas etc)

### /src/twig/templates/

The templates directory of the twig folder should contain all page/content type templates such as news detail page template, a blog article template as well as the higher level templates (default.twig, master.twig).

### /src/twig/organisms/

Organisms are the next highest level of template and represent the highest level of component. An example of this would be a full width pagination component (article cards and pagination) or a full width carousel or video player.

Here is an example of the `/src/twig/organisms/latest-news.twig` organism -

```twig
<div class="latest-news">
    <div class="container-fluid center-xs">
        <h2>
            Latest News
        </h2>
        <div class="row">
            {% for card in latestNews %}
                <div class="col-xs-12 col-sm-4">
                    {% include twigDir ~ 'molecules/card.twig' with card  %}
                </div>
            {% endfor %}
        </div>
    </div>
</div>
```

### /src/twig/molecules/

Molecules fall as the next level of component after organisms and organisms may contain multiple molecules. An example of a molecule could be a an article card with a poster image, title, description and cta. This molecule could then make up one of the multiple article cards in a article pagination organism.

Here is an example of the `/src/twig/molecules/card.twig` molecule -

```twig
<div class="card">
    {{ title }}

    {% if link %}
    <a href="{{ link.href }}" class="card-link">
        {{ link.text }}
    </a>
    {% endif %}

    {% if id %}
    <div class="card-id">
        {{ id }}
    </div>
    {% endif %}

    <p>
        {% include twigDir ~ 'atoms/button.twig' with button only %}
    </p>
</div>
```

### /src/twig/atoms/

Atoms are essentially the lowest level of component and will belong to both molecules and organisms. Examples of atoms could include buttons, publish details (author, date, category), image tiles etc

An example of the `/src/twig/atoms/button.twig` atom -

```twig
<button class="button {{ className }}">
    {{ text ?: 'Submit' }}
    <span class="icon icon-chevron-right"></span>
</button>
```

# Iconfont

An svg Iconfont task is setup to watch and compile svg icons from the `/public/assets/icons/` directory to an Iconfont as part of the `yarn start` process.

The font is output at the `/public/assets/fonts/` folder and automatically deployed to the `/build` and cms build directories on running the [build process](building-production).

The Iconfont css is already included via the `/src/scss/index.scss` file.

For more configuration and editing of the Iconfont script please check the `/scripts/iconfont.js` file.

# Image compression

As outlined in the [build process](building-production), building will run an image compression task on all images within the public/build directories.

Image compression is taken care of by the [imagemin-webpack-plugin](https://www.npmjs.com/package/imagemin-webpack-plugin) webpack plugin.

This plugin is essentially a wrapper around [Imagemin](https://github.com/imagemin/imagemin) and thus most imagemin properties and setup and can be configured. Configuration is handled in the `/config/webpack.config.prod.js` file under the `ImageminPlugin` plugin section.

# Favicon generation

A script is included to generate favicons, webapp icons, manifest and meta tags. Configuration for the favicons script can be found at `/scripts/favicons.js`.

By default the script will target the image at `/public/assets/images/favicon.png` to generate the favicon and all web app icons. A large res source image should be used here to cater to large app and splash screen icons.

This task must be run manually each time you wish to update the favicon or meta fields with the following command -

    yarn favicons

This will not only generate the favicon, icons and the manifest.json in the `/public/meta/` folder but also create a favicons.twig partial in the `/src/twig/templates/` folder. The favicons.twig partial contains all the generated meta tags and fields relating to the favicon, icons and manifest references allowing you to then include it in a high level twig template such as the master.twig -

```twig
<head>
    <base href="/" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{ documentDescription }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

    {% include twigDir ~ 'templates/favicons.twig' %}

    <title>
        {{ documentTitle }}
    </title>
</head>
```
