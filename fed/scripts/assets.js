const pjson = require("../package.json");
const fs = require("fs");
const chalk = require("chalk");
const rimraf = require("rimraf");
const ncp = require("ncp");

// Assets should be managed and maintained within the /src/assets/ directory to allow for importing through JS/SASS
// This script clones the /src/assets/ folder to /public/assets/ to allow for references within html files

const copyAssetsToPublic = () => {
    rimraf("public/assets", () => {
        ncp(pjson.assetsDirectory, "public/assets", function(err) {
            if (err) {
                return console.error(err);
            }
        });
    });
};

// Watch kit directory for file changes and compile
if (process.argv.indexOf("-w") !== -1) {
    fs.watch(
        pjson.assetsDirectory,
        {
            recursive: true
        },
        copyAssetsToPublic
    );
    console.log(
        chalk.blue(
            `🔍 Now watching ${pjson.assetsDirectory} for asset file changes.`
        )
    );
}

copyAssetsToPublic();
