const pjson = require("../package.json");
const assetManifest = require("../build/asset-manifest.json");
const fs = require("fs");
const chalk = require("chalk");
const ncp = require("ncp");
const replaceStream = require("replacestream");
const rimraf = require("rimraf");
const readDirectory = require("./read-directory");
ncp.limit = 16;

const buildToCMS = () => {
    console.log(
        chalk.blue(
            `Deploying build files and directories to ${pjson.cmsBuildDirectory}...`
        )
    );

    const getVersionFromFilepath = filePath => {
        const pathSections = filePath.split("/");
        const filename = pathSections[pathSections.length - 1];
        return filename.split(".")[1];
    };
    const cssVersion = getVersionFromFilepath(assetManifest["main.css"]);
    const jsVersion = getVersionFromFilepath(assetManifest["main.js"]);

    // Copy Static (css, js)
    const _buildCSSJS = async () => {
        //const dest = pjson.cmsBuildDirectory.replace("../web", "");
        //const base = __dirname.split("/fed/scripts")[0];

        // delete and create new cms/static directory
        rimraf(`${pjson.cmsBuildDirectory}/static/`, () => {
            fs.mkdir(
                `${pjson.cmsBuildDirectory}/static/`,
                { recursive: true },
                err => {
                    // Move 'built' files to cms output directory
                    ncp(
                        "./build/static/",
                        pjson.cmsBuildDirectory + "/static/",
                        {},
                        function(err) {
                            if (err) {
                                return console.error(chalk.red("❌ " + err));
                            }

                            // Create Version timestamp
                            const dt = new Date();
                            const yy = dt.getFullYear();
                            const mm = dt.getMonth() + 1;
                            const dd = dt.getDate();
                            const datedVersion =
                                yy.toString() +
                                (mm < 10 ? "0" + mm : mm).toString() +
                                dd.toString() +
                                "." +
                                dt.getTime();

                            // remove version numbers from outputted files so CMS can still find them
                            // CMS can then handle file versioning
                            readDirectory(
                                `${pjson.cmsBuildDirectory}/static/`,
                                false,
                                (filePath, fileName) => {
                                    // If it's a js/css/map file - replace all occurences of the JS/CSS version with empty string
                                    if (
                                        fileName.includes(".js") ||
                                        fileName.includes(".css")
                                    ) {
                                        let data = fs
                                            .readFileSync(filePath)
                                            .toString();
                                        data = data.replace(
                                            new RegExp(
                                                `(${cssVersion}|${jsVersion})\.`,
                                                "g"
                                            ),
                                            ""
                                        );
                                        fs.writeFileSync(
                                            filePath,
                                            Buffer.from(data)
                                        );
                                        console.log(
                                            filePath,
                                            "Modified source map files!"
                                        );
                                    }

                                    const versionRemovedFileName = filePath.replace(
                                        new RegExp(
                                            `(${jsVersion}|${cssVersion}).`
                                        ),
                                        filePath.includes(".map")
                                            ? ""
                                            : datedVersion + "."
                                    );
                                    if (versionRemovedFileName !== filePath) {
                                        fs.renameSync(
                                            filePath,
                                            versionRemovedFileName
                                        );
                                        console.log(
                                            chalk.green(
                                                `✅ ${fileName} was copied and renamed to ${versionRemovedFileName}.`
                                            )
                                        );
                                    }
                                },
                                true
                            );

                            // TODO - need to refactor for wordpress/php builds
                            // Create Build File
                            fs.writeFileSync(
                                `${pjson.cmsBuildDirectory}/version.php`,
                                `<?php define('CODE_VER', '${datedVersion}'); ?>`
                            );
                        }
                    );
                }
            );
        });
    };
    _buildCSSJS();

    // Copy index.html to master.php template
    ncp(
        "./public/index.html",
        pjson.cmsBuildDirectory + "/templates/master.php",
        {
            // transform: (read, write) => {
            //     read.pipe(
            //         replaceStream(
            //             "/meta/",
            //             `<?php echo get_template_directory_uri(); ?>/meta/`
            //         )
            //     ).pipe(write);
            // },
        },
        function(err) {
            if (err) {
                return console.error(err);
            }
        }
    );

    // Copy assets (images, fonts)
    ncp("build/assets/", pjson.cmsBuildDirectory + "/assets", function(err) {
        if (err) {
            return console.error(chalk.red("❌ " + err));
        }
        console.log(
            chalk.green(
                `✅ build/assets/ was copied to ${pjson.cmsBuildDirectory}/assets.`
            )
        );
    });

    // // Copy Meta (meta/manifest files and favicons)
    ncp("build/meta/", pjson.cmsBuildDirectory + "/meta", function(err) {
        if (err) {
            return console.error(chalk.red("❌ " + err));
        }
        console.log(
            chalk.green(
                `✅ build/meta/ was copied to ${pjson.cmsBuildDirectory}/meta.`
            )
        );
    });

    // TODO - need to refactor for wordpress/php builds
    // Copy index.html template to master.php template
    // const _buildTemplate = async () => {
    //  const dest = pjson.cmsBuildDirectory.replace('../web', '');
    //  ncp('./public/index.html', pjson.cmsBuildDirectory + '/templates/master.php', {
    //      transform: (read, write) => {
    //          read.pipe(replaceStream('/meta/', `${dest}meta/`))
    //              .pipe(write);
    //      }
    //  }, function (err) {
    //      if (err) {
    //          return console.error(err);
    //      }
    //  });
    // }
    // _buildTemplate();
};

if (pjson.cmsBuildDirectory) {
    buildToCMS();
}
