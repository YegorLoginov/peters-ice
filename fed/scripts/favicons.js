const _ = require("underscore"),
    pjson = require("../package.json"),
    fs = require("fs"),
    chalk = require("chalk"),
    favicons = require("favicons"),
    source = pjson.favicons.sourceIcon, // Source image(s). `string`, `buffer` or array of `string`
    faviconsDir = "public" + pjson.favicons.path,
    configuration = {
        path: pjson.favicons.path, // Path for overriding default icons path. `string`
        appName: pjson.displayName, // Your application's name. `string`
        appDescription: null, // Your application's description. `string`
        developerName: "Adrenalin Media", // Your (or your developer's) name. `string`
        developerURL: null, // Your (or your developer's) URL. `string`
        dir: "auto", // Primary text direction for name, short_name, and description
        lang: "en-US", // Primary language for name and short_name
        background: "#ffffff", // Background colour for flattened icons. `string`
        theme_color: "#f43100", // Theme color user for example in Android's task switcher. `string`
        display: "standalone", // Preferred display mode: "fullscreen", "standalone", "minimal-ui" or "browser". `string`
        orientation: "any", // Default orientation: "any", "natural", "portrait" or "landscape". `string`
        start_url: pjson.homepage, // Start URL when launching the application from a device. `string`
        version: pjson.version, // Your application's version string. `string`
        logging: false, // Print logs to console? `boolean`
        icons: {
            // Platform Options:
            // - offset - offset in percentage
            // - shadow - drop shadow for Android icons, available online only
            // - background:
            //   * false - use default
            //   * true - force use default, e.g. set background for Android icons
            //   * color - set background for the specified icons
            //
            android: true, // Create Android homescreen icon. `boolean` or `{ offset, background, shadow }`
            appleIcon: true, // Create Apple touch icons. `boolean` or `{ offset, background }`
            appleStartup: true, // Create Apple startup images. `boolean` or `{ offset, background }`
            coast: {
                offset: 25,
            }, // Create Opera Coast icon with offset 25%. `boolean` or `{ offset, background }`
            favicons: true, // Create regular favicons. `boolean`
            firefox: true, // Create Firefox OS icons. `boolean` or `{ offset, background }`
            windows: true, // Create Windows 8 tile icons. `boolean` or `{ background }`
            yandex: true, // Create Yandex browser icon. `boolean` or `{ background }`
        },
    },
    callback = function(error, response) {
        if (error) {
            return console.log(chalk.red("❌" + error.message)); // Error description e.g. "An unknown error has occurred"
        }

        // create output directory if dosnt exist
        if (!fs.existsSync(faviconsDir)) {
            fs.mkdirSync(faviconsDir);
        }

        // save each of the images to the output directory
        for (let image of response.images) {
            fs.writeFile(faviconsDir + image.name, image.contents, function(
                err
            ) {
                if (err) {
                    return console.log(chalk.red("❌" + err));
                }
            });
        }

        // save each of the files to the output directory
        for (let file of response.files) {
            let existingJson;
            let newJson;
            let extendedJson;

            // extend existing json files (prevents overwriting of keys)
            if (file.name.indexOf(".json") >= 0) {
                try {
                    existingJson = fs.readFileSync(
                        faviconsDir + file.name,
                        "utf-8"
                    );
                    if (existingJson) {
                        newJson = _.extend(
                            JSON.parse(existingJson),
                            JSON.parse(file.contents)
                        );
                        extendedJson = JSON.stringify(newJson, null, 2);
                    }
                } catch (err) {
                    // File was not found
                }
            }

            let fileContents = extendedJson ? extendedJson : file.contents;

            fs.writeFile(faviconsDir + file.name, fileContents, function(err) {
                if (err) {
                    console.log(chalk.red("❌" + err));
                }
            });
        }

        // create html string of all html tags
        let htmlTags = "";
        for (let html of response.html) {
            htmlTags += html + "\r\n";
        }

        // Add WP theme path
        htmlTags = htmlTags.replace(
            /\/meta\//g,
            "<?php echo get_template_directory_uri(); ?>/meta/"
        );

        // write meta tags to twig partial (can be included in master.twig)
        fs.writeFile(pjson.favicons.outputMetaFile, htmlTags, function(err) {
            if (err) {
                console.log(chalk.red("❌" + err));
            } else {
                console.log(
                    chalk.green(
                        `✅ Favicons, Icons and Web App meta data generated.`
                    )
                );
            }
        });
    };

console.log(
    chalk.blue(`Building web app icons, generating meta tags and manifest...`)
);
favicons(source, configuration, callback);
