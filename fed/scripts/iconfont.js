const pjson = require("../package.json");
const fs = require("fs");
const chalk = require("chalk");
const webfontsGenerator = require("webfonts-generator");
const readDirectory = require("./read-directory");

const buildIconfont = () => {
    const svgFiles = [];

    // Gather all svgs in icon folder
    readDirectory(pjson.iconfont.svgDirectory, ".svg", svgPath => {
        svgFiles.push(svgPath);
    });

    webfontsGenerator(
        {
            fontName: pjson.iconfont.fontName,
            files: svgFiles,
            dest: pjson.iconfont.outputDirectory,
            //cssDest: './public/assets/fonts/_iconfont.scss',
            //cssFontsPath: '',
            normalize: true,
            fontHeight: 1001,
            writeFiles: false // don't write files by default as sass will be generated with base64 encoding below
        },
        function(error, result) {
            if (error) {
                console.error(chalk.red("❌ Iconfont build error: ", error));
            } else {
                let generatedCSS = result.generateCss();
                var removedFontFace =
                    generatedCSS.slice(0, generatedCSS.indexOf("@font-face")) +
                    generatedCSS.slice(generatedCSS.indexOf("}") + 1);
                const base64Woff = Buffer.from(result.woff).toString("base64");
                let outputFont = `/* The contents of this file are generated automatically, DO NOT write to this file as changes will be lost. */
                @font-face {
                    font-family: '${pjson.iconfont.fontName}';
                    src: url(data:application/font-woff;charset=utf-8;base64,${base64Woff}) format("woff");
                    font-weight: normal;
                    font-style: normal;
                }
                ${removedFontFace}
            `;

                // write file to src/scss/base directory
                fs.writeFile(pjson.iconfont.outputSCSS, outputFont, function(
                    err
                ) {
                    if (err) {
                        return console.log(
                            chalk.red("❌ Iconfont write error: " + err)
                        );
                    }
                });

                console.log(chalk.green("✅ Iconfont was compiled."));
            }
        }
    );
};

// watch kit directory for file changes and compile
if (process.argv.indexOf("-w") !== -1) {
    fs.watch(
        pjson.iconfont.svgDirectory,
        {
            recursive: false
        },
        function(eventType, fileName) {
            buildIconfont();
        }
    );
    console.log(
        chalk.blue(
            `🔍 Now watching ${pjson.iconfont.svgDirectory} for .svg files.`
        )
    );
}

buildIconfont();
