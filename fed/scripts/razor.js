const pjson = require("../package.json");
//const _ = require("lodash");
const fs = require("fs");
const chalk = require("chalk");
const rimraf = require("rimraf");
//const cheerio = require("cheerio");
//const vash = require("vash");
//const raz = require("raz");

//const raz = require("@adrenalin/razor-js");
const raz = require("../../razor-js");

//const app = require("express")();
const readDirectory = require("./read-directory");

// vash configuration
// vash.config.debug = true;
// vash.config.debugCompiler = true;
//vash.config.modelName = "Model";

// compile razor/.cshtml with json model and write to html file
const compileRazor = (filePath, fileName, changed) => {
    return new Promise((resolve, reject) => {
        const razorJsonPath = filePath.replace(".cshtml", ".json");

        // attemp to load corresponding json model data for the .cshtml template
        fs.readFile(razorJsonPath, "utf8", function(err, razorJsonModel) {
            // fall back to empty model if json file data could not be read

            fs.readFile(filePath, "utf8", function(err, razorTemplate) {
                if (razorTemplate) {
                    // pass data
                    // load .cshtml template into cheerio parser
                    // const $ = cheerio.load(data);
                    // $("*")
                    //     .contents()
                    //     .each(function() {
                    //         if (this.nodeType === 3) {
                    //             // is a text node
                    //             if (this.data && this.data.charAt(0) === "@") {
                    //                 // text node string starts with Razor "@" expression so lowercase it for vash (js razor compiler)
                    //                 this.data = this.data.toLowerCase();
                    //             }
                    //         }
                    //     });
                    // // set parsed output
                    // const parsedTemplate = $.root().html();

                    // pass parsed template to raz to compile

                    // var templateOutputHtml = raz.render({
                    //     model: JSON.parse(razorJsonModel),
                    //     template: razorTemplate
                    // });

                    const templateJsonModel = razorJsonModel
                        ? JSON.parse(razorJsonModel)
                        : {};
                    var templateOutputHtml = raz.renderFile(
                        filePath,
                        {
                            settings: {
                                //"x-powered-by": true,
                                //etag: "weak",
                                //env: "development",
                                //"query parser": "extended",
                                //"subdomain offset": 2,
                                //"trust proxy": false,
                                views:
                                    "/Users/nickmitchell/_code/adrenalin/fed_boilerplate/src/razor/shared"
                                //"jsonp callback name": "callback",
                                //"view engine": "raz"
                            },
                            //_locals: {},
                            //cache: false,
                            ...templateJsonModel
                        },
                        function(err, data) {
                            if (err) {
                                console.log(err);
                            }
                            console.log(data);
                        }
                    );

                    // pass parsed template to vash to compile
                    // const template = vash.compile(parsedTemplate);
                    // const templateOutputHtml = template(
                    //     JSON.parse(razorJsonModel)
                    // );

                    // write compiled template
                    if (templateOutputHtml) {
                        fs.writeFile(
                            `${pjson.razor.outputDirectory}${fileName.replace(
                                ".cshtml",
                                ".html"
                            )}`,
                            templateOutputHtml,
                            err => {
                                if (err) {
                                    reject(err);
                                } else {
                                    resolve();
                                    if (!changed) {
                                        console.log(
                                            chalk.green(
                                                `✅ ${fileName} was compiled and saved.`
                                            )
                                        );
                                    }
                                }
                            }
                        );
                    }
                } else if (err) {
                    console.log(chalk.red("❌" + err));
                }
            });
        });
    });
};

// scan twigDirectory for all twig files to compile
const compileAllRazorFiles = changed => {
    const filesToCompile = [];

    // clear generated html in /public on first run
    if (!changed) {
        rimraf.sync(`${pjson.razor.outputDirectory}*.html`);
    }

    readDirectory(pjson.razor.root, ".cshtml", (filePath, fileName) => {
        filesToCompile.push(compileRazor(filePath, fileName, changed));
    });

    Promise.all(filesToCompile)
        .then(() => {
            // build index file after all .html files are built
            buildIndex();
        })
        .catch(err => {
            console.log(chalk.red("❌" + err));
        });
};

// index directory of twig/html pages
const buildIndex = () => {
    const directoryFilename = "index.html";
    let indexExists;
    let directoryLinks = "";

    // check to only build index directory if an index.cshtml is not present
    readDirectory(pjson.razor.root, ".cshtml", (filePath, filename) => {
        if (filename && filename === "index.cshtml") {
            indexExists = true;
        }
    });

    // of no index.cshtml found then render directory index.html
    if (!indexExists) {
        // create li list of public .html files
        readDirectory(
            pjson.razor.outputDirectory,
            ".html",
            (filePath, filename) => {
                if (filename && filename !== directoryFilename) {
                    directoryLinks =
                        directoryLinks +
                        `<li><a href="/${filename}">${filename}</a></li>`;
                }
            }
        );

        // write index.html directory file
        fs.writeFile(
            pjson.razor.outputDirectory + directoryFilename,
            `<!DOCTYPE html>
            <html lang="en">
                <head>
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                    <title>
                        Directory - Adrenalin Boilerplate
                    </title>
                </head>
                <body>
                    <div class="container">
                        <h1>📚 Directory</h1>
                        <p>
                            A directory of .html files that have been compiled from .cshtml files in the <strong>${pjson.razor.root}</strong> directory.
                        </p>
                        <p>
                            To use root or index as a page (ideal for single page applications), create an index.cshtml file in the <strong>${pjson.razor.root}</strong> directory.
                        </p>
                        <h2>Pages</h2>
                        <ul>${directoryLinks}</ul>
                    </div>
                </body>
            </html>
            `,
            err => {
                if (err) {
                    return console.log(err);
                }
            }
        );
    }
};

// Watch kit directory for file changes and compile
if (process.argv.indexOf("-w") !== -1) {
    fs.watch(
        pjson.razor.root,
        {
            recursive: true //watch all sub folder twig templates
        },
        (eventType, fileName) => {
            if (eventType === "rename" && fileName.split("/").length === 1) {
                // Page/View was created, renamed or deleted
                console.log(
                    chalk.yellow(
                        `⚠️ The page ${fileName} was created, renamed or deleted. Please run 'yarn start' to reinitialise the page.`
                    )
                );
                // exit process
                process.exit(1);
            } else {
                // Normal compile of pages
                //compileRazor(pjson.razor.root + fileName, fileName);
                compileAllRazorFiles(true);
            }
        }
    );
    console.log(
        chalk.blue(
            `🔍 Now watching ${pjson.razor.root} for .cshtml file changes.`
        )
    );
}

compileAllRazorFiles();
