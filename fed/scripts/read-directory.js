const path = require("path");
const fs = require("fs");

const readDirectory = function(startPath, filter, callback, recursive) {
    if (!fs.existsSync(startPath)) {
        console.log("no dir ", startPath);
        return;
    }

    var files = fs.readdirSync(startPath);
    for (var i = 0; i < files.length; i++) {
        var filepath = path.join(startPath, files[i]);
        var stat = fs.lstatSync(filepath);

        // recursive
        if (recursive && stat.isDirectory()) {
            // recursive
            readDirectory(filepath, filter, callback, recursive);
        } else if (filter) {
            // filter
            if (filepath.indexOf(filter) >= 0) {
                callback(filepath, files[i]);
            }
        } else {
            callback(filepath, files[i]);
        }
    }
};

module.exports = readDirectory;
