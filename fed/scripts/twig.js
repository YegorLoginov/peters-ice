const pjson = require("../package.json");
const Twig = require("twig");
const fs = require("fs");
const chalk = require("chalk");
const rimraf = require("rimraf");
const readDirectory = require("./read-directory");

// Prevent twig compilation errors from killing the process
process.on("uncaughtException", function(error) {});

// Disable caching of twig views/templates (allows watch task to update)
Twig.cache(false);

// compile twig and write to html file
const compileTwig = (filePath, fileName, changed) => {
    return new Promise((resolve, reject) => {
        Twig.renderFile(
            filePath,
            {
                settings: {
                    views: "src/"
                },
                // variables
                twigDir: "twig/"
            },
            (err, html) => {
                if (err) {
                    reject(err);
                } else {
                    // compare existing compiled .html
                    fs.readFile(
                        `${pjson.twig.outputDirectory}${fileName}.html`,
                        "utf8",
                        function(err, data) {
                            // if new compiled twig page is different or html dosnt exist then write html
                            if (
                                err ||
                                data.replace(/ /g, "") !==
                                    html.replace(/ /g, "")
                            ) {
                                // write updated html
                                fs.writeFile(
                                    `${
                                        pjson.twig.outputDirectory
                                    }${fileName.replace(".twig", ".html")}`,
                                    html,
                                    err => {
                                        if (err) {
                                            reject(err);
                                        } else {
                                            resolve();
                                            if (!changed) {
                                                console.log(
                                                    chalk.green(
                                                        `✅ ${fileName} was compiled and saved.`
                                                    )
                                                );
                                            }
                                        }
                                    }
                                );
                            }
                        }
                    );
                }
            }
        );
    });
};

// scan twigDirectory for all twig files to compile
const compileAllTwigFiles = changed => {
    const filesToCompile = [];

    // clear generated html in /public on first run
    if (!changed) {
        rimraf.sync(`${pjson.twig.outputDirectory}*.html`);
    }

    readDirectory(pjson.twig.root, ".twig", (filePath, fileName) => {
        filesToCompile.push(compileTwig(filePath, fileName, changed));
    });

    Promise.all(filesToCompile)
        .then(() => {
            // build index file after all .html files are built
            buildIndex();
        })
        .catch(err => {
            console.log(chalk.red("❌" + err));
        });
};

// index directory of twig/html pages
const buildIndex = () => {
    const directoryFilename = "index.html";
    let indexExists;
    let directoryLinks = "";

    // check to only build index directory if an index.twig is not present
    readDirectory(pjson.twig.root, ".twig", (filePath, filename) => {
        if (filename && filename === "index.twig") {
            indexExists = true;
        }
    });

    // of no index.twig found then render directory index.html
    if (!indexExists) {
        // create li list of public .html files
        readDirectory(
            pjson.twig.outputDirectory,
            ".html",
            (filePath, filename) => {
                if (filename && filename !== directoryFilename) {
                    directoryLinks =
                        directoryLinks +
                        `<li><a href="/${filename}">${filename}</a></li>`;
                }
            }
        );

        // write index.html directory file
        fs.writeFile(
            pjson.twig.outputDirectory + directoryFilename,
            `<!DOCTYPE html>
            <html lang="en">
                <head>
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                    <title>
                        Directory - Adrenalin Boilerplate
                    </title>
                </head>
                <body>
                    <div class="container">
                        <h1>📚 Directory</h1>
                        <p>
                            A directory of .html files that have been compiled from .twig files in the <strong>${
                                pjson.twig.root
                            }</strong> directory.
                        </p>
                        <p>
                            To use root or index as a page (ideal for single page applications), create an index.twig file in the <strong>${
                                pjson.twig.root
                            }</strong> directory.
                        </p>
                        <h2>Pages</h2>
                        <ul>${directoryLinks}</ul>
                    </div>
                </body>
            </html>
            `,
            err => {
                if (err) {
                    return console.log(err);
                }
            }
        );
    }
};

// Watch kit directory for file changes and compile
if (process.argv.indexOf("-w") !== -1) {
    fs.watch(
        pjson.twig.root,
        {
            recursive: true //watch all sub folder twig templates
        },
        (eventType, fileName) => {
            if (eventType === "rename" && fileName.split("/").length === 1) {
                // Page/View was created, renamed or deleted
                console.log(
                    chalk.yellow(
                        `⚠️ The page ${fileName} was created, renamed or deleted. Please run 'yarn start' to reinitialise the page.`
                    )
                );
                // exit process
                process.exit(1);
            } else {
                // Normal compile of pages
                //compileTwig(pjson.twig.root + fileName, fileName);
                compileAllTwigFiles(true);
            }
        }
    );
    console.log(
        chalk.blue(`🔍 Now watching ${pjson.twig.root} for .twig file changes.`)
    );
}

compileAllTwigFiles();
