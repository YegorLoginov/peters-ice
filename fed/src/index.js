// @flow

import * as React from "react";
import ReactDOM from "react-dom";
import * as serviceWorker from "./serviceWorker";
import NProgress from "nprogress";

import "@scss/index.scss";
import App from "./js/containers/App";
import store from "./js/store";
import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import DataFetcher from "./js/utils/DataFetcher";
import Routes from "./js/Routes";
import actions from "@actions/index.js";

// Configure the site laoding bar
NProgress.configure({
    minimum: 0.3,
    trickleSpeed: 250,
    easing: "ease",
    speed: 250,
    barSelector: ".bar",
    template:
        '<div class="bar"><div class="peg"></div></div><div class="spinner"><div class="spinner-icon"></div></div>',
});

const OuterApp = (): React.Node => {
    const handleLoadingStateChange = ({ loading, error }: any = {}) => {
        if (loading) {
            NProgress.start();
        } else {
            NProgress.done();
            window.scrollTo(0, 0);
        }
        if (error) {
            NProgress.done();
        }
    };
    return (
        <Provider store={store}>
            <Router>
                <DataFetcher
                    routes={Routes}
                    passToPreload={{ store }}
                    onLoadingStateChange={handleLoadingStateChange}
                    initialPreload={(): Promise<any> => {
                        return Promise.all([
                            store.dispatch(actions.initialFetchAll()),
                        ]);
                    }}
                >
                    <App />
                </DataFetcher>
            </Router>
        </Provider>
    );
};

const rootEl = document.getElementById("root");
if (rootEl) {
    ReactDOM.render(<OuterApp />, rootEl);
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
