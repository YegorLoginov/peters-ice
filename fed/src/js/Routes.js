import TemplateChooser from "@containers/TemplateChooser";

const routes = [
    {
        path: "*",
        component: TemplateChooser,
    },
];

export default routes;
