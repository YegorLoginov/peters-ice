import * as options from "./options";
import * as initialFetchAll from "./initialFetchAll";
import * as page from "./page";
import * as ui from "./ui";

export default {
    ...initialFetchAll,
    ...options,
    ...page,
    ...ui,
};
