import ActionTypes from "@ActionTypes";
import API from "@api/index";

// FETCH OPTIONS
// -------------

const fetchOptionsRequest = () => ({
    type: ActionTypes.FETCH_OPTIONS_REQUEST,
});

const fetchOptionsSuccess = options => ({
    type: ActionTypes.FETCH_OPTIONS_SUCCESS,
    options,
});

const fetchOptionsFail = error => ({
    type: ActionTypes.FETCH_OPTIONS_FAIL,
    error,
});

export const fetchOptions = () => dispatch => {
    dispatch(fetchOptionsRequest());
    return API.fetchOptions()
        .then(data => {
            dispatch(fetchOptionsSuccess(data));
        })
        .catch(error => {
            console.error(error);
            dispatch(fetchOptionsFail(error));
        });
};
