import ActionTypes from "@ActionTypes";
import API from "@api/index";

const fetchPageRequest = () => ({
    type: ActionTypes.FETCH_PAGE_REQUEST,
});

const fetchPageSuccess = page => ({
    type: ActionTypes.FETCH_PAGE_SUCCESS,
    page,
});

const fetchPageFail = error => ({
    type: ActionTypes.FETCH_PAGE_FAIL,
    error,
});

export const fetchPage = (path = "/", pageQuery = "") => dispatch => {
    dispatch(fetchPageRequest());
    return API.fetchPage(path, pageQuery)
        .then(async data => {
            const page = { ...data, path };
            dispatch(fetchPageSuccess(page));
        })
        .catch(error => {
            console.error(error);
            dispatch(fetchPageFail(error));
        });
};

export const updateCurrentPagePath = (path = "/") => ({
    type: ActionTypes.UPDATE_CURRENT_PAGE_PATH,
    path,
});

export const updateFiltersList = filters => ({
    type: ActionTypes.UPDATE_FILTERS_LIST,
    filters,
});

export const updateFeaturedList = featured => ({
    type: ActionTypes.UPDATE_FEATURED_LIST,
    featured,
});
