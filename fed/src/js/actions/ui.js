import ActionTypes from "@ActionTypes";

export const toggleMobileMenu = () => ({
    type: ActionTypes.TOGGLE_MOBILE_MENU,
});

export const closeMobileMenu = () => ({
    type: ActionTypes.CLOSE_MOBILE_MENU,
});
