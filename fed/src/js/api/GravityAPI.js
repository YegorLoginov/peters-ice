// @flow

import md5 from "md5";

import Config from "@Config";
import { convertToFormData } from "@utils/convertToFormData";

class GravityAPI {
    _formId: string;
    _uniqueId: string;
    _fileUploadURL: string;
    _uploadedFileIndex: 0;

    constructor(formId: string, uniqueId: string, uploadPageId: string) {
        this._uniqueId = uniqueId;
        this._fileUploadURL = `${Config.wordpressHost}/?gf_page=${uploadPageId}`;
        this._formId = formId;
    }

    submit = (values: { [key: string]: any }): Promise<any> => {
        return fetch(`${Config.gravityAPI}/forms/${this._formId}/submissions`, {
            method: "POST",
            body: convertToFormData(values),
        }).then((res): any => res.json());
    };

    uploadFile = (file: File, fieldId: string): Promise<any> => {
        this._uploadedFileIndex += 1;
        const data = {
            name: `${md5(
                file.name + Date.now() + this._uploadedFileIndex
            )}.${file.name.split(".").pop()}`,
            form_id: `${this._formId}`,
            field_id: fieldId,
            gform_unique_id: this._uniqueId,
            original_filename: String(file.name),
            file: file,
        };
        return fetch(this._fileUploadURL, {
            method: "POST",
            body: convertToFormData(data),
        }).then((res): any => res.json());
    };
}

export default GravityAPI;
