// @flow

import Config from "@Config";
import keyValueToURLParams from "@utils/keyValueToURLParams";
import qs from "query-string";

import GravityAPI from "./GravityAPI";

const getJSON = (
    url: string,
    headers: {} = {},
    body: ?{} = null
): Promise<any> => {
    return fetch(url, {
        headers: {
            ...headers,
        },
        body: body ? JSON.stringify(body) : null,
    })
        .then((res: any): any => res.json())
        .catch((error: any): any => console.error(error));
};

export const fetchOptions = (): Promise<any> => {
    return getJSON(Config.optionsAPI).then(({ acf }: any): any => acf);
};

export const fetchPage = (
    pagePath: string = "/",
    pageQuery: string
): Promise<any> => {
    const queries = qs.parse(pageQuery);
    if (
        queries.preview &&
        (queries.preview_id || queries.p || queries.page_id)
    ) {
        return getJSON(
            `${Config.pageAPI}?preview=true&page_id=${queries.preview_id ||
                queries.p ||
                queries.page_id}`,
            { "X-WP-Nonce": window.rest_nonce }
        );
    } else {
        return getJSON(`${Config.pageAPI}?path=${pagePath}`);
    }
};

const fetchDataWithHeaders = (url: string): Promise<any> => {
    return fetch(url).then(async (res: any): Promise<any> => {
        let headers = {};
        for (let [key, value] of res.headers.entries()) {
            headers[key] = value;
        }
        return { data: await res.json(), headers };
    });
};

export const fetchPosts = ({
    postType,
    perPage = 12,
    page = 1,
    fields,
    extra = {},
}: {
    postType: string,
    perPage?: number,
    page?: number,
    fields?: string[],
    extra: { [key: string]: any },
}): Promise<any> => {
    const params: any = {
        orderby: "menu_order",
        order: "asc",
        per_page: perPage,
        page: page,
        ...extra,
    };
    if (fields) {
        params["_fields"] = fields.join(",");
    }
    const urlParams = keyValueToURLParams(params);
    const url = `${Config.genericWordpressAPI}/${postType}?${urlParams}`;
    return fetchDataWithHeaders(url);
};

export default {
    fetchOptions,
    fetchPage,
    fetchPosts,
    GravityAPI,
};
