// @flow

import * as React from "react";

import Link from "@components/atoms/Link";

import { LinkType } from "@types";

type PropsType = {
    className: string,
    children: React.Node,
    variation: "primary" | "secondary" | "tertiary" | "white",
    to?: string,
    link?: LinkType,
    elementType?: string,
};

const Button = (props: PropsType): React.Node => {
    const {
        link,
        variation,
        children,
        elementType,
        className,
        ...remainingProps
    } = props;

    const isAnchor = props.to || (link && link.url);
    const RootElement = isAnchor ? Link : elementType || "button";
    const classes = ["button"];
    classes.push(className);
    classes.push(`button--${variation}`);

    let linkProps = {};
    if (link) {
        linkProps = {
            to: link.url,
            target: link.target,
        };
    }

    return (
        <RootElement
            className={classes.join(" ")}
            {...remainingProps}
            {...linkProps}
        >
            {link ? link.title : children}
        </RootElement>
    );
};

Button.defaultProps = {
    className: "",
    variation: "primary",
    children: null,
};

export default Button;
