// @flow

import * as React from "react";

import Link from "@components/atoms/Link";
import CtaArrow from "@components/atoms/CtaArrow";
import { LinkType } from "@types";

type PropsType = {
    children: React.Node,
    className: string,
    to?: string,
    link?: LinkType,
};

const CTA = (props: PropsType): React.Node => {
    const { link, children, className, ...restProps } = props;

    const isLink = props.to || link;
    const RootNode = isLink ? Link : "button";

    let elProps = {};

    if (link) {
        elProps = {
            to: link.url,
            target: link.target,
        };
    }

    if (!isLink) {
        elProps = {
            type: "button",
        };
    }

    const classes = ["cta"];
    if (className) {
        classes.push(className);
    }

    const inner = link ? link.title : children;

    return (
        <RootNode {...elProps} {...restProps} className={classes.join(" ")}>
            {inner}
            <CtaArrow />
        </RootNode>
    );
};

CTA.defaultProps = {
    children: null,
    className: "",
};

export default CTA;
