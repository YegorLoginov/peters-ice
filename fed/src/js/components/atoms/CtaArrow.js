// @flow

import * as React from "react";

import Icon from "@components/atoms/Icon";

type PropsType = {};

const CtaArrow = (props: PropsType): React.Node => {
    return (
        <span className="cta-arrow">
            <Icon name="long-arrow-right"></Icon>
        </span>
    );
};

export default CtaArrow;
