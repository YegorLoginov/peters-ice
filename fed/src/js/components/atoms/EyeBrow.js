// @flow

import * as React from "react";

type PropsType = {
    children: React.Node,
    className: string,
    color: "red",
};

function EyeBrow(props: PropsType): React.Node {
    const classList = ["eyebrow"];
    classList.push(`eyebrow--${props.color}`);
    classList.push(props.className);
    return <p className={classList.join(" ")}>{props.children}</p>;
}

EyeBrow.defaultProps = {
    className: "",
    color: "red",
};

export default EyeBrow;
