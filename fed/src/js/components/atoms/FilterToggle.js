// @flow

import * as React from "react";

type PropsType = {
    title: string,
    active: boolean,
    onClick: () => void,
};

const FilterToggle = ({ title, active, ...props }: PropsType): React.Node => {
    return (
        <button
            onClick={props.onClick}
            className={`filter-toggle filter-toggle--${
                active ? "active" : "inactive"
            }`}
            {...props}
        >
            {title}
        </button>
    );
};

export default FilterToggle;
