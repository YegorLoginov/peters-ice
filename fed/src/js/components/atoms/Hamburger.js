// @flow

import * as React from "react";

type PropsType = {
    active: boolean,
};

const Hamburger = (props: PropsType): React.Node => {
    return (
        <div className={`hamburger ${props.active ? "active" : ""}`}>
            <span></span>
            <span></span>
            <span></span>
        </div>
    );
};

export default Hamburger;
