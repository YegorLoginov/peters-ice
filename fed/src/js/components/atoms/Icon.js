// @flow

import * as React from "react";

type PropsType = { name: string, label?: string };

const Icon = (props: PropsType): React.Node => {
    return (
        <i
            aria-label={props.label || props.name}
            className={`icon icon-${props.name}`}
        />
    );
};

export default Icon;
