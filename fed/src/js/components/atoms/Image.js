// @flow

import * as React from "react";

import { ImageType } from "@types";

type PropsType = {
    image?: ImageType,
    className: string,
    onLoad?: () => void,
    size?:
        | "thumbnail"
        | "medium"
        | "medium_large"
        | "large"
        | "2048x2048"
        | "1536x1536"
        | string,
};

const Image = ({ image, className, ...props }: PropsType): React.Node => {
    const [loaded, setLoaded] = React.useState(false);
    const classes = className.split(" ").filter((a): boolean => !!a);
    classes.push(loaded ? "loaded" : "loading");
    if (!image) {
        return null;
    }
    return (
        <img
            {...props}
            src={
                props.size && image.sizes ? image.sizes[props.size] : image.url
            }
            alt={image.alt}
            className={classes.join(" ")}
            onLoad={() => {
                setLoaded(true);
                if (props.onLoad) {
                    props.onLoad();
                }
            }}
        />
    );
};

Image.defaultProps = {
    className: "",
};

export default Image;
