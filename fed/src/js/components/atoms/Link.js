// @flow

import * as React from "react";
import { Link as RouterLink } from "react-router-dom";
import Config from "@Config";
import scrollToEl from "@utils/scrollToEl";

type PropsType = {
    to: string,
    onClick?: (event: SyntheticEvent<HTMLAnchorElement>) => void,
    children?: any,
};

const Link = (props: PropsType): React.Node => {
    const realTo = props.to ? props.to.replace(Config.wordpressHost, "") : "";
    const handleClick = (event: SyntheticEvent<HTMLAnchorElement>) => {
        if (realTo[0] === "#") {
            const el = document.querySelector(realTo);
            if (el) {
                event.preventDefault();
                scrollToEl(el);
            }
        }
        if (props.onClick) {
            props.onClick(event);
        }
    };

    if (
        props.to &&
        props.to.indexOf(Config.wordpressHost) === -1 &&
        props.to.indexOf("http") === 0
    ) {
        // support external link
        const anchorProps = { ...props };
        delete anchorProps.to;
        return (
            <a href={props.to} {...anchorProps}>
                {anchorProps.children}
            </a>
        );
    } else {
        return <RouterLink {...props} onClick={handleClick} to={realTo} />;
    }
};

export default Link;
