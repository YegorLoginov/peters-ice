// @flow

import * as React from "react";

let scrollDistance = 0;

const LockBodyScroll = (): React.Node => {
    React.useEffect((): any => {
        if (typeof window !== `undefined`) {
            const { body } = document;

            scrollDistance = window.scrollY;
            requestAnimationFrame(() => {
                if (body) {
                    body.classList.add("noscroll");
                    body.style.top = `-${scrollDistance}px`;
                }
            });

            return () => {
                requestAnimationFrame(() => {
                    if (body) {
                        body.classList.remove("noscroll");
                        body.style.top = "";
                        if (scrollDistance) {
                            window.scroll(null, scrollDistance);
                            scrollDistance = 0;
                        }
                    }
                });
            };
        }
    }, []);
    return null;
};

export default LockBodyScroll;
