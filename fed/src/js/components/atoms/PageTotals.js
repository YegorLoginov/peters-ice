// @flow

import * as React from "react";

type PropsType = {
    currentPageNumber: number,
    postsPerPage: number,
    totalPosts: number,
    featuredCount: number,
};

const PageTotals = ({
    currentPageNumber,
    postsPerPage,
    totalPosts,
    featuredCount,
}: PropsType): React.Node => {
    let start =
        totalPosts === 0 ? 0 : (currentPageNumber - 1) * postsPerPage + 1;
    let end = totalPosts === 0 ? 0 : start + postsPerPage - 1;

    // Add featured posts to counts
    totalPosts += featuredCount;
    end += featuredCount;
    if (currentPageNumber !== 1) {
        start += featuredCount;
    }

    // Account for there only being 1 page of pagination
    if (totalPosts < end) {
        end = totalPosts;
    }

    return (
        <p className="page-totals">
            Showing <strong>{start}</strong> - <strong>{end}</strong> of{" "}
            <strong>{totalPosts}</strong> results
        </p>
    );
};

PageTotals.defaultProps = {
    featuredCount: 0,
};

export default PageTotals;
