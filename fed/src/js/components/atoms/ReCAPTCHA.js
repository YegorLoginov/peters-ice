// @flow

import * as React from "react";
import ReCAPTCHA from "react-google-recaptcha";

// Very simple around react-google-recaptcha to enable execute to
// return a promise containing the successful recaptcha response
class RecaptchaWrapper extends React.Component<any> {
    recaptchaRef = React.createRef<ReCAPTCHA>();
    executeResolver: ?(string) => void;
    code: ?string = null;

    handleChange = (code: string) => {
        this.code = code;
        if (this.executeResolver) {
            this.executeResolver(code);
            this.executeResolver = null;
        }
    };

    execute = (): Promise<string> => {
        this.code = null;
        if (this.recaptchaRef.current) {
            this.recaptchaRef.current.reset();
        }
        if (this.recaptchaRef.current) {
            this.recaptchaRef.current.execute();
        }
        return new Promise(resolve => {
            if (this.code) {
                resolve(this.code);
                this.executeResolver = null;
            } else {
                this.executeResolver = resolve;
            }
        });
    };

    render(): React.Node {
        return (
            <ReCAPTCHA
                {...this.props}
                onChange={this.handleChange}
                ref={this.recaptchaRef}
            />
        );
    }
}

export default RecaptchaWrapper;
