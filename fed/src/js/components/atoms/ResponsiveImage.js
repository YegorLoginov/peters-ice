// @flow

import * as React from "react";

import Image from "@components/atoms/Image";

import { ImageType } from "@types";
import Config from "@Config";

export type ResponsiveImageType = Array<{
    desktop: ImageType,
    mobile?: ImageType,
}>;

const ResponsiveImage = (props: {
    image: ResponsiveImageType,
    className: string,
    breakpoint: "sm" | "md",
    sizes?: {
        desktop: string,
        mobile: string,
    },
}): React.Node => {
    if (!props.image || !props.image[0]) {
        return (
            <Image
                className={props.className}
                image={{ url: Config.fallbackFeaturedImage }}
            />
        );
    }
    if (!props.image[0].mobile) {
        return (
            <Image
                className={`${props.className}`}
                image={props.image[0].desktop}
                size={(props.sizes && props.sizes.desktop) || "original"}
            />
        );
    }
    return (
        <>
            <Image
                className={`show-for-${props.breakpoint} ${props.className}`}
                image={props.image[0].desktop}
                size={(props.sizes && props.sizes.desktop) || "original"}
            />
            <Image
                className={`hide-for-${props.breakpoint} ${props.className}`}
                image={props.image[0].mobile}
                size={(props.sizes && props.sizes.mobile) || "original"}
            />
        </>
    );
};

ResponsiveImage.defaultProps = {
    className: "",
    breakpoint: "md",
};

export default ResponsiveImage;
