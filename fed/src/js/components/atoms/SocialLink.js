// @flow

import * as React from "react";

import Icon from "@components/atoms/Icon";

const SocialLink = (props: { icon: string, url: string }): React.Node => {
    return (
        <a
            href={props.url}
            target="_blank"
            rel="noreferrer noopener"
            className="social-link"
        >
            <div className="social-link__icon">
                <Icon name={props.icon}></Icon>
            </div>
            <div className="social-link__name">{props.icon}</div>
        </a>
    );
};

export default SocialLink;
