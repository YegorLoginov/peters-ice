// @flow

import * as React from "react";

import Image from "@components/atoms/Image";

import { type ImageType } from "@types";

type PropsType = {
    image: ImageType,
    size?: string,
};

/**
 * Default thumbnail image used across the site - has a subtle zoom
 * animation on hover.
 *
 * Must be nested inside `ThumbImage.Wrapper` or a container element with
 * the following style attributes:
 * - `position: absolute | relative`
 * - `overflow: hidden`
 *
 **/
const ThumbImage = (props: PropsType): React.Node => {
    return (
        <Image
            className="thumb-image"
            image={props.image}
            size={props.size || "original"}
        ></Image>
    );
};

/** Use this component as a wrapper around ThumbImage to use the default Aspect Ratio */
ThumbImage.Wrapper = (props: { children: React.Node }): React.Node => {
    return <div className="thumb-image-container">{props.children}</div>;
};

export default ThumbImage;
