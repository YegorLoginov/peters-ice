// @flow

import * as React from "react";

import Icon from "@components/atoms/Icon";

const WTBItem = (props: {
    logo: string,
    name: string,
    link: string,
}): React.Node => {
    return (
        <a
            href={props.link}
            className="wtb-item"
            target="_blank"
            rel="noreferrer noopener"
        >
            <div className="wtb-item__logo">
                <img src={props.logo} alt="" />
            </div>
            <div className="wtb-item__text">
                <div className="wtb-item__text__inner">{props.name}</div>
                <div className="wtb-item__text__icon">
                    <Icon name="arrow-up-right" />
                </div>
            </div>
        </a>
    );
};

export default WTBItem;
