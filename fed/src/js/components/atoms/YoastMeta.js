// @flow

import * as React from "react";
import Helmet from "react-helmet";
import { useSelector } from "react-redux";

import usePageFromStore from "@hooks/usePageFromStore";
import { getFeaturedImage } from "@utils/post";

const YoastMeta = (): React.Node => {
    const { page, loading } = usePageFromStore();

    const currentPagePath = useSelector(
        (state: any): any => state.page.currentPagePath
    );

    React.useEffect((): any => {
        // Do nothing - just force a render whenever one of the below properties change
    }, [page, loading, currentPagePath]);

    if (!page && loading) return null;

    const { seo } = page;

    if (!seo) return null;

    const hasFeaturedImage = !!seo.meta.find((item): any => {
        return item.property === "og:image";
    });

    if (!hasFeaturedImage) {
        const featuredImg = getFeaturedImage(page);
        seo.meta.push({
            property: "og:image",
            content: featuredImg.url,
        });
        seo.meta.push({
            property: "og:image:width",
            content: featuredImg.width,
        });
        seo.meta.push({
            property: "og:image:height",
            content: featuredImg.height,
        });
    }

    // const excerpt = getExcerpt(page);
    // const hasDescription = !!seo.meta.find((item): any => {
    //     return item.property === "og:description";
    // });

    // if (!hasDescription && excerpt) {
    //     seo.meta.unshift({
    //         property: "og:description",
    //         content: excerpt,
    //     });
    //     seo.meta.unshift({
    //         name: "description",
    //         content: excerpt,
    //     });
    // }

    return (
        <Helmet>
            <title>{seo.title}</title>
            {seo.meta.map((tag: any, index: number): React.Node => (
                <meta key={index} {...tag} />
            ))}
        </Helmet>
    );
};

export default YoastMeta;
