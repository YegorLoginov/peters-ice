// @flow

import * as React from "react";
import AnimateHeight from "@components/atoms/AnimateHeight";
import RichText from "./RichText";

export type AccordionType = {
    title: string,
    content: string,
};

type PropsType = {
    accordion: AccordionType,
};

const Accordion = ({ accordion }: PropsType): React.Node => {
    const [isOpen, setIsOpen] = React.useState(false);
    const handleTitleClick = () => {
        setIsOpen((a: boolean): boolean => !a);
    };
    return (
        <div className={`accordion ${isOpen ? "accordion--active" : ""}`}>
            <button className="accordion__title" onClick={handleTitleClick}>
                <h5 className="accordion__title__inner">{accordion.title}</h5>
                <i
                    className={`icon icon-${
                        isOpen ? "arrow-up" : "arrow-down"
                    }`}
                ></i>
            </button>
            <div className="accordion__content">
                <AnimateHeight
                    duration={500}
                    hideOverflow
                    height={isOpen ? null : 0}
                >
                    <div
                        style={{
                            transition: "300ms ease all",
                            opacity: isOpen ? 1 : 0,
                        }}
                    >
                        <RichText
                            className="accordion__content__inner"
                            html={accordion.content}
                        />
                    </div>
                </AnimateHeight>
            </div>
        </div>
    );
};

Accordion.defaultProps = {};

export default Accordion;
