// @flow

import * as React from "react";

import Image from "@components/atoms/Image";
import { ImageType } from "@types";

const AvailableIn = (props: {
    availableIn: Array<{ image: ImageType }>,
}): React.Node => {
    return (
        <div className="available-in">
            {props.availableIn.map(({ image }, index): React.Node => (
                <div className="available-in__item" key={index}>
                    <div className="available-in__item__image">
                        <Image image={image}></Image>
                    </div>
                </div>
            ))}
        </div>
    );
};

export default AvailableIn;
