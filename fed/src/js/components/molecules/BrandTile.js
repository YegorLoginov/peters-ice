// @flow

import * as React from "react";

import Link from "@components/atoms/Link";

const BrandTile = (props: { brand: any }): React.Node => {
    const image = props.brand.acf.brand_logo;
    return (
        <Link
            to={props.brand.url}
            className="brand-tile"
            aria-label={props.brand.title}
        >
            <img
                className="brand-tile__image"
                src={image ? image.sizes.medium_large : null}
                alt={`${props.brand.title} logo`}
            />
        </Link>
    );
};

export default BrandTile;
