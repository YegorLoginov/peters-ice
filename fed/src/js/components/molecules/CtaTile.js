// @flow

import * as React from "react";

import Link from "@components/atoms/Link";
import CTA from "@components/atoms/CTA";
import ThumbImage from "@components/atoms/ThumbImage";

import { LinkType, ImageType } from "@types";

export type CtaTileType = {
    cta: LinkType,
    image: ImageType,
    description?: string,
    title: string,
};

type PropsType = {
    tile: CtaTileType,
    size: "regular" | "large",
};

const CtaTile = (props: PropsType): React.Node => {
    const { tile } = props;
    const classes = ["cta-tile"];
    classes.push(`cta-tile--${props.size}`);
    return (
        <Link
            to={tile.cta.url}
            className={classes.join(" ")}
            aria-label={tile.title}
        >
            <ThumbImage image={tile.image} size="1536x1536" />
            <div className="cta-tile__content">
                <h3 className="cta-tile__title heading-5">{tile.title}</h3>
                {tile.description && (
                    <p className="cta-tile__description">{tile.description}</p>
                )}
                <CTA>{tile.cta.title}</CTA>
            </div>
        </Link>
    );
};

CtaTile.defaultProps = {
    size: "regular",
};

export default CtaTile;
