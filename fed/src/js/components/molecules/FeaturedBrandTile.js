// @flow

import * as React from "react";

import Link from "@components/atoms/Link";
import { getExcerpt } from "@utils/post";
import ThumbImage from "@components/atoms/ThumbImage";
import Image from "@components/atoms/Image";
import { ImageType } from "@types";
import { type AcfType as AcfBrandTYpe } from "@containers/BrandHub";

const FeaturedBrandTile = (props: {
    brand: any,
    image: ImageType,
}): React.Node => {
    const brandACF: AcfBrandTYpe = props.brand.acf;

    return (
        <Link
            to={props.brand.url}
            className="featured-brand-tile"
            aria-label={props.brand.title}
        >
            <div className="featured-brand-tile__image">
                <ThumbImage image={props.image} size="1536x1536"></ThumbImage>
                <div className="featured-brand-tile__image__logo">
                    <Image
                        image={brandACF.brand_logo}
                        size="medium_large"
                    ></Image>
                </div>
            </div>
            <div className="featured-brand-tile__content">
                <h3 className="featured-brand-tile__title">
                    {props.brand.title}
                </h3>
                <p className="featured-brand-tile__blurb body">
                    {getExcerpt(props.brand)}
                </p>
            </div>
        </Link>
    );
};

export default FeaturedBrandTile;
