// @flow

import * as React from "react";

import Button from "@components/atoms/Button";

export type GravityFormFieldType = {
    id: number,
    type: string,
    label: string,
    placeholder: string,
    content: string,
    [key: string]: any,
};

export type GravityFormType = {
    fields: Array<GravityFormFieldType>,
    id: number,
    confirmations: any,
};

const FileUploadComponent = (props: any): React.Node => {
    const classes = ["file-upload-component"];
    for (let value of Object.values(props.classes)) {
        if (value) {
            classes.push(value);
        }
    }

    let fileNames = [];
    if (props.file instanceof FileList) {
        for (let file of props.file) {
            fileNames.push(file.name);
        }
    }

    const [change, setChange] = React.useState(false);
    return (
        <div className={classes.join(" ")}>
            {/* input element (input, select, textarea) */}
            {React.cloneElement(props.inputElement, {
                multiple: true,
            })}

            <label className="file-upload-component__label">
                {props.label}
            </label>

            <div className="file-upload-component__instructions">
                Select one or more images to attach. Accepted file types:{" "}
                {props.accept}
                <br />
                Max file size {props["data-max-file-size"]}MB
            </div>

            <div className="file-upload-component__inner">
                {/* Custom upload control */}
                <div className="file-upload-component__left">
                    <div className="file-upload-component__file-list">
                        {props.file.length ? fileNames.join(", ") : ""}
                    </div>
                    {props.file.length ? (
                        <div
                            className="file-upload-component__clear"
                            onClick={() => {
                                props.inputElement.ref.current.value = null;
                                setChange(!change);
                            }}
                        >
                            <i className="icon icon-close"></i>
                        </div>
                    ) : null}
                </div>
                <div className="file-upload-component__right">
                    <label htmlFor={props.id}>
                        <Button
                            type="button"
                            className="file-upload-component__button"
                            variation="secondary"
                            elementType="div"
                        >
                            Choose files
                        </Button>
                    </label>
                </div>
            </div>

            {/*errors*/}
            {!props.hideErrors &&
            props.errors &&
            props.errors.length &&
            props.showErrors ? (
                <div className={`react-form__field__errors`}>
                    <div
                        className={`react-form__field__errors__item`}
                        id={props.accessibilityIds.error}
                        dangerouslySetInnerHTML={{
                            __html: props.errors[0],
                        }}
                    ></div>
                </div>
            ) : null}
        </div>
    );
};

export default FileUploadComponent;
