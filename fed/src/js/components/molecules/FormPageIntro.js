// @flow

import * as React from "react";

import Icon from "@components/atoms/Icon";
import CTA from "@components/atoms/CTA";

import { LinkType } from "@types";

export type IntroType = {
    title: string,
    subtitle: string,
    cta: LinkType,
    phone_number: string,
};

const FormPageIntro = (props: { intro: IntroType }): React.Node => {
    return (
        <div className="form-page-intro">
            <div className="form-page-intro__text">
                <h3 className="heading-2">{props.intro.title}</h3>
                <p className="body">{props.intro.subtitle}</p>
            </div>
            <div className="form-page-intro__links">
                {props.intro.phone_number && (
                    <a
                        href={`tel:${props.intro.phone_number}`}
                        className="form-page-intro__phone"
                    >
                        <Icon name="phone" />
                        {props.intro.phone_number}
                    </a>
                )}
                {props.intro.cta && (
                    <div className="form-page-intro__cta">
                        <CTA link={props.intro.cta}></CTA>
                    </div>
                )}
            </div>
        </div>
    );
};

export default FormPageIntro;
