/* eslint-disable jsx-a11y/anchor-is-valid */
// @flow

import * as React from "react";
import qs from "query-string";

import paginationGenerator from "@utils/paginationGenerator";

type PropsType = {
    numberOfPages: number,
    currentPageNumber: number,
    pageNumberChanged: (newPageNumber: number) => void,
};

const Pagination = (props: PropsType): React.Node => {
    const TOTAL_PAGES_TO_SHOW = 3;

    const baseURL =
        window.location.protocol +
        "//" +
        window.location.host +
        window.location.pathname;

    const prevButtonNumber = props.currentPageNumber - 1;
    const prevDisabled = prevButtonNumber < 1;
    const prevHref = `${baseURL}#page=${prevButtonNumber}`;

    const nextButtonNumber = props.currentPageNumber + 1;
    const nextDisabled = nextButtonNumber > props.numberOfPages;
    const nextHref = `${baseURL}#page=${nextButtonNumber}`;

    let numbers = paginationGenerator(
        props.numberOfPages,
        props.currentPageNumber,
        TOTAL_PAGES_TO_SHOW
    );

    return (
        <div className="pagination">
            <a
                className={`pagination__arrow pagination__arrow--back ${
                    prevDisabled ? "disabled" : ""
                }`}
                href={prevDisabled ? null : prevHref}
                onClick={(evt: any) => {
                    evt.preventDefault();
                    if (!prevDisabled) {
                        props.pageNumberChanged(prevButtonNumber);
                    }
                }}
            >
                <i className="icon icon-long-arrow-left"></i>
            </a>
            {numbers.map((num: number | string): React.Node => {
                if (typeof num === "string") {
                    return "...";
                }

                const query = qs.parse(window.location.hash.replace("#", ""));
                query.page = num;
                const queryString = qs.stringify(query, { encode: false });

                return (
                    <a
                        key={num}
                        className={`pagination__link ${
                            num === props.currentPageNumber
                                ? "pagination__link--active"
                                : ""
                        }`}
                        href={`${baseURL}#${queryString}`}
                        onClick={(evt: any) => {
                            evt.preventDefault();
                            props.pageNumberChanged(parseInt(num));
                        }}
                    >
                        <span className="pagination__link__num">{num}</span>
                    </a>
                );
            })}
            <a
                className={`pagination__arrow pagination__arrow--next ${
                    nextDisabled ? "disabled" : ""
                }`}
                href={nextDisabled ? null : nextHref}
                onClick={(evt: any) => {
                    evt.preventDefault();
                    if (!nextDisabled) {
                        props.pageNumberChanged(nextButtonNumber);
                    }
                }}
            >
                <i className="icon icon-long-arrow-right"></i>
            </a>
        </div>
    );
};

export default Pagination;
