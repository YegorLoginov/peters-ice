// @flow

import * as React from "react";
import TreeModal from "@treetrum/react-treemodal";

import Icon from "@components/atoms/Icon";

type PropsType = {
    isOpen: boolean,
    onClose: () => void,
    color: string,
    title: string,
    children: React.Node,
};

const PetersModal = (props: PropsType): React.Node => {
    return (
        <TreeModal
            isOpen={props.isOpen}
            onClose={props.onClose}
            overlayColor="rgba(225, 219, 197, 0.45)"
        >
            <div className="ptrs-modal">
                <div className={`ptrs-modal__header bg--${props.color}`}>
                    <h4 className="ptrs-modal__title heading-4">
                        {props.title}
                    </h4>
                    <button
                        type="button"
                        className="ptrs-modal__close"
                        onClick={props.onClose}
                    >
                        <Icon name="close"></Icon>
                    </button>
                </div>
                <div className="ptrs-modal__body">{props.children}</div>
            </div>
        </TreeModal>
    );
};

export default PetersModal;
