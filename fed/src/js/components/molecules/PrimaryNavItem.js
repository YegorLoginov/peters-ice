// @flow

import * as React from "react";
import { useHistory } from "react-router-dom";

import isLinkActive from "@utils/isLinkActive";
import PrimaryNavSubmenu from "@components/molecules/PrimaryNavSubmenu";
import Link from "@components/atoms/Link";

import { PrimaryNavItemType } from "@types";

const PrimaryNavItem = (props: {
    item: PrimaryNavItemType,
    showSubmenu: boolean,
    onClick: (event: MouseEvent) => void,
}): React.Node => {
    // Make sure the component reloads when the history updates
    useHistory();

    // Link with children
    if (props.item.has_children) {
        return (
            <>
                <li className="primary-nav-item">
                    <button
                        className="primary-nav-item__link"
                        onClick={props.onClick}
                    >
                        {props.item.label}
                        <i className="icon icon-chevron-down"></i>
                    </button>
                    <PrimaryNavSubmenu
                        links={props.item.children}
                        active={props.showSubmenu}
                        className={`primary-nav-submenu--${props.item.label}`}
                    />
                </li>
            </>
        );
    }

    // Standard link
    return (
        <li
            className={`primary-nav-item ${
                isLinkActive(props.item.link.url) ? "active" : ""
            }`}
        >
            <Link className="primary-nav-item__link" to={props.item.link.url}>
                {props.item.link.title}
            </Link>
        </li>
    );
};

PrimaryNavItem.defaultProps = {
    onClick: () => {},
    showSubmenu: false,
};

export default PrimaryNavItem;
