// @flow

import * as React from "react";

import Link from "@components/atoms/Link";

import { ChildLinkType } from "@types";

type PropsType = {
    links: ChildLinkType[],
    active: boolean,
    className?: string,
};

const PrimaryNavSubmenu = (props: PropsType): React.Node => {
    const classList = ["primary-nav-submenu"];
    if (props.active) {
        classList.push("active");
    }
    if (props.className) {
        classList.push(props.className);
    }
    return (
        <ul className={classList.join(" ")}>
            {props.links.map(({ link }, index): React.Node => (
                <li
                    className="primary-nav-submenu__item"
                    key={index}
                    style={{
                        transitionDelay: props.active
                            ? `${index * 100}ms`
                            : null,
                    }}
                >
                    <Link className="primary-nav-submenu__link" to={link.url}>
                        {link.title}
                    </Link>
                </li>
            ))}
        </ul>
    );
};

export default PrimaryNavSubmenu;
