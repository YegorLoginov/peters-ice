// @flow

import * as React from "react";

import Image from "@components/atoms/Image";
import Link from "@components/atoms/Link";

import { ImageType } from "@types";

type PropsType = {
    colour: string,
    image: ImageType,
    url: string,
    brand: string,
    title: string,
    tags?: Array<{ name: string, slug: string }>,
    variation: "regular" | "featured",
};

const ProductTile = (props: PropsType): React.Node => {
    const classes = ["product-tile"];
    classes.push(`product-tile--${props.variation}`);

    const tagsToShow = props.tags ? props.tags.slice(0, 3) : [];
    const remainingTagsCount = (props.tags ? props.tags.slice(3) : []).length;
    return (
        <Link to={props.url} className={classes.join(" ")}>
            <div className="product-tile__top">
                <div className={`product-tile__bg bg--${props.colour}`}></div>
                <div className="product-tile__img">
                    {props.image && (
                        <Image size="medium_large" image={props.image}></Image>
                    )}
                </div>
            </div>
            <div className="product-tile__bottom">
                <h3 className="product-tile__brand">{props.brand}</h3>
                <h4 className="product-tile__title">{props.title}</h4>
                {props.tags && props.tags.length > 0 && (
                    <div className="product-tile__tags">
                        {tagsToShow.map((tag): React.Node => (
                            <p key={tag.slug} className="product-tile__tag">
                                {tag.name}
                            </p>
                        ))}
                        {remainingTagsCount > 0 && (
                            <div className="product-tile__more">
                                +{remainingTagsCount}
                            </div>
                        )}
                    </div>
                )}
            </div>
        </Link>
    );
};

ProductTile.defaultProps = {
    variation: "regular",
};

export default ProductTile;
