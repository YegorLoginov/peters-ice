// @flow

import * as React from "react";
import ReactDOMServer from "react-dom/server";

import wrapEl from "@utils/wrapEl";
import scrollToEl from "@utils/scrollToEl";
import CtaArrow from "@components/atoms/CtaArrow";

type PropsType = {
    html: string,
    className?: string,
};

const RichText = (props: PropsType): React.Node => {
    const htmlRef = React.useRef(null);
    const classList = ["rich-text"];
    if (props.className) {
        classList.push(props.className);
    }

    // Modify contents of HTML as necessary
    React.useEffect(() => {
        if (htmlRef.current) {
            const ref = htmlRef.current;

            // Add wrapper around all tables
            const tables = ref.querySelectorAll("table");
            for (const table of tables) {
                const outer = document.createElement("div");
                const inner = document.createElement("div");
                outer.classList.add("table-wrapper");
                inner.classList.add("table-wrapper__inner");
                wrapEl(table, outer);
                wrapEl(table, inner);
            }

            // Add anchor scrolling behaviour
            ref.addEventListener("click", (event: MouseEvent) => {
                const { target } = event;
                if (
                    target instanceof HTMLAnchorElement &&
                    target.matches('a[href^="#"]')
                ) {
                    const id = "#" + target.href.split("#")[1];
                    const el = document.querySelector(id);
                    if (el) {
                        scrollToEl(el);
                        event.preventDefault();
                    }
                }
            });

            // Add responsive wrapper around iframes
            const iframes = ref.querySelectorAll("iframe");
            for (const iframe of iframes) {
                const outer = document.createElement("div");
                outer.classList.add("iframe-wrapper");
                wrapEl(iframe, outer);
            }

            // Insert spans inside <li>
            const listItems = ref.querySelectorAll("ol li, ul li");
            for (const item of listItems) {
                item.innerHTML = `<span>${item.innerHTML}</span>`;
                item.classList.add("inner-added");
            }

            // Add arrows to CTAs
            const ctas = ref.querySelectorAll(".cta");
            for (const cta of ctas) {
                cta.innerHTML = `${
                    cta.innerHTML
                }${ReactDOMServer.renderToString(<CtaArrow />)}`;
            }
        }
    }, [props.html]);

    return (
        <div
            ref={htmlRef}
            className={classList.join(" ")}
            dangerouslySetInnerHTML={{ __html: props.html }}
        />
    );
};

RichText.defaultProps = {
    backgroundColor: "white",
};

export default RichText;
