// @flow

import * as React from "react";
import Accordion, { type AccordionType } from "@components/molecules/Accordion";
import EyeBrow from "@components/atoms/EyeBrow";

type PropsType = {
    title: string,
    eyebrow: string,
    accordions: AccordionType[],
};

const AccordionsSection = (props: PropsType): React.Node => {
    return (
        <div className="accordions-section">
            <div className="container-fluid">
                <div className="accordions-section__intro">
                    <EyeBrow>{props.eyebrow}</EyeBrow>
                    <h3 className="accordions-section__title heading-3">
                        {props.title}
                    </h3>
                </div>
                <div className="accordions-section__accordions">
                    {props.accordions.map((accordion, index): React.Node => (
                        <Accordion key={index} accordion={accordion} />
                    ))}
                </div>
            </div>
        </div>
    );
};

AccordionsSection.defaultProps = {};

export default AccordionsSection;
