// @flow

import * as React from "react";

import BrandTile from "@components/molecules/BrandTile";

type PropsType = {
    posts: Array<any>,
    colClass: string,
    renderItem: (brand: any) => React.Node,
};

const BrandGrid = ({ posts, colClass, ...props }: PropsType): React.Node => {
    if (!posts || posts.length === 0) {
        return null;
    }
    return (
        <div className="brand-grid">
            <div className="brand-grid__items">
                {posts.map((post): React.Node => (
                    <div className="brand-grid__item" key={post.ID || post.id}>
                        {props.renderItem(post)}
                    </div>
                ))}
            </div>
        </div>
    );
};

BrandGrid.defaultProps = {
    colClass: "col-xs-6 col-md-3",
    renderItem: (brand): React.Node => <BrandTile brand={brand} />,
};

export default BrandGrid;
