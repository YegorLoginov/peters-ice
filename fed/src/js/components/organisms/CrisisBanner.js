// @flow

import * as React from "react";
import { useSelector } from "react-redux";
import RichText from "@components/molecules/RichText";

type CrisisBannerType = {
    content: string,
    enabled: boolean,
    type: "standard" | "custom",
    colour: [{ colour: string }],
};

const CrisisBanner = (): React.Node => {
    const data = useSelector(
        (state): CrisisBannerType => state.options.crisis_banner
    );
    if (!data.enabled) {
        return null;
    }

    const classes = ["crisis-banner"];
    classes.push(`crisis-banner--${data.type}`);
    if (data.type === "custom") {
        classes.push(`bg--${data.colour[0].colour}`);
    }

    return (
        <div className={classes.join(" ")}>
            <div className="container-fluid">
                <div className="crisis-banner__inner">
                    <i className="icon icon-info"></i>
                    <RichText html={data.content} />
                </div>
            </div>
        </div>
    );
};

export default CrisisBanner;
