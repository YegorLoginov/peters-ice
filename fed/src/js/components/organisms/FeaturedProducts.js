// @flow

import * as React from "react";

import usePageFromStore from "@hooks/usePageFromStore";
import Icon from "@components/atoms/Icon";
import Button from "@components/atoms/Button";
import { LinkType } from "@types";
import addACFData from "@utils/addACFData";
import ProductGrid from "@components/organisms/ProductGrid";

type PropsType = {};

type FeaturedProductsType = {
    title: string,
    subtitle: string,
    products: { product: any }[],
    cta: LinkType,
};

const FeaturedProducts = (props: PropsType): React.Node => {
    const featuredProducts: FeaturedProductsType = usePageFromStore().page.acf
        .featured_products;

    return (
        <div className="featured-products">
            <div className="container-fluid">
                <div className="featured-products__icon">
                    <Icon name="ice-cream" />
                </div>
                <div className="featured-products__intro">
                    <h2 className="heading-1">{featuredProducts.title}</h2>
                    <p className="body">{featuredProducts.subtitle}</p>
                </div>
                <ProductGrid
                    products={featuredProducts.products
                        .map(({ product }): any => product)
                        .map(addACFData)}
                />
                <div className="featured-products__cta">
                    <Button link={featuredProducts.cta} />
                </div>
            </div>
        </div>
    );
};

export default FeaturedProducts;
