// @flow

import * as React from "react";
import { useSelector } from "react-redux";

import Link from "@components/atoms/Link";
import { LinkType } from "@types";
import Icon from "@components/atoms/Icon";

import froneriLogo from "@assets/images/froneri-logo.svg";

type PropsType = {};

type FooterDataType = {
    copyright: "",
    privacy_link: LinkType,
    terms_link: LinkType,
    socials: {
        facebook: string,
        youtube: string,
    },
};

const Footer = (props: PropsType): React.Node => {
    const data = useSelector((state): FooterDataType => state.options.footer);
    const year = new Date().getFullYear();
    return (
        <footer className="footer gap">
            <div className="container-fluid">
                <div className="footer__inner">
                    <div className="footer__items">
                        <div className="footer__item">
                            <Link to="https://www.froneri.com/" target="_blank">
                                <img
                                    src={froneriLogo}
                                    alt="Froneri"
                                    className="footer__froneri-logo"
                                />
                            </Link>
                        </div>
                        <div className="footer__item">
                            <p className="body-12">
                                {data.copyright.replace("{{year}}", year)}
                            </p>
                        </div>
                        <div className="footer__item">
                            <p className="body-12">
                                <Link to={data.privacy_link.url}>
                                    {data.privacy_link.title}
                                </Link>
                            </p>
                        </div>
                        <div className="footer__item">
                            <p className="body-12">
                                <Link to={data.terms_link.url}>
                                    {data.terms_link.title}
                                </Link>
                            </p>
                        </div>
                    </div>

                    <div className="footer__socials">
                        <Footer.SocialLink
                            name="facebook"
                            url={data.socials.facebook}
                        />
                        <Footer.SocialLink
                            name="youtube"
                            url={data.socials.youtube}
                        />
                    </div>
                </div>
            </div>
        </footer>
    );
};

Footer.SocialLink = (props: { name: string, url: string }): React.Node => {
    return (
        <a
            className="footer__socials__link"
            href={props.url}
            target="_blank"
            rel="noreferrer noopener"
            aria-label={props.name}
        >
            <Icon name={props.name}></Icon>
        </a>
    );
};

export default Footer;
