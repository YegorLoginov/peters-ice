// @flow

import * as React from "react";

import ReCAPTCHA from "@components/atoms/ReCAPTCHA";
import Button from "@components/atoms/Button";
import { ReactForm, ReactFormField } from "@adrenalin/react-form";
import RichText from "@components/molecules/RichText";
import useAsyncState from "@hooks/useAsyncState";
import GravityAPI from "@api/GravityAPI";
import FileUploadComponent from "@components/molecules/FileUploadComponent";
import { GravityFormType, GravityFormFieldType } from "@types";
import Config from "@Config";

type PropsType = {
    form: GravityFormType,
    fileUploadPageId: string,
    recaptchaSiteKey: string,
    onChange?: (values: any) => void,
};

// @flow

export const dateValidator = {
    ID: "custom-date",
    validate: (input: any): boolean => {
        var regex = new RegExp(Config.regex.date);
        return regex.test(input.value);
    },
    error: `Please enter a valid date in dd/mm/yyyy format`,
};

const getInputType = (gravityType: string): string => {
    const fieldTypeMap = {
        fileupload: "file",
        date: "text",
        time: "text",
        email: "email",
        phone: "tel",
    };
    return fieldTypeMap[gravityType] || gravityType;
};

const getValidators = (field: GravityFormFieldType): string[] => {
    const validators = [];
    if (field.type === "email") {
        validators.push("email");
    }
    if (field.type === "phone") {
        //validators.push("mobile");
    }
    if (field.type === "fileupload" && field.maxFileSize) {
        validators.push(`maxFileSize:${field.maxFileSize}`);
    }
    if (field.type === "date") {
        validators.push("custom-date");
    }
    if (field.maxLength && !isNaN(field.maxLength)) {
        validators.push(`maxLength:${field.maxLength}`);
    }
    return validators;
};

const GravityForm = (props: PropsType): React.Node => {
    const captchaRef = React.useRef<ReCAPTCHA>(null);

    const [statusText, setStatusText] = React.useState();
    const [asyncState, actions] = useAsyncState();

    const gravityAPI = React.useRef<GravityAPI>(
        new GravityAPI(
            props.form.id,
            Math.random()
                .toString(36)
                .substr(2, 8),
            props.fileUploadPageId
        )
    ).current;

    const formRef = React.useRef(null);

    if (!gravityAPI) return null;

    const handleSubmit = async (values: any = {}): Promise<any> => {
        try {
            setStatusText("Sending your message.. please wait!");
            actions.request();

            // Get the recaptcha token
            let recaptchaCode = null;
            if (captchaRef.current) {
                recaptchaCode = await captchaRef.current.execute();
            }

            // Upload all files before subitting form
            let uploadedFiles = {};
            for (const [inputName, inputValue] of Object.entries(values)) {
                if (inputValue instanceof FileList) {
                    for (const file of inputValue) {
                        const inputId = inputName.replace("input_", "");
                        // setButtonText(`Uploading atachments...`);
                        const responseJSON = await gravityAPI.uploadFile(
                            file,
                            inputId
                        );
                        if (!uploadedFiles.hasOwnProperty(inputName)) {
                            uploadedFiles[inputName] = [];
                        }
                        if (responseJSON.status === "error") {
                            // If a file upload fails, cancel the whole submission
                            actions.fail({
                                validation_messages: {
                                    [inputId]: `Could not upload the file ${file.name}`,
                                },
                            });
                            setStatusText(null);
                            return;
                        }
                        uploadedFiles[inputName].push(responseJSON.data);
                    }
                }
            }

            // setButtonText("Submitting");

            // Delete any 'standard' file inputs
            // They will all be uploaded under the 'gform_uploaded_files' key
            const valuesToUpload = { ...values };
            for (let [key, value] of Object.entries(values)) {
                if (value instanceof FileList) {
                    delete valuesToUpload[key];
                }
            }
            valuesToUpload["gform_uploaded_files"] = JSON.stringify(
                uploadedFiles
            );

            // Add the recaptcha code
            if (recaptchaCode) {
                valuesToUpload["g-recaptcha-response"] = recaptchaCode;
            }

            // Submit the form
            const response = await gravityAPI.submit(valuesToUpload);
            if (response.is_valid) {
                actions.success(true);
            } else {
                actions.fail(response);
            }
            setStatusText(null);
        } catch (error) {
            console.error(error);
            actions.fail(error);
        }
    };

    if (asyncState.success) {
        return (
            <div className="gravity-form-success">
                <i className="gravity-form-success__icon icon icon-tick-circle"></i>
                <h3 className="gravity-form-success__title heading-3">
                    Thank you for submitting the form
                </h3>
            </div>
        );
    }

    return (
        <div
            className="gravity-form"
            style={{
                opacity: asyncState.loading ? 0.5 : null,
                transition: "200ms ease all",
            }}
        >
            {asyncState.error && (
                <div className="gravity-form__errors">
                    {asyncState.error.validation_messages ? (
                        <>
                            <p>
                                The following errors occurred when submitting
                                the form:
                            </p>
                            <ul>
                                {Object.entries(
                                    asyncState.error.validation_messages
                                ).map(([fieldId, error], index): React.Node => {
                                    const input = props.form.fields.find(
                                        (f): boolean =>
                                            String(f.id) === String(fieldId)
                                    );
                                    return (
                                        <li key={index}>
                                            <strong>
                                                {input && input.label + ": "}
                                            </strong>
                                            {String(error)}
                                        </li>
                                    );
                                })}
                            </ul>
                        </>
                    ) : (
                        <>
                            <p>
                                <strong>Unknown error:</strong>
                            </p>
                            {/* Fallback to showing the raw error */}
                            <code>
                                <pre>
                                    {JSON.stringify(asyncState.error, null, 2)}
                                </pre>
                            </code>
                        </>
                    )}
                </div>
            )}
            <ReactForm
                data-form-id={props.form.id}
                ref={formRef}
                onSuccess={handleSubmit}
                validators={[dateValidator]}
                onChange={(...args) => {
                    if (formRef.current) {
                        const form = formRef.current;
                        if (props.onChange) {
                            const onChange = props.onChange;
                            onChange(form.prepareFormData());
                        }
                    }
                }}
            >
                {props.form.fields.map((field): React.Node => {
                    const required =
                        field.isRequired === "1" || field.isRequired === true;
                    if (field.type === "html") {
                        return <RichText key={field.id} html={field.content} />;
                    }
                    if (field.type === "captcha") {
                        return (
                            <ReCAPTCHA
                                key={field.id}
                                ref={captchaRef}
                                sitekey={props.recaptchaSiteKey}
                                size="invisible"
                            />
                        );
                    }
                    if (field.type === "time" && field.inputs.length > 0) {
                        const labelId = `label-${field.id}`;
                        return (
                            <div
                                key={field.id}
                                className={`gravity-form__field-with-children ${field.cssClass}`}
                            >
                                <div
                                    id={labelId}
                                    className="gravity-form__field-with-children__label"
                                >
                                    {field.label}
                                </div>
                                <div className="gravity-form__field-with-children__children">
                                    {field.inputs
                                        .slice(0, 2)
                                        .map((input, index): React.Node => {
                                            const fieldId = `input_${field.id}[${index}]`;
                                            const validators = [
                                                ">=:0",
                                                `<=:${
                                                    index === 0 ? "24" : "60"
                                                }`,
                                            ];
                                            return (
                                                <ReactFormField
                                                    key={input.id}
                                                    name={fieldId}
                                                    id={fieldId}
                                                    placeholder={
                                                        input.placeholder
                                                    }
                                                    type="text"
                                                    info={
                                                        input.customLabel ||
                                                        input.label
                                                    }
                                                    errorMessages={{
                                                        ">=":
                                                            "Please enter a valid time format",
                                                        "<=":
                                                            "Please enter a valid time format",
                                                    }}
                                                    validators={validators}
                                                    label={field.label}
                                                />
                                            );
                                        })}
                                </div>
                            </div>
                        );
                    }
                    return (
                        <ReactFormField
                            key={field.id}
                            name={`input_${field.id}`}
                            id={`input_${field.id}`}
                            required={required}
                            type={getInputType(field.type)}
                            placeholder={field.placeholder}
                            label={field.label}
                            className={`${required ? "required" : ""} ${
                                field.cssClass
                            }`}
                            validators={getValidators(field)}
                            fieldComponent={
                                field.type === "fileupload"
                                    ? FileUploadComponent
                                    : null
                            }
                            errorMessages={{
                                maxFileSize: field.maxFileSize
                                    ? `Each file must be smaller than ${field.maxFileSize}MB`
                                    : null,
                            }}
                            data-max-file-size={field.maxFileSize}
                            accept={
                                field.type === "fileupload"
                                    ? field.allowedExtensions
                                          .split(", ")
                                          .map((a): string => "." + a)
                                          .join(", ")
                                    : null
                            }
                            info={field.description}
                        >
                            {field.choices
                                ? field.choices.map(
                                      (choice, index): React.Node => {
                                          return (
                                              <option
                                                  key={choice.id || index}
                                                  value={choice.value}
                                              >
                                                  {choice.text}
                                              </option>
                                          );
                                      }
                                  )
                                : null}
                        </ReactFormField>
                    );
                })}
                <div className="gravity-form__submit">
                    <Button type="submit">Submit</Button>
                </div>
                {statusText && (
                    <div className="gravity-form__status">{statusText}</div>
                )}
            </ReactForm>
        </div>
    );
};

export default GravityForm;
