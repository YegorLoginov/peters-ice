// @flow

import * as React from "react";

import ResponsiveImage, {
    type ResponsiveImageType,
} from "@components/atoms/ResponsiveImage";

type PropsType = {
    image: ResponsiveImageType,
    type: "simple" | "rich",
};

const HeroBrandHub = (props: PropsType): React.Node => {
    const classes = ["hero-brand-hub"];
    classes.push(`hero-brand-hub--${props.type}`);
    return (
        <div className={classes.join(" ")}>
            <ResponsiveImage
                className="hero-brand-hub__image"
                image={props.image}
                breakpoint="sm"
                sizes={{ desktop: "1536x1536", mobile: "large" }}
            />
        </div>
    );
};

export default HeroBrandHub;
