// @flow

import * as React from "react";
import usePageFromStore from "@hooks/usePageFromStore";
import { useSelector } from "react-redux";

import Image from "@components/atoms/Image";
import EyeBrow from "@components/atoms/EyeBrow";
import Button from "@components/atoms/Button";
import CtaArrow from "@components/atoms/CtaArrow";
import Link from "@components/atoms/Link";
import ResponsiveImage from "@components/atoms/ResponsiveImage";

import { ImageType, LinkType } from "@types";

type PropsType = {};

type SecondaryType = {
    eyebrow: string,
    image: ImageType,
    link: LinkType,
    background_colour: { colour: string }[],
};

type ResponsiveImageType = Array<{ desktop: ImageType, mobile: ImageType }>;

type HeroType = {
    primary_feature: {
        title: string,
        reverse_text_colour: boolean,
        image: ResponsiveImageType,
        cta: LinkType,
    },
    secondary_feature_1: SecondaryType,
    secondary_feature_2: SecondaryType,
};

const HeroHome = (props: PropsType): React.Node => {
    const hero: HeroType = usePageFromStore().page.acf.hero;
    const primary = hero.primary_feature;
    const secondary_1 = hero.secondary_feature_1;
    const secondary_2 = hero.secondary_feature_2;

    const renderSecondary = (
        feature: SecondaryType,
        flipped: boolean = false
    ): React.Node => {
        const classlist = ["hero-home__secondary"];
        if (flipped) {
            classlist.push("flipped");
        }
        return (
            <Link to={feature.link.url} className={classlist.join(" ")}>
                <div className={`hero-home__secondary__outer`}>
                    <div
                        className={`hero-home__secondary__inner ${
                            flipped ? "flipped" : ""
                        }`}
                    >
                        <div className="hero-home__secondary__image">
                            <Image image={feature.image} size="medium_large" />
                        </div>
                        <div
                            className={`hero-home__secondary__content ${`bg--${feature.background_colour[0].colour}`}`}
                        >
                            <div className="hero-home__secondary__content__inner">
                                <EyeBrow>{feature.eyebrow}</EyeBrow>
                                <h2 className="hero-home__secondary__title">
                                    {feature.link.title}
                                </h2>
                                <CtaArrow></CtaArrow>
                            </div>
                        </div>
                    </div>
                </div>
            </Link>
        );
    };

    const hasBanner = useSelector(
        (state): boolean => state.options.crisis_banner.enabled
    );

    return (
        <div
            className={`hero-home hero-home--${hasBanner ? "has-banner" : ""}`}
        >
            <div className="hero-home__outer">
                <div className="hero-home__inner">
                    <Link to={primary.cta.url} className={"hero-home__primary"}>
                        <ResponsiveImage
                            className="hero-home__primary__img"
                            image={primary.image}
                            sizes={{ desktop: "1536x1536", mobile: "large" }}
                        />
                        <h1
                            className={`hero-home__primary__title ${
                                primary.reverse_text_colour ? "reverse" : ""
                            }`}
                        >
                            {primary.title}
                        </h1>
                        <Button>{primary.cta.title}</Button>
                    </Link>
                    <div className="hero-home__secondaries">
                        {renderSecondary(secondary_1)}
                        {renderSecondary(secondary_2, true)}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default HeroHome;
