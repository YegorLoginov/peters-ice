// @flow

import * as React from "react";

import Image from "@components/atoms/Image";
import { getFeaturedImage, getExcerpt } from "@utils/post";
import addACFData from "@utils/addACFData";
import Link from "@components/atoms/Link";
import WTBAvailableAtButtonModal from "@components/organisms/WTBAvailableAtButtonModal";
import { type AcfType as AcfBrandTYpe } from "@containers/BrandHub";

type PropsType = {
    product: any,
};

const HeroProductDetail = ({ product }: PropsType): React.Node => {
    const { colour } = product.acf.colour[0];
    const brand = addACFData(product.acf.brand);
    const brandACF: AcfBrandTYpe = brand.acf;
    const classes = ["hero-product-detail"];
    classes.push(`bg--${colour}`);
    return (
        <div className={classes.join(" ")}>
            <div className="container-fluid">
                <div className="hero-product-detail__inner">
                    <div className="hero-product-detail__img">
                        <Image image={getFeaturedImage(product)} size="large" />
                    </div>
                    <div className="hero-product-detail__content">
                        <Link to={brand.url}>
                            <Image
                                className="hero-product-detail__logo"
                                image={brandACF.brand_logo}
                                size="medium_large"
                            />
                        </Link>
                        <h1 className="hero-product-detail__title heading-2">
                            {product.post_title}
                        </h1>
                        {product.acf.collection_label && (
                            <h3 className="hero-product-detail__collection">
                                {product.acf.collection_label}
                            </h3>
                        )}
                        <p className="body-20">{getExcerpt(product)}</p>
                        <ul className="hero-product-detail__tags">
                            {product.tags_input.map((tag): React.Node => (
                                <li
                                    key={tag}
                                    className="hero-product-detail__tag"
                                >
                                    {tag}
                                </li>
                            ))}
                        </ul>
                        <WTBAvailableAtButtonModal
                            brandACF={brandACF}
                            color={colour}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default HeroProductDetail;
