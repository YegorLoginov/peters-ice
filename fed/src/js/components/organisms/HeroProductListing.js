// @flow

import * as React from "react";
import {
    getFeaturedImage,
    getResponsiveFeaturedImage,
    getExcerpt,
} from "@utils/post";

import CTA from "@components/atoms/CTA";
import ResponsiveImage from "@components/atoms/ResponsiveImage";

type PropsType = {
    page: any,
    hasOffset: boolean,
};

const HeroProductListing = (props: PropsType): React.Node => {
    const excerpt = getExcerpt(props.page);
    const cta = props.page.acf ? props.page.acf.meta[0].hero_cta : null;

    const classes = ["hero-product-listing"];
    if (props.hasOffset) {
        classes.push("hero-product-listing--has-offset");
    }

    const hasFeaturedImage = !!getFeaturedImage(props.page, false);

    return (
        <div className={classes.join(" ")}>
            {hasFeaturedImage && (
                <ResponsiveImage
                    className="hero-product-listing__bg"
                    image={getResponsiveFeaturedImage(props.page)}
                    sizes={{ desktop: "1536x1536", mobile: "large" }}
                />
            )}
            <div className="container-fluid">
                <div className="hero-product-listing__inner">
                    <h1 className="heading-1 hero-product-listing__title">
                        {props.page.post_title}
                    </h1>
                    {excerpt && (
                        <p className="body hero-product-listing__subtitle">
                            {excerpt}
                        </p>
                    )}
                    {cta && (
                        <CTA className="hero-product-listing__cta" link={cta} />
                    )}
                </div>
            </div>
        </div>
    );
};

HeroProductListing.defaultProps = {
    hasOffset: true,
};

export default HeroProductListing;
