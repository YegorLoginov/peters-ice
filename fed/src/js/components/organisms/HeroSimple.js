// @flow

import * as React from "react";
import { getExcerpt, getFeaturedImage } from "@utils/post";
import Image from "@components/atoms/Image";

type PropsType = {
    page: any,
};

const HeroSimple = (props: PropsType): React.Node => {
    const excerpt = getExcerpt(props.page);
    const image = getFeaturedImage(props.page, false);
    const classes = ["hero-simple"];
    if (!image) {
        classes.push("hero-simple--no-image");
    }
    return (
        <div className={classes.join(" ")}>
            <div className="container-fluid">
                <div className="hero-simple__outer">
                    <div className="hero-simple__inner">
                        <div className="hero-simple__left">
                            <h1 className="hero-simple__title heading-1">
                                {props.page.post_title}
                            </h1>
                            {excerpt && (
                                <p className="body hero-simple__blurb">
                                    {getExcerpt(props.page)}
                                </p>
                            )}
                        </div>
                        {image && (
                            <div className="hero-simple__right">
                                <div className="hero-simple__image">
                                    <Image image={image} size="large" />
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default HeroSimple;
