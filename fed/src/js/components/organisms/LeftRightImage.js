// @flow

import * as React from "react";

import Image from "@components/atoms/Image";
import { ContentBlockType } from "@types";
import EyeBrow from "@components/atoms/EyeBrow";
import CTA from "@components/atoms/CTA";

function getYoutubeIdFromURL(url: string): string {
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
    var match = url.match(regExp);
    return match && match[7].length === 11 ? match[7] : "";
}

const LeftRightImage = (props: {
    content: ContentBlockType,
    variation: "left" | "right",
}): React.Node => {
    const classes = ["left-right-image"];
    classes.push(`left-right-image--${props.variation}`);
    classes.push(`left-right-image--type-${props.content.type}`);
    return (
        <div className={classes.join(" ")}>
            <div className="container-fluid">
                <div className="left-right-image__inner">
                    <div className="left-right-image__image">
                        <div className="left-right-image__image__inner">
                            {props.content.type === "youtube" ? (
                                <iframe
                                    title={getYoutubeIdFromURL(
                                        props.content.video_link
                                    )}
                                    src={`https://www.youtube.com/embed/${getYoutubeIdFromURL(
                                        props.content.video_link
                                    )}?enablejsapi=1`}
                                    frameBorder="0"
                                />
                            ) : (
                                <Image
                                    image={props.content.image}
                                    size="large"
                                ></Image>
                            )}
                        </div>
                    </div>
                    <div className="left-right-image__content">
                        <div className="left-right-image__content__inner">
                            {props.content.eyebrow && (
                                <EyeBrow className="left-right-image__eyebrow">
                                    {props.content.eyebrow}
                                </EyeBrow>
                            )}
                            <h3 className="left-right-image__title heading-3">
                                {props.content.title}
                            </h3>
                            <p className="left-right-image__description body-16-20">
                                {props.content.description}
                            </p>
                            {props.content.cta && (
                                <CTA
                                    className="left-right-image__cta"
                                    link={props.content.cta}
                                ></CTA>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LeftRightImage;
