// @flow

import * as React from "react";

import Link from "@components/atoms/Link";
import AnimateHeight from "@components/atoms/AnimateHeight";
import LockBodyScroll from "@components/atoms/LockBodyScroll";

import { PrimaryNavItemType } from "@types";
import { useSelector } from "react-redux";

const MobileMenu = (props: {
    links: PrimaryNavItemType[],
    active: boolean,
}): React.Node => {
    const [activeSubmenu, setActiveSubmenu] = React.useState<?number>(null);
    const showMenu = useSelector((state): boolean => state.ui.showMobileMenu);
    return (
        <div className={`mobile-menu ${showMenu ? "is-open" : ""}`}>
            {showMenu && <LockBodyScroll />}
            <ul className="mobile-menu__menu">
                {props.links.map((link, index): React.Node => {
                    return (
                        <li
                            key={index}
                            className={`mobile-menu__item ${
                                activeSubmenu === index ? "active" : ""
                            }`}
                            style={{
                                transitionDelay: showMenu
                                    ? `${index * 100 + 300}ms`
                                    : null,
                            }}
                        >
                            {link.has_children ? (
                                <>
                                    <button
                                        className="mobile-menu__link"
                                        onClick={() => {
                                            if (activeSubmenu === index) {
                                                setActiveSubmenu(null);
                                            } else {
                                                setActiveSubmenu(index);
                                            }
                                        }}
                                    >
                                        {link.label}
                                        <i className="icon icon-chevron-down"></i>
                                    </button>
                                    <AnimateHeight hideOverflow duration={350}>
                                        <ul className={`mobile-menu__submenu`}>
                                            {link.children.map(
                                                (child, index): React.Node => (
                                                    <li
                                                        className="mobile-menu__submenu__item"
                                                        key={index}
                                                    >
                                                        <Link
                                                            to={child.link.url}
                                                        >
                                                            {child.link.title}
                                                        </Link>
                                                    </li>
                                                )
                                            )}
                                        </ul>
                                    </AnimateHeight>
                                </>
                            ) : (
                                <Link
                                    className="mobile-menu__link"
                                    to={link.link.url}
                                >
                                    {link.link.title}
                                </Link>
                            )}
                        </li>
                    );
                })}
            </ul>
        </div>
    );
};

export default MobileMenu;
