// @flow

import * as React from "react";

type PropsType = {
    isOpen: boolean,
    children: React.Node,
    onClose: () => void,
};

const Modal = (props: PropsType): React.Node => {
    React.useEffect((): any => {
        if (props.isOpen) {
            const handleKeyDown = (event: KeyboardEvent) => {
                if (event.key === "Escape") {
                    props.onClose();
                }
            };
            const handleWindowClick = (event: MouseEvent) => {
                if (event.target instanceof HTMLElement) {
                    if (!event.target.matches(".modal__inner *")) {
                        props.onClose();
                    }
                }
            };
            window.addEventListener("click", handleWindowClick);
            window.addEventListener("keydown", handleKeyDown);
            return () => {
                window.removeEventListener("click", handleWindowClick);
                window.removeEventListener("keydown", handleKeyDown);
            };
        }
    }, [props.isOpen]);

    const classes = ["modal"];
    if (props.isOpen) {
        classes.push("is-open");
    }

    return (
        <div className={classes.join(" ")}>
            <div className="modal__inner">{props.children}</div>
        </div>
    );
};

export default Modal;
