// @flow

import * as React from "react";

import Button from "@components/atoms/Button";
import ProductGrid from "@components/organisms/ProductGrid";
import useProductsFromBrand from "@hooks/useProductsFromBrand";

type PropsType = {
    page: any,
};

const MoreFromBrand = ({ page }: PropsType): React.Node => {
    const { brand } = page.acf;
    const products = useProductsFromBrand(brand.ID, page.ID, 99);
    if (products.length < 1) {
        return null;
    }
    return (
        <div className="more-from-brand">
            <div className="container-fluid">
                <div className="more-from-brand__inner">
                    <div className="more-from-brand__title">
                        <h2 className="heading-2">
                            More from {brand.post_title}
                        </h2>
                    </div>
                    <div className="more-from-brand__content">
                        <ProductGrid showTags={true} products={products} />
                    </div>
                    <div className="more-from-brand__cta">
                        <Button variation="secondary" to="/brands">
                            See all our brands
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default MoreFromBrand;
