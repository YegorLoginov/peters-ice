// @flow

import * as React from "react";

type PropsType = {
    children: React.Node,
};

const ProductDetailContent = (props: PropsType): React.Node => {
    return (
        <div className="page-content">
            <div className="container-fluid">
                <div className="page-content__inner">{props.children}</div>
            </div>
        </div>
    );
};

ProductDetailContent.Section = (props: {
    children: React.Node,
    classModifier?: string,
}): React.Node => {
    return (
        <div
            className={`page-content__section page-content__section--${props.classModifier ||
                ""}`}
        >
            <div className="page-content__section__inner">{props.children}</div>
        </div>
    );
};

export default ProductDetailContent;
