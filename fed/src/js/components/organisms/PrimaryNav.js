// @flow

import * as React from "react";

import PrimaryNavItem from "@components/molecules/PrimaryNavItem";
import Link from "@components/atoms/Link";
import Hamburger from "@components/atoms/Hamburger";
import petersLogo from "@assets/images/peters-logo.svg";

import { PrimaryNavItemType } from "@types";
import { useDispatch } from "react-redux";
import actions from "@actions/index";
import { useLocation } from "react-router-dom";

type PropsType = {
    links: PrimaryNavItemType[],
};

const PrimaryNav = (props: PropsType): React.Node => {
    const dispatch = useDispatch();
    const location = useLocation();

    const [burgerActive, setBurgerActive] = React.useState(false);
    const [activeSubmenu, setActiveSubmenu] = React.useState<?number>(null);
    const leftLinks = props.links.slice(0, 3);
    const rightLinks = props.links.slice(3, 6);

    // Close submenu if window clicked after opening submenu
    React.useEffect((): any => {
        const handle = () => {
            setActiveSubmenu(null);
        };
        if (activeSubmenu !== null) {
            window.addEventListener("click", handle);
            return () => {
                window.removeEventListener("click", handle);
            };
        }
    }, [activeSubmenu]);
    React.useEffect((): any => {
        dispatch(actions.closeMobileMenu());
        setBurgerActive(false);
    }, [location.pathname]);

    const renderMenu = (
        links: PrimaryNavItemType[],
        position: "left" | "right",
        indexOffset: number = 0
    ): React.Node => {
        const classList = ["primary-nav__menu"];
        classList.push(`primary-nav__menu--${position}`);
        return (
            <ul className={classList.join(" ")}>
                {links.map((item, idx): React.Node => {
                    const index = idx + indexOffset;
                    return (
                        <PrimaryNavItem
                            key={index}
                            item={item}
                            showSubmenu={activeSubmenu === index}
                            onClick={event => {
                                event.stopPropagation();
                                if (activeSubmenu === index) {
                                    setActiveSubmenu(null);
                                } else {
                                    setActiveSubmenu(index);
                                }
                            }}
                        />
                    );
                })}
            </ul>
        );
    };

    return (
        <>
            <nav className="primary-nav">
                <div className="primary-nav__inner">
                    {renderMenu(leftLinks, "left")}
                    <div className="primary-nav__logo">
                        <Link to="/">
                            <img src={petersLogo} alt="Peters Ice Cream Logo" />
                        </Link>
                    </div>
                    <button
                        aria-label="Open menu"
                        className="primary-nav__hamburger active"
                        onClick={() => {
                            dispatch(actions.toggleMobileMenu());
                            setBurgerActive(!burgerActive);
                        }}
                    >
                        <Hamburger active={burgerActive} />
                    </button>
                    {renderMenu(rightLinks, "right", leftLinks.length)}
                </div>
            </nav>
            <div
                className={`primary-nav-overlay ${
                    activeSubmenu !== null ? "active" : ""
                }`}
            />
        </>
    );
};

export default PrimaryNav;
