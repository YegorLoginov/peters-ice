// @flow

import * as React from "react";

import ProductTile from "@components/molecules/ProductTile";
import { getFeaturedImage } from "@utils/post";

type PropsType = {
    products: any[],
    variation: "offset" | "featured",
    showTags: boolean,
};

const ProductGrid = (props: PropsType): React.Node => {
    const classes = ["product-grid"];
    classes.push(`product-grid--${props.variation}`);

    const xsColMap = {
        offset: 6,
        featured: 12,
    };

    const mdColMap = {
        offset: 3,
        featured: 4,
    };

    return (
        <div className={classes.join(" ")}>
            <div
                className={`row ${
                    props.products.length < 4 ? "center-xs" : ""
                }`}
            >
                {props.products.map((product): React.Node => (
                    <div
                        className={`col-xs-${
                            xsColMap[props.variation]
                        } col-md-${
                            mdColMap[props.variation]
                        } product-grid__col`}
                        key={`${props.variation}-${product.ID || product.id}`}
                    >
                        <div className="product-grid__product">
                            <ProductTile
                                brand={product.acf.brand.post_title}
                                colour={product.acf.colour[0].colour}
                                image={getFeaturedImage(product)}
                                title={product.title}
                                url={product.url}
                                tags={
                                    props.showTags
                                        ? product.tags_object || undefined
                                        : undefined
                                }
                                variation={
                                    props.variation === "featured"
                                        ? "featured"
                                        : "regular"
                                }
                            />
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

ProductGrid.defaultProps = {
    variation: "offset",
    showTags: false,
};

export default ProductGrid;
