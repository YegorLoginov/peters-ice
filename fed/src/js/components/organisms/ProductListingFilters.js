// @flow

import * as React from "react";

import FilterToggle from "@components/atoms/FilterToggle";
import AnimateHeight from "@components/atoms/AnimateHeight";

type PropsType = {
    filters: {
        dietary: any[],
        brand: any[],
    },
    activeFilters: {
        dietary: any[],
        brand: any[],
    },
    filtersUpdated: (newFilters: {
        dietary: any[],
        brand: any[],
    }) => void,
};

const ProductListingFilters = (props: PropsType): React.Node => {
    const [activeTab, setActiveTab] = React.useState("dietary");

    const getTabTitle = (key: string): React.Node => {
        const titles = {
            dietary: (
                <>
                    <span>Filter by</span> <strong>Dietary</strong>
                </>
            ),
            brand: (
                <>
                    <span>Filter by</span> <strong>Brand</strong>
                </>
            ),
        };
        return titles[key] || <>Unknown tab</>;
    };

    const renderTab = (tabKey: string): React.Node => {
        const handleClick = () => {
            setActiveTab(tabKey);
        };
        const classes = [];
        classes.push("product-listing-filters__tab");
        if (activeTab === tabKey) {
            classes.push("active");
        }
        return (
            <button
                key={tabKey}
                onClick={handleClick}
                className={classes.join(" ")}
            >
                {getTabTitle(tabKey)}
            </button>
        );
    };

    const isFilterActive = (filter: any): boolean => {
        return !!props.activeFilters[activeTab].find(
            (f): boolean => f.slug === filter.slug
        );
    };

    const handleFilterClick = (filter: any) => {
        const current = [...props.activeFilters[activeTab]];
        if (isFilterActive(filter)) {
            const idx = props.activeFilters[activeTab].findIndex(
                (f): boolean => f.slug === filter.slug
            );
            current.splice(idx, 1);
        } else {
            current.push(filter);
        }
        props.filtersUpdated({
            ...props.activeFilters,
            [activeTab]: current,
        });
    };

    return (
        <div className="product-listing-filters">
            <div className="container-fluid">
                <div className="product-listing-filters__inner">
                    <div className="product-listing-filters__tabs">
                        {Object.keys(props.filters).map(renderTab)}
                    </div>
                    <div className="product-listing-filters__contents">
                        <AnimateHeight>
                            <div className="product-listing-filters__contents__inner">
                                {props.filters[activeTab].map(
                                    (filter, index): React.Node => (
                                        <FilterToggle
                                            key={`${activeTab}-${filter.slug}`}
                                            onClick={() => {
                                                handleFilterClick(filter);
                                            }}
                                            active={isFilterActive(filter)}
                                            title={filter.name || filter.title}
                                            data-for={activeTab}
                                            style={{
                                                animationDelay: `${15 *
                                                    index}ms`,
                                            }}
                                        />
                                    )
                                )}
                            </div>
                        </AnimateHeight>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ProductListingFilters;
