// @flow

import * as React from "react";

import CtaTile, { type CtaTileType } from "@components/molecules/CtaTile";
import useMatchHeight from "@hooks/useMatchHeight";

type PropsType = {
    tiles: CtaTileType[],
    size: "regular" | "large",
};

const ThreeAcross = (props: PropsType): React.Node => {
    const tiles = props.tiles;
    const container = React.useRef(null);
    useMatchHeight(container, [tiles]);
    return (
        <div className="three-across" ref={container}>
            <div className="row">
                {tiles.map((tile, index): React.Node => (
                    <div
                        key={index}
                        className="col-xs-12 col-md-4 three-across__col"
                    >
                        <div className="three-across__single">
                            <CtaTile
                                key={index}
                                tile={tile}
                                size={props.size}
                            />
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

ThreeAcross.defaultProps = {
    size: "regular",
};

export default ThreeAcross;
