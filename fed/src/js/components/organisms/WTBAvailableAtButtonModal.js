// @flow

import * as React from "react";

import Button from "@components/atoms/Button";
import PetersModal from "@components/molecules/PetersModal";
import WTBItem from "@components/atoms/WTBItem";

import ColesLogo from "../../../assets/images/logo-coles.png";
import WoolworthsLogo from "../../../assets/images/logo-woolworths.png";
import UberEatsLogo from "../../../assets/images/logo-uber-eats.png";
import DoorDashLogo from "../../../assets/images/logo-doordash.png";
import PetersLogo from "../../../assets/images/logo-peters.png";

import { type AcfType as BrandAcfType } from "@containers/BrandHub";

const WTBAvailableAtButtonModal = (props: {
    color: string,
    brandACF: BrandAcfType,
}): React.Node => {
    const [isModalOpen, setIsModalOpen] = React.useState(false);
    const hasModal =
        props.brandACF.where_to_buy__available_at === "where-to-buy"
            ? Object.values(props.brandACF.where_to_buy).find(
                  (value): boolean => !!value
              )
            : props.brandACF.available_at &&
              props.brandACF.available_at.length > 0;
    if (!hasModal) {
        return null;
    }

    return (
        <>
            <Button
                onClick={() => {
                    setIsModalOpen(true);
                }}
            >
                {props.brandACF.where_to_buy__available_at === "where-to-buy" // to avoid confusion, the labels have changed https://adrenalinmedia.atlassian.net/browse/PI-267
                    ? "Buy now"
                    : "Find stores"}
            </Button>
            <PetersModal
                color={props.color}
                title={
                    props.brandACF.where_to_buy__available_at === "where-to-buy"
                        ? "Buy Now"
                        : "Find Stores"
                }
                isOpen={isModalOpen}
                onClose={() => {
                    setIsModalOpen(false);
                }}
            >
                {props.brandACF.where_to_buy__available_at ===
                "where-to-buy" ? (
                    <>
                        {props.brandACF.where_to_buy.woolworths && (
                            <WTBItem
                                logo={WoolworthsLogo}
                                name="Woolworths"
                                link={props.brandACF.where_to_buy.woolworths}
                            />
                        )}
                        {props.brandACF.where_to_buy.coles && (
                            <WTBItem
                                logo={ColesLogo}
                                name="Coles"
                                link={props.brandACF.where_to_buy.coles}
                            />
                        )}
                        {props.brandACF.where_to_buy.uber_eats && (
                            <WTBItem
                                logo={UberEatsLogo}
                                name="Uber Eats"
                                link={props.brandACF.where_to_buy.uber_eats}
                            />
                        )}
                        {props.brandACF.where_to_buy.doordash && (
                            <WTBItem
                                logo={DoorDashLogo}
                                name="DoorDash"
                                link={props.brandACF.where_to_buy.doordash}
                            />
                        )}
                        {props.brandACF.where_to_buy.peters && (
                            <WTBItem
                                logo={PetersLogo}
                                name="Peters"
                                link={props.brandACF.where_to_buy.peters}
                            />
                        )}
                    </>
                ) : (
                    <>
                        {props.brandACF.available_at.map(
                            (item, index): React.Node => (
                                <div className="available-at-item" key={index}>
                                    <div className="available-at-item__title heading-3">
                                        {item.title}
                                    </div>
                                    <div className="available-at-item__subtitle body-16-20">
                                        {item.subtitle}
                                    </div>
                                </div>
                            )
                        )}
                    </>
                )}
            </PetersModal>
        </>
    );
};

export default WTBAvailableAtButtonModal;
