const USE_LOCAL = true;

const host =
    process.env.NODE_ENV === "production"
        ? window.location.protocol + "//" + window.location.host
        : USE_LOCAL
        ? "http://localhost:8000"
        : "https://picdev.wpengine.com";

const apiBase = `${host}/wp-json`;

export default {
    wordpressHost: host,
    tagsCounterKey: "tagsCounter",
    tagsAPI: `${apiBase}/wp/v2/tags`,
    genericWordpressAPI: `${apiBase}/wp/v2`,
    optionsAPI: `${apiBase}/acf/v3/options/options`,
    pageAPI: `${apiBase}/adrenalin/page`,
    searchAPI: `${apiBase}/adrenalin/search`,
    fallbackFeaturedImage: "http://placehold.it/800",
    gravityAPI: `${apiBase}/gf/v2`,
    regex: {
        date: /^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/,
    },
};
