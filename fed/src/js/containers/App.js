// @flow

import * as React from "react";
import Helmet from "react-helmet";
import { Switch } from "react-router-dom";
import { renderRoutes } from "react-router-config";
import { useSelector } from "react-redux";

import Routes from "../Routes";
import PrimaryNav from "@components/organisms/PrimaryNav";
import MobileMenu from "@components/organisms/MobileMenu";
import Footer from "@components/organisms/Footer";
import YoastMeta from "@components/atoms/YoastMeta";
import CrisisBanner from "@components/organisms/CrisisBanner";

import { PrimaryNavItemType } from "@types";

const App = (props: any): React.Node => {
    const primaryNavLinks = useSelector(
        (state): PrimaryNavItemType[] => state.options.primary_menu
    );

    return (
        <div className="site-wrap">
            <Helmet defaultTitle="Peters Ice Cream"></Helmet>
            <YoastMeta />
            <PrimaryNav links={primaryNavLinks} />
            <MobileMenu links={primaryNavLinks} active={false} />
            <div className="page">
                <div className="page__content gap">
                    <CrisisBanner />
                    <Switch>{renderRoutes(Routes)}</Switch>
                </div>
            </div>
            <Footer />
        </div>
    );
};

export default App;
