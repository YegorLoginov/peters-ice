// @flow

import * as React from "react";

import actions from "@actions/index.js";
import usePageFromStore from "@hooks/usePageFromStore";
import HeroBrandHub from "@components/organisms/HeroBrandHub";
import { AccordionSectionType, ContentBlockType } from "@types";
import Image from "@components/atoms/Image";
import { getExcerpt, getResponsiveFeaturedImage } from "@utils/post";
import Button from "@components/atoms/Button";
import Icon from "@components/atoms/Icon";
import SocialLink from "@components/atoms/SocialLink";
import LeftRightImage from "@components/organisms/LeftRightImage";
import ProductGrid from "@components/organisms/ProductGrid";
import AccordionsSection from "@components/organisms/AccordionsSection";
import WTBAvailableAtButtonModal from "@components/organisms/WTBAvailableAtButtonModal";
import {
    WhereToBuyAvailableAtSelectorType,
    WhereToBuyType,
    AvailableAtType,
    ImageType,
} from "@types";

import useProductsFromBrand from "@hooks/useProductsFromBrand";

export type AcfType = {
    brand_colour: { colour: string }[],
    content_blocks: ContentBlockType[],
    flavours: { flavour: any }[],
    meta: any[],
    tagline: string,
    where_to_buy: WhereToBuyType,
    available_at: AvailableAtType[],
    where_to_buy__available_at: WhereToBuyAvailableAtSelectorType,
    social: { facebook?: string, instagram?: string },
    accordions: AccordionSectionType[],
    page_type: "rich" | "simple",
    brand_logo: ImageType,
};

type PropsType = {};

const BrandHub = (props: PropsType): React.Node => {
    const { page } = usePageFromStore();
    const acf: AcfType = page.acf;
    const accordions = acf.accordions?.[0];
    const brandColour = acf.brand_colour?.[0].colour || "pink";
    const flavours = useProductsFromBrand(page.ID, "", 99);

    return (
        <div className="brand-hub">
            <HeroBrandHub
                type={acf.page_type}
                image={getResponsiveFeaturedImage(page)}
            />
            <div className={`brand-hub-intro`}>
                <div className="container-fluid">
                    <div
                        className={`brand-hub-intro__inner bg--${brandColour}`}
                    >
                        <div className="brand-hub-intro__logo">
                            <Image
                                image={page.acf.brand_logo}
                                size="medium_large"
                            ></Image>
                        </div>
                        <div className="brand-hub-intro__content">
                            <h1 className="heading-3 brand-hub-intro__title">
                                {page.post_title}
                            </h1>
                            {acf.tagline && (
                                <p className="tagline brand-hub-intro__tagline">
                                    {acf.tagline}
                                </p>
                            )}
                            <p className="brand-hub-intro__excerpt body-20">
                                {getExcerpt(page)}
                            </p>
                        </div>
                        <div className="brand-hub-intro__sidebar">
                            <WTBAvailableAtButtonModal
                                brandACF={acf}
                                color={brandColour}
                            />
                            {acf.page_type === "rich" && (
                                <>
                                    {acf.social && (
                                        <Button
                                            to="#flavours"
                                            variation="white"
                                        >
                                            Flavours
                                            <Icon name="arrow-down"></Icon>
                                        </Button>
                                    )}
                                    {acf.social && (
                                        <div className="brand-hub-intro__socials">
                                            {acf.social.facebook && (
                                                <SocialLink
                                                    icon="facebook"
                                                    url={acf.social.facebook}
                                                />
                                            )}
                                            {acf.social.instagram && (
                                                <SocialLink
                                                    icon="instagram"
                                                    url={acf.social.instagram}
                                                />
                                            )}
                                        </div>
                                    )}
                                </>
                            )}
                        </div>
                    </div>
                </div>
            </div>
            {acf.page_type === "rich" && acf.content_blocks && (
                <div className="brand-hub__content-blocks">
                    {acf.content_blocks.map((block, index): React.Node => (
                        <LeftRightImage
                            key={index}
                            content={block}
                            variation={index % 2 === 0 ? "right" : "left"}
                        />
                    ))}
                </div>
            )}
            {flavours && flavours.length > 0 && (
                <div className="brand-hub__products" id="flavours">
                    <div className="container-fluid">
                        <div className="brand-hub__products__title">
                            <h2 className="heading-2">Flavours</h2>
                        </div>
                        <div className="brand-hub__products__products">
                            <ProductGrid showTags products={flavours} />
                        </div>
                    </div>
                </div>
            )}
            {accordions && accordions.enabled && (
                <AccordionsSection
                    eyebrow={accordions.eyebrow}
                    title={accordions.title}
                    accordions={accordions.accordions}
                />
            )}
        </div>
    );
};

BrandHub.preload = ({ passed: { store }, location }: any): Promise<any> => {
    return Promise.all([
        store.dispatch(actions.fetchPage(location.pathname, location.search)),
    ]);
};

export default BrandHub;
