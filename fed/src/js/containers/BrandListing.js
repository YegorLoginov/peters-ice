// @flow

import * as React from "react";

import { fetchPosts } from "@api/index";
import usePageFromStore from "@hooks/usePageFromStore";
import HeroProductListing from "@components/organisms/HeroProductListing";
import useDeltaState from "@hooks/useDeltaState";
import addACFData from "@utils/addACFData";
import BrandGrid from "@components/organisms/BrandGrid";
import FeaturedBrandTile from "@components/molecules/FeaturedBrandTile";
import { ImageType } from "@types";

type PropsType = {};

const BrandListing = (props: PropsType): React.Node => {
    // Constants
    const POSTS_PER_PAGE = 99;
    const POST_TYPE = "brands";

    // Hooked vars
    const { page } = usePageFromStore();
    const containerRef = React.useRef(null);
    const [state, setState] = useDeltaState({
        posts: [],
        initialLoad: true,
    });

    // Helpers vars
    const featuredPosts: { brand: { ID: string }, image: ImageType }[] = page
        .acf.featured_brands
        ? page.acf.featured_brands
        : [];

    // Main data fetching hook
    React.useEffect(() => {
        fetchPosts({
            postType: POST_TYPE,
            perPage: POSTS_PER_PAGE,
            extra: {
                exclude: featuredPosts
                    .map(({ brand }): any => brand.ID)
                    .join(","),
            },
        })
            .then(({ data }: any) => {
                setState({
                    posts: data,
                });
            })
            .finally(() => {
                setState({ initialLoad: false });
            });
    }, []);

    return (
        <>
            <HeroProductListing page={page} />
            {featuredPosts.length > 0 && (
                <div className="featured-brands">
                    <div className="container-fluid">
                        <div className="row">
                            {featuredPosts.map((item, index): React.Node => {
                                return (
                                    <div
                                        className="col-xs-12 col-sm-4"
                                        key={item.brand.ID}
                                    >
                                        <div className="featured-brands__item">
                                            <FeaturedBrandTile
                                                image={item.image}
                                                brand={addACFData(item.brand)}
                                            />
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </div>
            )}
            <div className="brand-listing" ref={containerRef}>
                <div className="container-fluid">
                    <div className="brand-listing__inner">
                        <div className="brand-listing__results">
                            <BrandGrid posts={state.posts} />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default BrandListing;
