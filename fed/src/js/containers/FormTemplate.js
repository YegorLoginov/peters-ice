// @flow

import * as React from "react";

import HeroProductListing from "@components/organisms/HeroProductListing";
import PageContent from "@components/organisms/PageContent";
import FormPageIntro, {
    type IntroType,
} from "@components/molecules/FormPageIntro";
import GravityForm from "@components/organisms/GravityForm";
import { GravityFormType } from "@types";
import useDynamicBrandTypeAndFlavour from "@hooks/useDynamicBrandTypeAndFlavour";
import { FeedbackBrandType } from "@types";

type PropsType = {
    page: any,
};

type AcfType = {
    meta: any,
    gravity_form: GravityFormType,
    intro: IntroType,
    feedback_brands: Array<FeedbackBrandType>,
};

const FormTemplate = ({ page }: PropsType): React.Node => {
    const acf: AcfType = page.acf;

    // Modify the form data to dynamically insert the brand, type and flavour values
    // This hook will only actually modify the form if it's the product-feedback form
    const {
        modifiedFormData,
        handleFormChange,
    } = useDynamicBrandTypeAndFlavour(acf.gravity_form, acf.feedback_brands);

    return (
        <>
            <HeroProductListing
                page={page}
                key={`product-listing-hero-${page.ID}`}
            />
            <PageContent>
                <PageContent.Section classModifier="intro">
                    <FormPageIntro intro={acf.intro} />
                </PageContent.Section>
                <PageContent.Section>
                    <GravityForm
                        key={modifiedFormData.id}
                        form={modifiedFormData}
                        fileUploadPageId={page.gform_upload_page_slug}
                        recaptchaSiteKey={page.gforms_captcha_public_key}
                        onChange={handleFormChange}
                    />
                </PageContent.Section>
            </PageContent>
        </>
    );
};

export default FormTemplate;
