// @flow

import * as React from "react";

import actions from "@actions/index.js";
import HeroHome from "@components/organisms/HeroHome";
import FeaturedProducts from "@components/organisms/FeaturedProducts";
import ThreeAcross from "@components/organisms/ThreeAcross";
import usePageFromStore from "@hooks/usePageFromStore";

const Home = (): React.Node => {
    const { page } = usePageFromStore();
    return (
        <>
            <HeroHome />
            <FeaturedProducts />
            <ThreeAcross tiles={page.acf.three_across_tiles} />
        </>
    );
};

Home.preload = ({ passed: { store }, location }: any): Promise<any> => {
    return Promise.all([
        store.dispatch(actions.fetchPage("/", location.search)),
    ]);
};

export default Home;
