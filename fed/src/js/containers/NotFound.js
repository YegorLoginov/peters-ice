// @flow

import * as React from "react";

import HeroProductListing from "@components/organisms/HeroProductListing";

const NotFound = (): React.Node => {
    return (
        <>
            <HeroProductListing
                page={{
                    post_title: "404 - Not Found",
                    acf: {
                        meta: [
                            {
                                excerpt:
                                    "How embarrassing! Sorry we can't find that page anywhere, would you like to see some ice cream instead?",
                                hero_cta: {
                                    title: "Show me ice cream!",
                                    url: "/products/",
                                },
                            },
                        ],
                    },
                }}
                hasOffset={false}
            />
        </>
    );
};

export default NotFound;
