// @flow

import * as React from "react";

import actions from "@actions/index.js";
import usePageFromStore from "@hooks/usePageFromStore";
import HeroProductDetail from "@components/organisms/HeroProductDetail";
import PageContent from "@components/organisms/PageContent";
import MoreFromBrand from "@components/organisms/MoreFromBrand";
import RichText from "@components/molecules/RichText";
import AvailableIn from "@components/molecules/AvailableIn";

type PropsType = {};

const ProductDetail = (props: PropsType): React.Node => {
    const { page } = usePageFromStore();
    const availableIn = page.acf.available_in || [];
    return (
        <>
            <HeroProductDetail
                product={page}
                key={`product-detail-hero-${page.ID}`}
            />
            <PageContent page={page}>
                {availableIn.length > 0 && (
                    <PageContent.Section>
                        <div className="rich-text">
                            <h3>Available in</h3>
                        </div>
                        <AvailableIn availableIn={availableIn} />
                    </PageContent.Section>
                )}
                <PageContent.Section>
                    <RichText
                        html={
                            `<h3>Ingredients</h3>` +
                            (page.acf.ingredients || "")
                        }
                    />
                </PageContent.Section>
                <PageContent.Section>
                    <RichText
                        html={
                            `<h3>Nutritional Table</h3>` +
                            (page.acf.nutritional || "")
                        }
                    />
                </PageContent.Section>
            </PageContent>
            <MoreFromBrand page={page} key={page.ID} />
        </>
    );
};

ProductDetail.preload = ({
    passed: { store },
    location,
}: any): Promise<any> => {
    return Promise.all([
        store.dispatch(actions.fetchPage(location.pathname, location.search)),
    ]);
};

export default ProductDetail;
