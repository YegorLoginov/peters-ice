// @flow

import * as React from "react";
import queryString from "query-string";

import { fetchPosts } from "@api/index";
import usePageFromStore from "@hooks/usePageFromStore";
import useAsyncState from "@hooks/useAsyncState";
import HeroProductListing from "@components/organisms/HeroProductListing";
import ProductListingFilters from "@components/organisms/ProductListingFilters";
import Pagination from "@components/molecules/Pagination";

import ProductGrid from "@components/organisms/ProductGrid";
import { useHistory } from "react-router-dom";
import useDeltaState from "@hooks/useDeltaState";
import addACFData from "@utils/addACFData";

type PropsType = {};

let lastFilterCount = 0;

let timer;
const updateLastFilterCount = newValue => {
    if (timer) {
        return;
    }
    timer = setTimeout(() => {
        lastFilterCount = newValue;
        timer = null;
    }, 10);
};

const ProductListing = (props: PropsType): React.Node => {
    // Constants
    const POSTS_PER_PAGE = 12;
    const POST_TYPE = "products";

    // Hooked vars
    const [asyncState, actions] = useAsyncState();
    const history = useHistory();
    const { page } = usePageFromStore();
    const containerRef = React.useRef(null);

    // Helpers funcs
    const updateHash = (fetchNextHash: (query: any) => any) => {
        const query = queryString.parse(window.location.hash);
        const newQuery = fetchNextHash(query);
        history.push(
            `#${queryString.stringify(newQuery, {
                encode: false,
            })}`
        );
    };

    const getFiltersFromURL = (): { dietary: any[], brand: any[] } => {
        const query = queryString.parse(window.location.hash);
        const dietarySlugs = query.dietary ? query.dietary.split(",") : [];
        const brandSlugs = query.brand ? query.brand.split(",") : [];
        const dietary = page.products_tags.filter((tag): any =>
            dietarySlugs.includes(tag.slug)
        );
        const brand = page.brands.filter((tag): any =>
            brandSlugs.includes(tag.slug)
        );
        return { dietary, brand };
    };

    // State
    const pageQuery = queryString.parse(window.location.hash);
    const [state, setState] = useDeltaState({
        posts: [],
        headers: {},
        initialLoad: true,
        filters: getFiltersFromURL(),
        pageNumber: pageQuery.page ? parseInt(pageQuery.page) : 1,
        prevPageNumber: null,
    });

    // Helpers vars
    const featuredPosts = (page.acf.featured_products || [])
        .map(({ product }): any => product)
        .map(addACFData);
    const totalPages = parseInt(state.headers["x-wp-totalpages"]) || 1;
    const filtersCount =
        state.filters.brand.length + state.filters.dietary.length;
    const hasFilters = filtersCount > 0;

    // Determine if we should be showing the featured posts
    let showFeatured = state.initialLoad;
    if (state.pageNumber === 1) {
        if (lastFilterCount === 0 && filtersCount === 1 && asyncState.loading) {
            showFeatured = true;
        }
        if (!hasFilters && asyncState.loading === false) {
            showFeatured = true;
        }
    } else if (state.prevPageNumber === 1) {
        if (asyncState.loading === true && !hasFilters) {
            showFeatured = true;
        }
    }

    updateLastFilterCount(filtersCount);

    // Re-set filters when the page changes
    React.useEffect(() => {
        setState({ filters: getFiltersFromURL() });
    }, [page.ID]);

    // Main data fetching hook
    React.useEffect(() => {
        actions.request();
        fetchPosts({
            postType: POST_TYPE,
            page: state.pageNumber,
            perPage: POSTS_PER_PAGE,
            extra: {
                tags: state.filters.dietary
                    .map((t): string => t.term_id)
                    .join(","),
                brands: state.filters.brand.map((b): string => b.id).join(","),
                exclude:
                    filtersCount > 0
                        ? ""
                        : featuredPosts
                              .map((product): any => product.ID)
                              .join(","),
            },
        })
            .then(({ data, headers }: any) => {
                setState({
                    posts: data.map((product): any => ({
                        ...product,
                        tags_object: product.tags.map((tagId): any =>
                            page.products_tags.find(
                                (f): any => f.term_id === tagId
                            )
                        ),
                    })),
                    headers,
                });
                actions.success();
            })
            .catch(actions.fail)
            .finally(() => {
                setState({ initialLoad: false });
            });
    }, [page.ID, state.pageNumber, state.filters]);

    return (
        <>
            <HeroProductListing page={page} />
            <ProductListingFilters
                filters={{
                    dietary: page.products_tags,
                    brand: page.brands,
                }}
                activeFilters={state.filters}
                filtersUpdated={nextFilters => {
                    setState({
                        filters: nextFilters,
                        pageNumber: 1,
                    });

                    // Add to URL
                    const getSlugsString = (arr): string => {
                        return arr.map((f): string => f.slug).join(",");
                    };
                    const dietary = getSlugsString(nextFilters.dietary);
                    const brand = getSlugsString(nextFilters.brand);
                    const newFilters = {};
                    if (dietary) {
                        newFilters.dietary = dietary;
                    }
                    if (brand) {
                        newFilters.brand = brand;
                    }
                    updateHash((): any => newFilters);
                }}
            />
            <div className="product-listing" ref={containerRef}>
                <div className="container-fluid">
                    <div className="product-listing__inner">
                        <div
                            className="product-listing__results"
                            style={{
                                opacity: asyncState.loading ? 0.5 : null,
                                transition: "200ms ease all",
                            }}
                        >
                            {showFeatured && featuredPosts.length > 0 && (
                                <ProductGrid
                                    showTags
                                    variation="featured"
                                    products={featuredPosts}
                                />
                            )}
                            {state.posts.length > 0 ? (
                                <ProductGrid showTags products={state.posts} />
                            ) : (
                                <div className="product-listing__no-results">
                                    <div className="product-listing__no-results__heading">
                                        No ice creams found :(
                                    </div>
                                    <p className="body-20">
                                        Try applying less filters to see more
                                        results
                                    </p>
                                </div>
                            )}
                        </div>
                        {(asyncState.loading && state.initialLoad) ||
                        totalPages === 1 ? null : (
                            <div className="pagination-container">
                                <Pagination
                                    numberOfPages={totalPages}
                                    currentPageNumber={state.pageNumber}
                                    pageNumberChanged={newPageNumber => {
                                        setState({
                                            prevPageNumber: state.pageNumber,
                                            pageNumber: newPageNumber,
                                        });
                                        updateHash((query): any => ({
                                            ...query,
                                            page: newPageNumber,
                                        }));
                                    }}
                                />
                            </div>
                        )}
                    </div>
                </div>
            </div>
        </>
    );
};

ProductListing.preload = ({ page, passed: { store } }): Promise<any> => {
    const promises = [];
    return Promise.all(promises);
};

export default ProductListing;
