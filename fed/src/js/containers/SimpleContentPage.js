// @flow

import * as React from "react";
import HeroSimple from "@components/organisms/HeroSimple";
import RichText from "@components/molecules/RichText";
import ThreeAcross from "@components/organisms/ThreeAcross";
import AccordionsSection from "@components/organisms/AccordionsSection";
import { AccordionSectionType } from "@types";

type PropsType = {
    page: any,
};

type AcfType = {
    accordions: AccordionSectionType[],
    three_across_tiles: any,
};

const SimpleContentPage = (props: PropsType): React.Node => {
    const { accordions, three_across_tiles }: AcfType = props.page.acf;
    const accordionSection = accordions?.[0];
    return (
        <>
            <HeroSimple
                page={props.page}
                key={`simple-hero-${props.page.ID}`}
            ></HeroSimple>
            <div className="simple-page-content">
                <div className="container-fluid">
                    <article>
                        <RichText html={props.page.acf.content} />
                    </article>
                </div>
            </div>
            {accordionSection && accordionSection.enabled && (
                <AccordionsSection
                    title={accordionSection.title}
                    eyebrow={accordionSection.eyebrow}
                    accordions={accordionSection.accordions}
                />
            )}
            {three_across_tiles && three_across_tiles.enabled && (
                <ThreeAcross size="large" tiles={three_across_tiles.tiles} />
            )}
        </>
    );
};

SimpleContentPage.defaultProps = {};

export default SimpleContentPage;
