// @flow

import * as React from "react";

import usePageFromStore from "@hooks/usePageFromStore";
import SimpleContentPage from "@containers/SimpleContentPage";
import ProductListing from "@containers/ProductListing";
import BrandListing from "@containers/BrandListing";
import FormTemplate from "@containers/FormTemplate";
import Home from "@containers/Home";
import ProductDetail from "@containers/ProductDetail";
import BrandHub from "@containers/BrandHub";
import actions from "@actions/index.js";
import NotFound from "./NotFound";

type PropsType = {};

const templateMapping = (page: Object): any => {
    if (page.page_template) {
        switch (page.page_template) {
            case "templates/template-product-listing.php":
                return ProductListing;
            case "templates/template-brand-listing.php":
                return BrandListing;
            case "templates/template-form-page.php":
                return FormTemplate;
            case "templates/home.php":
                return Home;
            default:
                return SimpleContentPage;
        }
    } else if (page.post_type) {
        switch (page.post_type) {
            case "products":
                return ProductDetail;
            case "brands":
                return BrandHub;
            default:
                return SimpleContentPage;
        }
    } else {
        return NotFound;
    }
};

const TemplateChooser = (props: PropsType): React.Node => {
    const { page } = usePageFromStore();
    const Component = templateMapping(page);
    if (Component) {
        return <Component page={page}></Component>;
    }
    return null;
};

TemplateChooser.preload = ({ location, passed }: any): ?Promise<any> => {
    // First fetch the page, then determine which Container component will be rendered,
    // before resolving, first check if those components have a preload method and fire them
    return new Promise((resolve: () => void, reject: any) => {
        if (!passed.store.dispatch) {
            resolve();
        }
        passed.store
            .dispatch(actions.fetchPage(location.pathname, location.search))
            .then(() => {
                const page = passed.store.getState().page[location.pathname];
                const ComponentThatWillRender = templateMapping(page);
                if (ComponentThatWillRender.preload) {
                    // Preload can return null - so need to check for that
                    const preload = ComponentThatWillRender.preload({
                        page,
                        passed,
                        location,
                    });
                    if (preload && preload.then) {
                        preload.then(resolve);
                    } else {
                        resolve();
                    }
                } else {
                    resolve();
                }
            })
            .catch((error: any) => {
                reject(error);
            });
    });
};

export default TemplateChooser;
