import { useState } from "react";

// Updates state as a delta - only works for objects
const useDeltaState = initialState => {
    const [state, _setState] = useState(initialState);
    const setState = newValueDelta => {
        _setState(old => ({
            ...old,
            ...newValueDelta,
        }));
    };
    return [state, setState];
};

export default useDeltaState;
