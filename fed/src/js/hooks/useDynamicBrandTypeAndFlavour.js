// @flow

import * as React from "react";

import { GravityFormType, FeedbackBrandType, GenericObjectType } from "@types";

const simpleDeepClone = (obj: GenericObjectType): GenericObjectType => {
    return JSON.parse(JSON.stringify(obj));
};

const useDynamicBrandTypeAndFlavour = (
    formData: GravityFormType,
    feedbackBrands: FeedbackBrandType[] = []
): {
    modifiedFormData: GravityFormType,
    handleFormChange: (values: GenericObjectType) => void,
} => {
    // Constants
    const BRAND_FIELD_ID = 15;
    const TYPE_FIELD_ID = 29;
    const FLAVOUR_FIELD_ID = 16;

    // Dynamically populate the possible options for Brand, Type and Flavour fields
    // make sure we're getting fresh data each render
    const form = simpleDeepClone(formData);

    // State objects
    const [selectedBrand, setSelectedBrand] = React.useState(null);
    const [selectedType, setSelectedType] = React.useState(null);

    // Helper variables
    const selectedBrandObject = feedbackBrands.find(
        (brand): boolean => brand.brand === selectedBrand
    ) || { flavours: [] };

    const filteredFlavours = selectedType
        ? selectedBrandObject.flavours.filter(
              (flavour): any => flavour.type === selectedType
          )
        : [];

    let types = selectedBrand
        ? [
              ...new Set(
                  selectedBrandObject.flavours.map((brand): any => brand.type)
              ),
          ]
        : [];
    let flavours =
        selectedBrand && selectedType
            ? [...new Set(filteredFlavours.map((brand): any => brand.product))]
            : [];

    if (form.id === 3) {
        form.fields.forEach((field, index) => {
            // Dynamically the brand field
            if (field.id === BRAND_FIELD_ID) {
                form.fields[index] = {
                    ...field,
                    choices: [
                        ...field.choices,
                        ...feedbackBrands.map((brand, index): any => ({
                            id: brand.brand,
                            text: brand.brand,
                            value: brand.brand,
                        })),
                    ],
                };
            }

            // Dynamically set the type field
            if (field.id === TYPE_FIELD_ID && selectedBrand) {
                form.fields[index] = {
                    ...field,
                    choices: [
                        ...field.choices,
                        ...types.map((type, index): any => ({
                            id: type,
                            text: type,
                            value: type,
                        })),
                    ],
                };
            }

            // Set the flavour field
            if (
                field.id === FLAVOUR_FIELD_ID &&
                selectedBrand &&
                selectedType
            ) {
                form.fields[index] = {
                    ...field,
                    choices: [
                        ...field.choices,
                        ...flavours.map((flavour, index): any => ({
                            id: flavour,
                            text: flavour,
                            value: flavour,
                        })),
                    ],
                };
            }
        });
    }

    const handleFormChange = values => {
        Object.entries(values).forEach(([key, value]: [string, any]) => {
            const id = Number(key.replace("input_", ""));
            if (id === BRAND_FIELD_ID && value !== selectedBrand) {
                setSelectedBrand(value || null);
            }
            if (id === TYPE_FIELD_ID && value !== selectedType) {
                setSelectedType(value || null);
            }
        });
    };

    return {
        modifiedFormData: form,
        handleFormChange: handleFormChange,
    };
};

export default useDynamicBrandTypeAndFlavour;
