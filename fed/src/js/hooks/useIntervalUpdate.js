// @flow

import * as React from "react";

// Forces a re-render every duration ms
const useIntervalUpdate = (duration: number = 1000) => {
    const timerState = React.useState(0);
    React.useEffect((): any => {
        const run = () => {
            timerState[1]((i: number): number => i + 1);
        };
        const int = window.setInterval(run, duration);
        return () => {
            window.clearInterval(int);
        };
    }, []);
};

export default useIntervalUpdate;
