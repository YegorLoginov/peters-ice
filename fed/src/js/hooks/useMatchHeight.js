// @flow

import * as React from "react";

const useMatchHeight = (rootElRef: any, dependants: Array<any>) => {
    // Make the content area of each tile match height
    React.useEffect((): any => {
        if (rootElRef.current instanceof HTMLElement) {
            const el = rootElRef.current;
            const calculate = () => {
                const tileContents: HTMLElement[] = Array.from(
                    el.querySelectorAll(".cta-tile__content")
                );
                const tallest = tileContents.reduce(
                    (maxHeight, current): number => {
                        current.style.height = "";
                        if (maxHeight < current.clientHeight) {
                            return current.clientHeight;
                        }
                        return maxHeight;
                    },
                    0
                );
                tileContents.forEach(tile => {
                    tile.style.height = `${tallest}px`;
                });
            };
            calculate();
            window.addEventListener("resize", calculate);
            return () => {
                window.removeEventListener("resize", calculate);
            };
        }
    }, dependants);
};

export default useMatchHeight;
