// @flow

import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";

/**
 * Gets the current page for path from the redux store.
 * Loading will be true if still loading the page or initialFetchAll is still loading
 */
const usePageFromStore = (): { page: any, loading: boolean } => {
    const location = useLocation();
    const loading = useSelector(
        (state: any): boolean =>
            state.initialFetchAll.loading || state.page.loading
    );
    const page =
        useSelector(
            (state: any): any => state.page[location.pathname],
            (): any => {}
        ) || {};
    return { page, loading };
};

export default usePageFromStore;
