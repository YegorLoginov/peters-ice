// @flow

import * as React from "react";
import { fetchPosts } from "@api/index";
import Config from "@Config";

const useProductsFromBrand = (
    brandId: string | number,
    exclude?: string | number = "",
    perPage?: number = 99
): any[] => {
    const [products, setProducts] = React.useState([]);
    React.useEffect(() => {
        let tagFetcher = Promise.resolve(window.ptrs_tags);
        if (!window.ptrs_tags) {
            tagFetcher = fetch(
                `${Config.wordpressHost}/wp-json/wp/v2/tags?per_page=99&_fields=id,name,slug`
            )
                .then((res): any => res.json())
                .then((data): any[] => {
                    window.ptrs_tags = data;
                    return data;
                });
        }
        tagFetcher.then(tags => {
            fetchPosts({
                postType: "products",
                perPage: perPage,
                extra: {
                    brands: brandId,
                    exclude: exclude,
                },
            }).then(({ data }) => {
                setProducts(
                    data.map((product): any => ({
                        ...product,
                        tags_object: product.tags.map((tagId): any =>
                            tags.find((tag): boolean => tag.id === tagId)
                        ),
                    }))
                );
            });
        });
    }, [brandId]);
    return products;
};

export default useProductsFromBrand;
