// Local Polyfills
import "./remove";
import "./setPrototypeOf";
import "./append";
import "./matches";

// NPM Polyfills
import "@babel/polyfill";
import "core-js/features/set";
import "core-js/features/map";
import "core-js/features/object/";
import "whatwg-fetch";
import "stickyfilljs";
import "promise-polyfill";

// Polyfills that requireing Initialising
import objectFitImages from "object-fit-images";

window.addEventListener("load", () => {
    objectFitImages();
});
