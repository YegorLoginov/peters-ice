import ActionTypes from "@ActionTypes";

const initialState = {
    loading: false,
    error: null,
    tags: [],
};

const contentTagsReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_CONTENT_TAGS_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case ActionTypes.FETCH_CONTENT_TAGS_SUCCESS:
            return {
                ...state,
                loading: false,
                tags: action.tags,
            };
        case ActionTypes.FETCH_CONTENT_TAGS_FAIL:
            return {
                ...state,
                loading: false,
                error: action.error,
            };
        default:
            return state;
    }
};

export default contentTagsReducer;
