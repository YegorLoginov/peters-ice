import { combineReducers } from "redux";

import initialFetchAll from "./initialFetchAll";
import options from "./options";
import page from "./page";
import ui from "./ui";

export default combineReducers({
    initialFetchAll,
    options,
    page,
    ui,
});
