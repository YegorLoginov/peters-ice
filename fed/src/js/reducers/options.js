import ActionTypes from "@ActionTypes";

const initialState = {
    loading: false,
    error: null,
};

const optionsReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_OPTIONS_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case ActionTypes.FETCH_OPTIONS_SUCCESS:
            return {
                ...state,
                loading: false,
                ...action.options,
            };
        case ActionTypes.FETCH_OPTIONS_FAIL:
            return {
                ...state,
                loading: false,
                error: action.error,
            };
        default:
            return state;
    }
};

export default optionsReducer;
