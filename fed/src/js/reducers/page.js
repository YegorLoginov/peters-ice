import ActionTypes from "@ActionTypes";

const initialState = {
    loading: false,
    error: null,
    currentPagePath: "/",
    related: {},
    filters: [],
    featured: [],
};

const pageReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_PAGE_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case ActionTypes.FETCH_PAGE_SUCCESS:
            const page = { ...action.page };
            delete page.related;
            return {
                ...state,
                loading: false,
                [action.page.path]: page,
                related: {
                    ...state.related,
                    ...action.page.related,
                },
            };
        case ActionTypes.FETCH_PAGE_FAIL:
            return {
                ...state,
                loading: false,
                error: action.error,
            };
        case ActionTypes.UPDATE_CURRENT_PAGE_PATH:
            return {
                ...state,
                currentPagePath: action.path,
            };
        case ActionTypes.UPDATE_FILTERS_LIST:
            return {
                ...state,
                filters: action.filters,
            };
        case ActionTypes.UPDATE_FEATURED_LIST:
            return {
                ...state,
                featured: action.featured,
            };
        default:
            return state;
    }
};

export default pageReducer;
