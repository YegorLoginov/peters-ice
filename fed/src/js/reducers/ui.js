import ActionTypes from "@ActionTypes";

const initialState = {
    showMobileMenu: false,
};

const uiReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.TOGGLE_MOBILE_MENU:
            return {
                ...state,
                showMobileMenu: !state.showMobileMenu,
            };
        case ActionTypes.CLOSE_MOBILE_MENU:
            return {
                ...state,
                showMobileMenu: false,
            };
        default:
            return state;
    }
};

export default uiReducer;
