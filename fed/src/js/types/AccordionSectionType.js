// @flow

import { type AccordionType } from "@components/molecules/Accordion";

export type AccordionSectionType = {
    enabled: boolean,
    eyebrow: string,
    title: string,
    accordions: AccordionType[],
};
