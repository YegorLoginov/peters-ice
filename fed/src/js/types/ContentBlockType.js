// @flow

import { LinkType, ImageType } from "@types";

export type ContentBlockType = {
    cta?: LinkType,
    description: string,
    eyebrow?: string,
    title: string,
    type: "image" | "youtube",
    image?: ImageType,
    video_link?: string,
};
