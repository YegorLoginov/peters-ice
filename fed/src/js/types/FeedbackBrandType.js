// @flow

export type FeedbackBrandType = {
    brand: string,
    flavours: Array<{
        product: string,
        type: string,
    }>,
};
