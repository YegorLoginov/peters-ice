// @flow

export type GenericObjectType = {
    [key: string]: any,
};
