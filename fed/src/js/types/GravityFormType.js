// @flow

export type GravityFormFieldType = {
    id: number,
    type: string,
    label: string,
    placeholder: string,
    content: string,
    maxFileSize?: number,
    [key: string]: any,
};

export type GravityFormType = {
    fields: Array<GravityFormFieldType>,
    id: number,
    confirmations: any,
};
