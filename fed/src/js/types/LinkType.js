// @flow

export type LinkType = {
    url: string,
    title: string,
    target: ?string,
    active?: boolean,
};
