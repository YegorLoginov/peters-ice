// @flow

import { type LinkType } from "./LinkType";

export type ChildLinkType = {
    link: LinkType,
};

export type ItemWithoutChildrenType = {
    has_children: false,
    link: LinkType,
};

export type ItemWithChildrenType = {
    has_children: true,
    label: string,
    children: ChildLinkType[],
};

export type PrimaryNavItemType = ItemWithChildrenType | ItemWithChildrenType;
