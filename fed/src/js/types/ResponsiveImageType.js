// @flow

import { type ImageType } from "./ImageType";

export type ResponsiveImageType = Array<{
    desktop: ImageType,
    mobile?: ImageType,
}>;
