// @flow

export type WhereToBuyAvailableAtSelectorType = "where-to-buy" | "available-at";

export type WhereToBuyType = {
    woolworths: string,
    coles: string,
    uber_eats: string,
    doordash: string,
    peters: string,
};

export type AvailableAtType = {
    title: string,
    subtitle: string,
};
