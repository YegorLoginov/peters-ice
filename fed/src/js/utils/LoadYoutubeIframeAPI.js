// @flow

const LoadYoutubeIframeAPI = (): Promise<any> => {
    if (window.YT) {
        return Promise.resolve();
    }
    return new Promise((resolve: any, reject: any) => {
        window.onYouTubeIframeAPIReady = () => {
            resolve();
        };
        const tag = document.createElement("script");
        tag.src = "https://www.youtube.com/iframe_api";
        if (document.body) {
            document.body.append(tag);
        }
    });
};

export default LoadYoutubeIframeAPI;
