import Store from "../store";
import _ from "lodash";

const addACFData = post => {
    const state = Store.getState();
    const related = _.get(state, `page.related[${post.ID}]`) || {};
    const merged = {
        ...post,
        ...related,
    };
    return merged;
};

export default addACFData;
