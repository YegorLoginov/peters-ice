// Separated into it's own file as we can't use flow to create a FormData object containing File entries.
// See: https://stackoverflow.com/questions/48858804/flow-call-of-method-append-error-with-object-literal

export const convertToFormData = obj => {
    const data = new FormData();
    for (const [key, value] of Object.entries(obj)) {
        // Need to replace inputs with a label like: `label[i]` to be `label[]`
        // This is how gravity forms handles sub fields but we cannot store multiple
        // values under the same key in an object
        const fixedKey = key.replace(/\[.+\]/, "[]");
        data.append(fixedKey, value);
    }
    return data;
};
