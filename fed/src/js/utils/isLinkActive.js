// @flow

import Config from "@Config";

export const isLinkActive = (url: string): boolean => {
    const pathToCleanArray = (path: string): Array<string> =>
        path.split("/").filter((a: any): boolean => !!a);

    const hostRemoved = url.replace(Config.wordpressHost, "");
    const candidate = pathToCleanArray(hostRemoved);
    const current = pathToCleanArray(window.location.pathname);

    let isActive = true;
    for (let [index, segment] of candidate.entries()) {
        if (segment !== current[index]) {
            isActive = false;
            break;
        }
    }

    return isActive;
};

export default isLinkActive;
