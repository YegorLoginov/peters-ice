// @flow

const keyValueToURLParams = (obj: any): string => {
    return Object.keys(obj)
        .map((key: string): any => `${key}=${obj[key]}`)
        .join("&");
};

export default keyValueToURLParams;
