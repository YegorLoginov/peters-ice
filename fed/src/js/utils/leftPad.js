// @flow

// https://stackoverflow.com/questions/13859538/simplest-inline-method-to-left-pad-a-string
const leftPad = (str: string, pad: string = "00"): string => {
    if (str.length > pad.length) {
        return str;
    }
    return (pad + str).slice(-pad.length);
};

export default leftPad;
