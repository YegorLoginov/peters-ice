export const generatePageLinks = (max, current, width) => {
    const filler = "...";
    const pages = [1];

    // Handle page ranges smaller than the width + start + end
    if (max <= width + 2) {
        for (let i = 2; i <= max; i++) {
            pages.push(i);
        }
        return pages;
    }

    // Add filler after the first page
    if (current > width) {
        pages.push(filler);
    }

    // Add the middle numbders
    if (current > max - width) {
        for (let i = max - width; i < max; i++) {
            pages.push(i);
        }
    } else {
        const widthMiddle = Math.floor(width / 2);
        let start = current - widthMiddle;
        if (start > 1) {
            for (let page = start; page < start + width; page++) {
                pages.push(page);
            }
        } else {
            for (let i = 2; i <= width + 1; i++) {
                pages.push(i);
            }
        }
    }

    // Add filler before the last page
    if (pages[pages.length - 1] !== max - 1) {
        pages.push(filler);
    }

    pages.push(max);

    return pages;
};

export default generatePageLinks;
