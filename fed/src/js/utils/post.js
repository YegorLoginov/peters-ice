// @flow

import _ from "lodash";
import Config from "@Config";

import { ImageType, ResponsiveImageType } from "@types";

export const getExcerpt = (post: any): any => {
    return _.get(post, "acf.meta[0].excerpt");
};

export const getFeaturedImage = (
    page: any,
    giveFallBack: boolean = true
): ImageType => {
    const featured_image = _.get(
        page,
        ["acf", "meta", "0", "featured_image"],
        null
    );
    if (featured_image) {
        return featured_image;
    } else {
        return giveFallBack
            ? {
                  url: Config.fallbackFeaturedImage,
              }
            : null;
    }
};

export const getMobileFeaturedImage = (page: any): ImageType => {
    const featured_image = _.get(
        page,
        ["acf", "meta", "0", "mobile_hero_image"],
        null
    );
    if (featured_image) {
        return featured_image;
    } else {
        return null;
    }
};

export const getResponsiveFeaturedImage = (page: any): ResponsiveImageType => {
    return [
        {
            desktop: getFeaturedImage(page),
            mobile: getMobileFeaturedImage(page),
        },
    ];
};

export const normalisePost = (post: any): any => {
    return {
        ...post,
        ID: post.id,
        title: post.title.rendered,
        url: post.link,
    };
};

export default {
    getExcerpt,
    getFeaturedImage,
    getResponsiveFeaturedImage,
    getMobileFeaturedImage,
};
