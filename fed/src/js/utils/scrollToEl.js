// @flow

import anime from "animejs";

const scrollToEl = (element: HTMLElement, duration: number = 500) => {
    const elementOffset = element.getBoundingClientRect().top;
    const scrollPosition = window.scrollY || window.pageYOffset;
    const elMarginTop = window.getComputedStyle(element).marginTop;
    let marginOffset = 0;
    if (elMarginTop) {
        marginOffset = parseInt(window.getComputedStyle(element).marginTop);
    }
    const scrollOffset = elementOffset + scrollPosition - marginOffset;

    anime({
        targets: [document.documentElement, document.body],
        scrollTop: scrollOffset,
        duration: duration,
        easing: "easeInOutQuad",
    });
};

export default scrollToEl;
