.PHONY : help
help :
	@echo "db-dump            : Dump database."
	@echo "docker-restart     : Restart docker instance and reimport the database."
	@echo "wp                 : Run Wordpress CLI. Example: make wp cmd=\"user list\""

NO_COLOR=\x1b[0m
OK_COLOR=\x1b[32;01m
ERROR_COLOR=\x1b[31;01m
WARN_COLOR=\x1b[33;01m

install:
	composer install
	cp config/wp-config-sample.php config/wp-config.php
	cp .env.sample .env
	#docker-compose build
	docker-compose up -d
	@echo "$(WARN_COLOR)"
	@echo "*******************************************************"
	@echo "INSTALL COMPLETE: update the following settings files"
	@echo "1) Update /.env with Docker settings."
	@echo "2) Update /config/wp-config.php with Wordpress settings."
	@echo "*******************************************************"
	@echo "$(NO_COLOR)"

db-dump:
	export $$(cat .env | grep -v ^\# | xargs) && \
		docker exec $${PROJECT_NAME}_mariadb /usr/bin/mysqldump -u wordpress --password=wordpress wordpress > dbs/db.sql

docker-restart:
	docker image prune -f
	docker-compose down
	docker volume prune -f
	docker-compose build
	docker-compose up -d

docker-kill-all:
	docker container stop $(docker container ls -a -q)
	docker system prune -a -f --volumes

wp:
	export $$(cat .env | grep -v ^\# | xargs) && \
		docker run -it --rm --volumes-from $${PROJECT_NAME}_wordpress --network container:$${PROJECT_NAME}_wordpress wordpress:cli ${cmd}
