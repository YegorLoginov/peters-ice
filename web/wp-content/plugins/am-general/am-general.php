<?php
/**
 * Plugin Name:     Am General
 * Description:     General features/functions.
 * Author:          Adrenalin Media
 * Author URI:      http://adrenalinmedia.com.au/
 * Text Domain:     am-general
 * Version:         0.1.0
 *
 * @package Am_General
 */

use Adrenalin\Customisation;
use Adrenalin\CustomisationProductAdmin;
use Adrenalin\Field;
use Adrenalin\GravityFormExtension;
use Adrenalin\templates\FormTemplate;
use Adrenalin\templates\ProductListingTemplate;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

require_once dirname( __FILE__ ) . '/vendor/autoload.php';

add_action( 'rest_api_init', 'am_general_rest_api_init' );
add_action( 'init', 'am_general_register_post_types' );
add_action( 'init', 'am_general_add_rest_nonce_in_preview');

add_filter( 'acf/settings/save_json', 'am_general_acf_settings_save_json' );
add_filter( 'acf/settings/load_json', 'am_general_acf_settings_load_json' );

add_filter( 'acf/format_value/type=post_object', 'am_general_acf_format_value', 99, 3 );

// ensure canonical is always https in live site
add_filter('wpseo_canonical', function ($url) {
    $url = str_replace('http://www.peters.com.au/', 'https://www.peters.com.au/', $url);
    return $url;
});

add_action('init', function () {
    new Field();
    new Customisation();
    new CustomisationProductAdmin();
    new GravityFormExtension();
    new ProductListingTemplate();
    new FormTemplate();
});

// Disable use XML-RPC
add_filter( 'xmlrpc_enabled', '__return_false' );

// Disable X-Pingback to header
add_filter( 'wp_headers', 'disable_x_pingback' );
function disable_x_pingback( $headers ) {
    unset( $headers['X-Pingback'] );

    return $headers;
}

if ( defined( 'WP_CLI' ) && WP_CLI ) {
	WP_CLI::add_command( 'adrenalin', '\Adrenalin\Cli' );
}

/**
 * Callback: rest_api_init.
 */
function am_general_rest_api_init() {
	$controller = new Adrenalin\Rest();
	$controller->register_routes();
}

function am_general_add_rest_nonce_in_preview() {
    add_action('wp_head', function () {
        if (is_preview()) {
            echo "<script>var rest_nonce = '" . wp_create_nonce('wp_rest') . "';</script>";
        }
    });
}

/**
 * Callback: acf/settings/save_json.
 */
function am_general_acf_settings_save_json() {
	$controller = new Adrenalin\Acf();
	return $controller->json_save_point();
}

/**
 * Callback: acf/settings/load_json.
 *
 * @param array $paths An array of paths to load config from.
 */
function am_general_acf_settings_load_json( $paths ) {
	$controller = new Adrenalin\Acf();
	return $controller->json_load_point( $paths );
}

/**
 * Callback: init.
 */
function am_general_register_post_types() {

    // Brand
    $brand = new Adrenalin\PostType( 'Brand' );
    $brand->slug = 'brands';
    $brand->name_single = 'Brand';
    $brand->name_plural = 'Brands';
    $brand->register();

    // Product
    $product = new Adrenalin\PostType( 'Product' );
    $product->slug = 'products';
    $product->name_single = 'Product';
    $product->name_plural = 'Products';
    $product->register();

    register_taxonomy_for_object_type( 'post_tag', 'page');

}

/**
 * Add format hook to allow load acf data for post_object field
 * avoid same ID cause recursive loop.
 */
function am_general_acf_format_value($value, $post_id, $field) {
	global $related;

	if (!empty($value) && is_object($value) && !isset($related[$value->ID])) {
		set_extra_values($value, $related[$value->ID]);
  } elseif (!empty($value) && is_array($value)) {
		foreach ($value as $key => $subvalue) {
			if (!isset($related[$subvalue->ID])) {
				set_extra_values($subvalue, $related[$subvalue->ID]);
			}
		}
	}

	return $value;
}

/**
 * Set extra fields for post_object
 *
 */
function set_extra_values($org_value, &$related) {
	$related['url'] = get_permalink( $org_value->ID );
	$related['title'] = $org_value->post_title;
	$related['acf'] = get_fields( $org_value->ID );
	if ($org_value->post_type == 'page') {
		$related['page_template'] = get_page_template_slug($org_value);
		$ancestors = get_post_ancestors($org_value);
		if (count($ancestors)) {
			$ancestor_id = array_pop($ancestors);
			$related['top_ancestor'] = ['id' => $ancestor_id,
																	'title' => get_the_title($ancestor_id), 
																	'url' => get_page_link($ancestor_id)];
		} else {
			$related['top_ancestor'] = FALSE;
		}
	} else {
		//attache tags
		$terms = get_the_tags($org_value->ID);
		if ($terms) {
			foreach ($terms as $term) {
				$related['tags_input'][] = $term->slug;
                $related['tags_object'][] = [
                    'name' => html_entity_decode($term->name),
                    'slug' => $term->slug
                ];
			}
		}
	}
}

