<?php
/**
 * ACF
 *
 * @package Am_General
 */

namespace Adrenalin;

/**
 * Class ACF.
 */
class Acf {

	/**
	 * The path to load and save JSON config.
	 *
	 * @var string
	 */
	protected $path = ABSPATH . '../config/acf';


	/**
	 * The path to load and save JSON config in WPE.
	 *
	 * @var string
	 */
	protected $wpe_path = ABSPATH . '/config/acf';

	/**
	 * Filter callback.
	 *
	 * @see filter acf/settings/save_json
	 */
	public function json_save_point() {
		return $this->path;
	}

	/**
	 * Filter callback.
	 *
	 * @param array $paths An array of paths to load config from.
	 * @see filter acf/settings/load_json
	 */
	public function json_load_point( $paths ) {
		// Remove original path.
		unset( $paths[0] );
		if (defined('WPE_APIKEY')) {
			$paths[] = $this->wpe_path;
		} else {
			$paths[] = $this->path;
		}
		return $paths;
	}
}
