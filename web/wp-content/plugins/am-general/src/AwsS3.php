<?php
/**
 * Aws
 *
 * @package Am_General
 */

namespace Adrenalin;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

/**
 * Class Aws.
 */
class AwsS3 {

	/**
	 * Filter callback.
	 *
	 * @param array $file_path The location of the file.
	 * @param array $file_name The name of the file.
	 */
	public static function upload( $file_path, $file_name ) {
		if ( ! defined( 'AS3CF_SETTINGS' ) ) {
			throw new Exception( 'Error', 1 );
		}

		$settings = unserialize( AS3CF_SETTINGS );

		$s3 = new S3Client([
			'version'     => 'latest',
			'region'      => $settings['region'],
			'credentials' => [
				'key'    => $settings['access-key-id'],
				'secret' => $settings['secret-access-key'],
			],
		]);

		try {
			// Upload data.
			$result = $s3->putObject([
				'Bucket' => $settings['bucket'],
				'Key'    => $file_name,
				'Body'   => fopen( $file_path, 'r' ),
				'ACL'    => 'public-read',
			]);

			// Print the URL to the object.
			return $result['ObjectURL'] . PHP_EOL;
		} catch ( S3Exception $e ) {
			return $e->getMessage() . PHP_EOL;
		}
	}
}
