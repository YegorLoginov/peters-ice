<?php
/**
 * Captcha
 *
 * @package Am_General
 */

namespace Adrenalin;

/**
 * Class Captcha.
 */
class CampaignMonitor {

	#private static $client_id;
	//api credentials goes here
	private static $crendetails;
	private static $receipt_list_id;


	private static $service_url_prefix;

	private static $initialized = false;


	private static function init() {
		//Initialize static properties.
		#static::$client_id = get_option('cm_client_id','');
		static::$crendetails = get_field('cm_token','option');
		static::$receipt_list_id = get_field('cm_receipt_id','option');
		static::$service_url_prefix = get_field('cm_service_url', 'option');
		static::$initialized = true;
	}

	/**
	 * add a new subscriber in list
	 * @param string $email
	 * @param string $name
	 * @return mixed
	 */
	public static function addSubscriber($email, $name) {
		if (!static::$initialized) {
			static::init();
		}
		$service_url = static::$service_url_prefix . '/subscribers/' . static::$receipt_list_id . '.json';

		$curl_post_data = array("EmailAddress" => $email,
				"Name" => $name,
				"Resubscribe" => true,
				"RestartSubscriptionBasedAutoresponders" => true,
				"ConsentToTrack" => 'Yes'
		);
		return static::request($service_url, $curl_post_data);
	}

	/**
	 * send request to campagin monitor
	 * @param string $service_url
	 * @param array $curl_post_data
	 * @return mixed
	 */
	private static function request($service_url, $curl_post_data) {
		$curl_post_data = json_encode($curl_post_data);
		$curl = curl_init($service_url);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_USERPWD, static::$crendetails);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($curl_post_data))
		);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$curl_response = curl_exec($curl);
		if (empty($curl_response)) {
			//server can't connected
			$curl_response['message'] = "Can't connect to the server.";
			$curl_response['code'] = "999";
			$response = $curl_response;
		} else if (strpos($curl_response, 'error') > -1){
			//response text contanis error
			$curl_response = new \stdClass();
			$curl_response['message']  = "Can't connect to the server due to server error.";
			$curl_response['code']= "998";
			$response = $curl_response;
		} else {
			$response['success'] = true;
		}

		curl_close($curl);
		return $response;
	}

	/**
	 * hide cm data in rest request
	 * @param array $data
	 * @return array
	 */
	public static function fileterRestAPI($data) {
		if (isset($data['acf'])) {
			foreach($data['acf'] as $key => $val) {
				if (substr($key,0,3) == 'cm_') {
					unset($data['acf'][$key]);
				}
			}
		}
		return $data;
	}

}
