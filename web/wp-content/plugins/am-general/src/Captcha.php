<?php
/**
 * Captcha
 *
 * @package Am_General
 */

namespace Adrenalin;

use ReCaptcha\ReCaptcha;

/**
 * Class Captcha.
 */
class Captcha {

	/**
	 * The threshold to determine whether a submission is genuine.
	 *
	 * @see \ReCaptcha\setScoreThreshold()
	 * @var float
	 */
	protected $threshold = 0.7;

	/**
	 * The action to match against.
	 *
	 * @see \ReCaptcha\setExpectedAction()
	 * @var string
	 */
	protected $action;

	/**
	 * The site's hostname without http/s or trailing slash.
	 *
	 * @see \ReCaptcha\setExpectedHostname()
	 * @return string
	 */
	public function getHostname() {
		$urlParts = parse_url( WP_SITEURL );
		return $urlParts['host'];
		return preg_replace( '#^http(s)?://#', '', rtrim( WP_SITEURL, '/' ) );
	}

	/**
	 * Constructor.
	 *
	 * @param string $action
	 */
	public function __construct( string $action ) {
		$this->action = $action;
	}

	/**
	 * Filter callback.
	 *
	 * @param string $response The user response token provided by reCAPTCHA.
	 */
	public function verify( $response = null ) {
		$recaptcha = new ReCaptcha( GOOGLE_RECAPTCHA_SECRET_KEY );
		$remoteIp  = $_SERVER['REMOTE_ADDR'];

		// reCaptcha v3
		// $resp = $recaptcha->setExpectedHostname( $this->getHostname() )
		// ->setExpectedAction( $this->action )
		// ->setScoreThreshold( $this->threshold )
		// ->verify( $response, $remoteIp );
		// reCaptcha v2
		$resp = $recaptcha->verify( $response, $remoteIp );

		// Check that the captcha was solved on the site
		$valid_hostname = $this->getHostname() === $resp->getHostname();

		if ( $valid_hostname && $resp->isSuccess() ) {
			// Verified!
			return true;
		} else {
			return $errors = $resp->getErrorCodes();
		}
	}
}
