<?php
/**
 * Cli
 *
 * @package Am_General
 */

namespace Adrenalin;

use WP_CLI;
use WP_CLI_Command;

/**
 * Class Cli.
 */
class Cli extends WP_CLI_Command {

	/**
	 * Clear all caches.
	 */
	public function clear_all() {
		if ( $this->plugin_is_active( 'w3-total-cache' ) ) {
			$this->wp( 'total-cache flush all' );
		}

		$this->wp( 'cache flush' );
		$this->wp( 'rewrite flush' );
	}

	/**
	 * Determine if plugin is enabled.
	 *
	 * @param string $name The name of the plugin.
	 */
	protected function plugin_is_active( $name ) {
		$plugin = $this->wp( "plugin is-active $name", [ 'return' => 'all' ] );
		return isset( $plugin->return_code ) && 0 === $plugin->return_code;
	}

	/**
	 * Run a WP_CLI command.
	 *
	 * @param string $command The command to run with arugments.
	 * @param array  $options Additional options.
	 */
	protected function wp( $command, $options = [] ) {
		return WP_CLI::runcommand( $command, $options );
	}

	/**
	 * Print a message.
	 *
	 * @param string $command The command to run with arugments.
	 * @param array  $options Additional options.
	 */
	protected function print( $message, $type = 'success' ) {
		if ( is_array( $message ) ) {
			$message = serialize( $message );
		}

		WP_CLI::success( $message );
	}

	/**
	 * Load and decode a JSON file.
	 *
	 * @param string $file The path to the JSON file to load.
	 */
	protected function load_json( $file ) {
		if ( ! file_exists( $file ) ) {
			throw new \InvalidArgumentException( sprintf( 'File not found: %s', $file ) );
		}

		if ( pathinfo( $file, PATHINFO_EXTENSION ) !== 'json' ) {
			throw new \InvalidArgumentException( sprintf( 'File is not JSON: %s', $file ) );
		}

		return json_decode( file_get_contents( $file ), true );
	}

	/**
	 * Export plugin status list.
	 */
	public function export_active_plugins() {
		// @TODO config this.
		$path = ABSPATH . '../config/system/';
		$file = 'plugins.json';

		// Remove trailing slash.
		$path = untrailingslashit( $path );

		// Bail early if dir does not exist.
		if ( ! is_writable( $path ) ) {
			return false;
		}

		// Get data.
		$get_active_plugins = get_option( 'active_plugins' );
		$active_plugins     = [];

		// Get a list of currently activate plugins.
		foreach ( $get_active_plugins as $value ) {
			$parts            = explode( '/', $value );
			$active_plugins[] = $parts[0];
		}

		$json = json_encode( $active_plugins, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE );

		$f = fopen( "{$path}/{$file}", 'w' );
		fwrite( $f, $json );
		fclose( $f );
	}

	/**
	 * Sync plugins from exported JSON list.
	 */
	public function sync_plugins() {
		// @TODO make this path configurable like ACF export path.
		$plugin_updates = $this->load_json( ABSPATH . '../config/system/plugins.json' );

		if ( empty( $plugin_updates ) ) {
			$this->print( 'No plugins set in config.' );
			return;
		}

		// Plugins that are currently active.
		$active_plugins = [];

		// Plugins that need activating.
		$plugins_to_activate = [];

		// Get a list of currently activate plugins.
		$get_active_plugins = get_option( 'active_plugins' );

		foreach ( $get_active_plugins as $value ) {
			$parts            = explode( '/', $value );
			$active_plugins[] = $parts[0];
		}

		// Compare current plugin status to plugins in config.
		foreach ( $plugin_updates as $name ) {
			$key = array_search( $name, $active_plugins );

			if ( $key !== false ) {
				// Remove from the active plugins list
				unset( $active_plugins[ $key ] );
			} else {
				// Activate the plugin if it is non-active.
				$plugins_to_activate[] = $name;
			}
		}

		// Activate plugins
		if ( $plugins_to_activate ) {
			$plugin_string = implode( ' ', $plugins_to_activate );
			$this->wp( "plugin activate $plugin_string" );
		}

		// De-activate remaining plugins
		if ( $active_plugins ) {
			$plugin_string = implode( ' ', $active_plugins );
			$this->wp( "plugin deactivate $plugin_string" );
		}

		WP_CLI::success( 'Sync finished.' );

		$this->clear_all();
	}

	/**
	 * Sync ACF fields.
	 * Modified from \acf_admin_field_groups->check_sync
	 */
	public function sync_acf() {
		if ( ! $this->plugin_is_active( 'advanced-custom-fields-pro' ) ) {
			return;
		}

		acf_update_setting( 'json', false );

		$sync   = [];
		$groups = acf_get_field_groups();

		// Bail early if no field groups.
		if ( empty( $groups ) ) {
			return;
		}

		// Find JSON field groups which have not yet been imported.
		foreach ( $groups as $group ) {

			// Vars.
			$local    = acf_maybe_get( $group, 'local', false );
			$modified = acf_maybe_get( $group, 'modified', 0 );
			$private  = acf_maybe_get( $group, 'private', false );

			// Ignore DB / PHP / private field groups.
			if ( $local !== 'json' || $private ) {

				// Do nothing.
			} elseif ( ! $group['ID'] ) {

				$sync[ $group['key'] ] = $group;

			} elseif ( $modified && $modified > get_post_modified_time( 'U', true, $group['ID'], true ) ) {

				$sync[ $group['key'] ] = $group;

			}
		}

		// Bail if no sync needed.
		if ( empty( $sync ) ) {
			return;
		}

		foreach ( $sync as $key => $v ) {

			// Append fields.
			if ( acf_have_local_fields( $key ) ) {

				$sync[ $key ]['fields'] = acf_get_local_fields( $key );

			}
			// Import.
			$field_group = acf_import_field_group( $sync[ $key ] );
		}

		WP_CLI::success( 'Sync successful' );

		$this->clear_all();
	}

	/**
	 * Sync Wordfence settings.
	 *
	 * @param string $token (optional) The token given when exporting the settings.
	 */
	public function sync_wordfence( $token = null ) {
		if ( ! $this->plugin_is_active( 'wordfence' ) ) {
			WP_CLI::error( 'Wordfence is not enabled.' );
			return;
		}

		if ( ! $token ) {
			// @TODO config this
			$json  = $this->load_json( ABSPATH . '../config/system/wordfence.json' );
			$token = $json['token'];
		}

		// Import settings
		\wordfence::importSettings( $token );

		WP_CLI::success( 'Wordfence settings sync successful.' );

		$this->clear_all();
	}

	/**
	 * Update Wordfence htaccess directives and other files added by Wordfence.
	 */
	public function update_wordfence_files() {
		if ( ! $this->plugin_is_active( 'wordfence' ) ) {
			WP_CLI::error( 'Wordfence is not enabled.' );
			return;
		}

		global $wp_filesystem;

		// @TODO get this somehow
		$serverConfiguration          = 'apache-mod_php';
		$allow_relaxed_file_ownership = true;
		$helper                       = new \wfWAFAutoPrependHelper( $serverConfiguration, null );

		if ( ! $helper::isValidServerConfig( $serverConfiguration ) ) {
			WP_CLI::error( 'Wordfence invalid server configuration.' );
			return;
		}

		// Modified from \wordfence::ajax_installAutoPrepend_callback().
		$ajaxURL     = admin_url( 'admin-ajax.php' );
		$credentials = request_filesystem_credentials( $ajaxURL, '', false, ABSPATH, array( 'version', 'locale', 'action', 'serverConfiguration', 'currentAutoPrepend' ), $allow_relaxed_file_ownership );

		if ( WP_Filesystem( $credentials, ABSPATH, $allow_relaxed_file_ownership ) ) {
			$helper->performInstallation( $wp_filesystem );

			WP_CLI::success( 'Wordfence files added/updated successfully.' );
		} else {
			WP_CLI::error( 'Wordfence error with file permissions.' );
		}
	}

}
