<?php
/**
 * Created by PhpStorm.
 * User: dion.tsai
 * Date: 3/03/2020
 * Time: 3:14 PM
 */

namespace Adrenalin;


class CommonHelper
{
    public static function parse_document($content) {
        $title = null;
        $json_ld = [];
        $internal_errors = libxml_use_internal_errors(true);

        $dom = new \DOMDocument();
        $dom->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));

        // handle title
        $nodes = $dom->getElementsByTagName('title');
        if ($nodes->length > 0) {
            $title = esc_html(wp_strip_all_tags(stripslashes($nodes[0]->nodeValue), true));
        }

        // handle metas
        $yoast_metas = [];
        $metas = $dom->getElementsByTagName('meta');
        foreach ($metas as $meta) {
            if ($meta->hasAttributes()) {
                $yoast_meta = [];
                foreach ($meta->attributes as $attr) {
                    $yoast_meta[$attr->nodeName] = esc_html(wp_strip_all_tags(stripslashes($attr->nodeValue), true));
                }
                $yoast_metas[] = $yoast_meta;
            }
        }

        // handle json ld
        $xpath = new \DOMXPath($dom);
        foreach ($xpath->query('//script[@type="application/ld+json"]') as $node) {
            $json_ld[] = json_decode((string)$node->nodeValue, true);
        }

        libxml_use_internal_errors($internal_errors);
        return ['title' => $title, 'meta' => $yoast_metas, 'json_ld' => $json_ld];
    }

    public static function keep_properties_for_array(array &$arr, array $props_to_keep)
    {
        foreach ($arr as &$obj) {
            self::keep_properties($obj, $props_to_keep);
        }
    }

    public static function keep_properties(&$obj, array $props_to_keep)
    {
        if (is_array($obj)) {
            foreach ($obj as $key => $val) {                
                if (!in_array($key, $props_to_keep)) {
                    unset($obj[$key]);
                }
            }
        } else {
            // to do for object
        }        
    }
}