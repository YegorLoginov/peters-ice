<?php
/**
 * Created by PhpStorm.
 * User: dion.tsai
 * Date: 12/03/2020
 * Time: 1:13 PM
 */

namespace Adrenalin;

class Customisation
{
    public function __construct()
    {
        foreach (get_post_types() as $post_type) {
            add_filter('rest_prepare_' . $post_type, [$this, 'wp_rest_modify_common_keys'], 10, 3);
        }

        add_filter('adrenalin_rest_page_parse', [$this, 'wp_handle_encoding_for_page_api'], 10, 1);
        add_filter('rest_post_dispatch', [$this, 'wp_filter_rest_tag_dispatch'], 10, 3);
        add_filter('rest_products_query', [$this, 'wp_rest_brand_filters_for_product_post_type'], 10, 2);

        // add new post filter
        // add_filter('wp_insert_post_data', [$this, 'wp_insert_post_with_last_menu_order'], 10, 2);
    }

    /**
     * Rest customisation to support "brands" query string
     * @param $args
     * @param $request
     * @return mixed
     */
    function wp_rest_brand_filters_for_product_post_type($args, $request)
    {
        $brand_ids = !empty($request['brands']) ? explode(',', $request['brands']) : null;
        if (!empty($brand_ids)) {
            $args['meta_query'] = array(
                array(
                    'key' => 'brand',
                    'value' => $brand_ids,
                )
            );
        }
        return $args;
    }

    /**
     * Remove the 'rendered' prop in 'title'. And add permalink in 'url'.
     * @param $data
     * @param $post
     * @param $context
     * @return mixed
     */
    function wp_rest_modify_common_keys($data, $post, $context)
    {
        $url = get_permalink($data->data['id']);
        $data->data['title'] = html_entity_decode($data->data['title']['rendered']);
        $data->data['url'] = $url;
        return $data;
    }

    /**
     * perform html decode in tags_input prop
     * @param $response
     * @return mixed
     */
    function wp_handle_encoding_for_page_api($response)
    {
        if (!empty($response['tags_input']) && is_array($response['tags_input'])) {
            foreach ($response['tags_input'] as &$tag_input) {
                $tag_input = html_entity_decode($tag_input);
            }
        }
        return $response;
    }

    /**
     * perform html decode in tag names
     * @param $rest_ensure_response
     * @param $instance
     * @param \WP_REST_Request $request
     * @return mixed
     */
    function wp_filter_rest_tag_dispatch($rest_ensure_response, $instance, \WP_REST_Request $request)
    {
        if ($request->get_route() === '/wp/v2/tags' && $request->get_method() === 'GET') {
            foreach ($rest_ensure_response->data as $key => &$item) {
                $item['name'] = html_entity_decode($item['name']);
            }
        }
        return $rest_ensure_response;
    }

    /**
     * Insert products/brands with the last menu_order
     * @param $data
     * @param $post
     * @return mixed
     */
    function wp_insert_post_with_last_menu_order($data, $post) {
        global $wpdb;        

        $allowed_post_types = ['products', 'brands'];
        $post_obj = get_post($post['ID']);

        if (in_array($data['post_type'], $allowed_post_types) && !empty($post_obj) && ($post_obj->post_status == 'auto-draft')) {
            $data['menu_order'] = $wpdb->get_var("SELECT MAX(menu_order)+1 AS menu_order FROM {$wpdb->posts} WHERE post_type='${data['post_type']}'");
        }
        return $data;
    }
}