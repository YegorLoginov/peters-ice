<?php
/**
 * Created by PhpStorm.
 * User: dion.tsai
 * Date: 12/03/2020
 * Time: 1:13 PM
 */

namespace Adrenalin;

class CustomisationProductAdmin
{
    public function __construct()
    {
        add_filter('manage_products_posts_columns', [$this, 'edit_product_add_columns']);
        add_filter('manage_products_posts_custom_column', [$this, 'edit_product_add_column_values'], 10, 2);
        add_action('admin_head', [$this, 'edit_product_add_column_style']);

        $this->edit_product_add_brand_filter();
    }

    function edit_product_add_columns($columns)
    {
        if (empty($_GET['admin_filter_brand'])) {
            $columns = array_slice($columns, 0, 2, true) +
                ['brand' => 'Brand'] +
                array_slice($columns, 2, count($columns) - 1, true);
        }

        return $columns;
    }

    function edit_product_add_column_values($column, $post_id)
    {        
        switch ($column) {
            case 'brand' :                
                $fields = get_field_objects($post_id);                
                if (!empty($fields['brand']) && $fields['brand']['value'] instanceof \WP_Post) {
                    echo '<a href="/wp-admin/edit.php?post_type=products&admin_filter_brand=' . $fields['brand']['value']->ID . '&filter_action=Filter">' . $fields['brand']['value']->post_title . '</a>';
                }
                break;
        }
    }

    function edit_product_add_column_style()
    {
        echo '<style type="text/css">';
        echo '.manage-column.column-brand { width: 150px; }';
        echo '</style>';
    }

    function edit_product_add_brand_filter()
    {
        if (is_admin()) {
            //this hook will create a new filter on the admin area for the specified post type
            add_action('restrict_manage_posts', function () {
                global $wpdb;
                $post_type = (isset($_GET['post_type'])) ? $_GET['post_type'] : 'post';
                if ($post_type == 'products') {
                    $values = [];
                    $result = $wpdb->get_results("select id, post_title from wp_posts where post_type = 'brands' order by post_title;");
                    foreach ($result as $data) {
                        $values[$data->id] = $data->post_title;
                    }
                    ?><select name="admin_filter_brand">
                    <option value="">All Brands</option>
                    <?php
                    $current_v = isset($_GET['admin_filter_brand']) ? $_GET['admin_filter_brand'] : '';
                    foreach ($values as $value => $label) {
                        printf('<option value="%s"%s>%s</option>',
                            $value,
                            $value == $current_v ? ' selected="selected"' : '',
                            $label
                        );
                    }
                    ?>
                    </select>
                    <?php
                }
            });

            //this hook will alter the main query according to the user's selection of the custom filter we created above:
            add_filter('parse_query', function ($query) {
                global $pagenow;
                $post_type = (isset($_GET['post_type'])) ? $_GET['post_type'] : 'post';
                if ($post_type == 'products' && $pagenow == 'edit.php' && isset($_GET['admin_filter_brand']) && !empty($_GET['admin_filter_brand'])) {
                    $query->set('meta_query', [
                        [
                            'key' => 'brand',
                            'value' => $_GET['admin_filter_brand'],
                            'compare' => '='
                        ],
                    ]);
                }
            });
        }
    }
}