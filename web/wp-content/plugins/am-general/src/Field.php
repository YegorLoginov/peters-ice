<?php
/**
 * Created by PhpStorm.
 * User: dion.tsai
 * Date: 3/03/2020
 * Time: 2:19 PM
 */

namespace Adrenalin;

use Adrenalin\fields\GravityFormField;
use Adrenalin\fields\ProductListingTemplateBrandsField;
use Adrenalin\fields\ProductListingTemplateProductTagsField;

class Field
{
    private $field_transform_map = [];
    private $post_ids_map = [];

    public function __construct()
    {
        add_filter('am_transform_acf_fields', [$this, 'transform_acf_fields'], 2, 10);

        $this->init();
    }

    public function init()
    {
        $field_transforms[] = new GravityFormField();

        foreach ($field_transforms as $field) {
            if(!empty($field->acf_field_key)) {
                $this->field_transform_map[$field->acf_field_key] = $field;
            }
        }
    }

    public function transform_acf_fields($fields, $postId)
    {
        if (empty($fields))
            return $fields;

        foreach ($fields as $key => $value) {
            if (!empty($this->field_transform_map[$key])) {
                $fields[$key] = $this->field_transform_map[$key]->transform($key, $value, $postId);
            }
        }
        return $fields;
    }
}
