<?php
/**
 * Files upload
 *
 * @package Am_General
 */

namespace Adrenalin;

/**
 * Class filesupload.
 */
class FilesUpload {

	/**
	 * A list of allowed file types.
	 *
	 * @var array
	 */
	protected $allowed_file_types = [
		'doc'  => 'application/msword',
		'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		'odt'  => 'application/vnd.oasis.opendocument.text ',
		'pdf'  => 'application/pdf',
		'txt'  => 'text/plain',
	];

	/**
	 * The maximum file upload size in bytes.
	 *
	 * @var int
	 */
	protected $max_file_size = 2000000;

	/**
	 * Any additional values to replace defaults values.
	 *
	 * @param array $files The files to upload.
	 */
	public function __construct( $files ) {
		$this->file = $this->getFile( $files );
	}

	/**
	 * Get the uploaded file.
	 */
	public function getFile( $file ) {
		if ( is_uploaded_file( $file['tmp_name'] ) !== true ) {
			throw new \Exception( 'Error Processing Request', 1 );
		}

		return $file;
	}

	/**
	 * Create an impossible to guess file name.
	 */
	public function createRandomFileName() {
		return wp_generate_password( 50, false ) . '-' . $this->getCleanFilename();
	}

	/**
	 * Create a safe file name.
	 */
	public function getCleanFilename() {
		$filename = pathinfo( $this->file['name'], PATHINFO_FILENAME );
		$filename = preg_replace( '/[^\w-]/', '', $filename );

		return $filename . '.' . $this->getExtension();
	}

	/**
	 * Create a safe file name.
	 */
	public function getExtension() {
		return pathinfo( $this->file['name'], PATHINFO_EXTENSION );
	}

	/**
	 * Get any errors during file upload.
	 */
	public function getUploadErrors() {
		if ( $this->file['error'] !== 0 ) {
			return $this->file['error'];
		}

		return '';
	}

	/**
	 * Determine if the file is under the size limit.
	 */
	public function isValidSize() {
		return $this->file['size'] <= $this->max_file_size;
	}

	/**
	 * Determine if the file is valid.
	 */
	public function isValidType() {
		$ext = $this->getExtension();

		if ( ! isset( $this->allowed_file_types[ $ext ] ) ) {
			return false;
		}

		$valid_mime_type  = $this->allowed_file_types[ $ext ];
		$actual_mime_type = $this->file['type'];

		return $valid_mime_type === $actual_mime_type;

	}
}
