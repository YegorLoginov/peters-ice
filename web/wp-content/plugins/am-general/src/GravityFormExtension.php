<?php
/**
 * Created by PhpStorm.
 * User: dion.tsai
 * Date: 5/03/2020
 * Time: 10:23 AM
 */

namespace Adrenalin;


class GravityFormExtension
{
    const GRAVITY_PRODUCT_FEEDBACK_FORM_ID = 3;
    const GRAVITY_GENERAL_ENQUIRY_FORM_ID = 4;

    public function __construct()
    {
        add_filter('gform_notification_' . self::GRAVITY_PRODUCT_FEEDBACK_FORM_ID, [$this, 'change_notification_format'], 10, 3);
        add_filter('gform_notification_' . self::GRAVITY_GENERAL_ENQUIRY_FORM_ID, [$this, 'change_notification_format'], 10, 3);
    }

    function format_crs_plain_text_emails($args)
    {
        $args['message'] = str_replace(array("\n\n", "\r"), "\r", $args['message']);
        return $args;
    }

    function change_notification_format($notification, $form, $entry)
    {
        // format the plain text message to remove line breaks
        add_filter('wp_mail', [$this, 'format_crs_plain_text_emails'], 10, 1);

        // change notification format to text from the default html for General Enquiry and Product Feedback form
        $notification['message_format'] = 'text';
        return $notification;
    }

//    public function send_to_crs($entry, $form)
//    {
//        if ($entry['form_id'] == GravityFormExtension::GRAVITY_GENERAL_ENQUIRY_FORM_ID) {
//            $this->send_general_enquiry_entry_to_crs($entry, $form);
//        }
//    }
//
//    private function send_general_enquiry_entry_to_crs($entry, $form)
//    {
//        $field_labels = [
//            '1' => 'First Name',
//            '2' => 'Last Name',
//            '3' => 'Email',
//            '4' => 'Phone Number',
//            '6' => 'POSTAL ADDRESS',
//            '8' => 'Street No',
//            '9' => 'Street Name',
//            '13' => 'Street Type',
//            '10' => 'Suburb',
//            '11' => 'Postcode',
//            '12' => 'State',
//            '7' => 'POSTAL ADDRESS',
//            '5' => 'Message',
//            '14' => 'Comments',
//        ];
//
//        $this->send_crs_email($form, $field_labels, $entry);
//    }
//
//    private function build_crs_body($field_labels, $entry)
//    {
//        $body = '';
//        foreach ($field_labels as $key => $field_label) {
//            if (!empty($entry[$key])) {
//                $body .= "{$field_label}={$entry[$key]}&";
//            }
//        }
//        return $body;
//    }
//
//    private function send_crs_email($form, $field_labels, $entry)
//    {
//        $body = $this->build_crs_body($field_labels, $entry);
//
//        add_filter('wp_mail_from', [$this, 'wp_mail_change_from_address']);
//        add_filter('wp_mail_from_name', [$this, 'wp_mail_change_from_name']);
//        wp_mail('peters@cybercrs.net', 'Peters Website - ' . $form['title'], $body);
//        remove_filter('wp_mail_from', [$this, 'wp_mail_change_from_address']);
//        remove_filter('wp_mail_from_name', [$this, 'wp_mail_change_from_name']);
//    }
//
//    public function wp_mail_change_from_address()
//    {
//        return 'no-reply@peters.com.au';
//    }
//
//    public function wp_mail_change_from_name()
//    {
//        return 'Peters Website';
//    }
//
//    private function helper_output_fields($form)
//    {
//        foreach ($form['fields'] as $field) {
//            echo "'{$field->id}' => '{$field->label}'," . PHP_EOL;
//        }
//        die;
//    }

}