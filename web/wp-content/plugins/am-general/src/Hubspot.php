<?php
/**
 * ACF
 *
 * @package Am_General
 */

namespace Adrenalin;

/**
 * Class Hubspot.
 */
class Hubspot {

	/**
	 * The path to the Hubspot API endpoint.
	 *
	 * @var string
	 */
	protected $getApi = 'https://api.hubapi.com/forms/v2/forms/';

	/**
	 * The path to the Hubspot API form POST endpoint.
	 *
	 * @var string
	 */
	protected $postApi = 'https://api.hsforms.com/submissions/v3/integration/submit/';
	// protected $postApi = 'https://forms.hsforms.com/submissions/v3/public/submit/formsnext/multipart/';

	/**
	 * Remove keys.
	 *
	 * @var array
	 */
	protected $remove_keys = [
		'followUpId',
		'leadNurturingCampaignId',
		'migratedFrom',
		'notifyRecipients',
		'deletable',
		'campaignGuid',
		'cloneable',
		'editable',
		'formType',
		'deletedAt',
		'parentId',
		'portalId',
		'name',
		'action',
		'method',
		'createdAt',
		'updatedAt',
		'performableHtml',
		'ignoreCurrentValues',
		'tmsId',
		'captchaEnabled',
		'themeName',
	];

	/**
	 * Determine if the given ID is a valid HubSpot ID.
	 *
	 * @param string $id The ID to validate.
	 */
	public static function isValidId( $id ) {
		// Letters, numbers, hyphons
		return preg_match( '/^[A-Za-z0-9-]+$/', $id );
	}

	/**
	 * Get a Hubspot form in JSON format.
	 *
	 * @param string $id The Hubspot form id.
	 */
	public function get_form_by_id( $id ) {
		if ( ! self::isValidId( $id ) ) {
			return [
				'status'  => 'error',
				'message' => 'Invalid form ID.',
			];
		}

		$url = $this->getApi . $id . '?hapikey=' . HUBSPOT_API_KEY;

		try {
			$client           = new \WP_Http();
			$hubspot_response = $client->get( $url );
			$response         = json_decode( $hubspot_response['body'], true );

			if ( isset( $response['guid'] ) ) {
				foreach ( $this->remove_keys as $key ) {
					unset( $response[ $key ] );
				}
			}
		} catch ( \Exception $e ) {
			$response = $e->messageBody();
		}

		 return $response;
	}

	/**
	 * Format the form field values to Hubspot's required formatting.
	 *
	 * @param array $form_fields The form field values.
	 */
	public function formatPayload( $form_fields ) {
		$payload = [
			'fields'  => [],
			'context' => [
				'hutk'           => $_COOKIE['hubspotutk'],
				'ipAddress'      => $_SERVER['REMOTE_ADDR'],
				'pageName'       => '',
				'pageUri'        => '',
				'sfdcCampaignId' => '',
			],
		];

		// Add context
		if ( ! empty( $form_fields['pageTitle'] ) ) {
			$payload['context']['pageName'] = sanitize_text_field( $form_fields['pageTitle'] );
		}

		if ( ! empty( $form_fields['pageUrl'] ) ) {
			if ( strncmp( $form_fields['pageUrl'], WP_SITEURL, strlen( WP_SITEURL ) ) === 0 ) {
				$payload['context']['pageUri'] = $form_fields['pageUrl'];
			}
		}

		// Remove meta data
		foreach ([
			'guid',
			'captcha',
			'context',
			'pageTitle',
			'pageUrl',
		] as $remove ) {
			unset( $form_fields[$remove] );
		}

		// Format Hubspot form fields
		foreach ( $form_fields as $key => $value ) {
			$payload['fields'][] = [
				'name'  => $key,
				'value' => $value,
			];
		}

		return $payload;
	}

	/**
	 * Post form data to HubSpot
	 *
	 * @param string $id The Hubspot form id.
	 * @param array  $data The form data.
	 */
	public function post_form( $id, $data = [], $context = [] ) {
		if ( empty( $data ) ) {
			return [ 'error' => 'No data sent.' ];
		}

		if ( ! self::isValidId( $id ) ) {
			return [ 'error' => 'Invalid form ID.' ];
		}

		$body = $this->formatPayload( $data, $context );
		$url  = $this->postApi . HUBSPOT_PORTAL_ID . '/' . $id;

		try {
			$client   = new \WP_Http();
			$headers  = [ 'Content-Type' => 'application/json' ];
			$response = $client->post( $url, [
				'body'    => json_encode( $body ),
				'headers' => $headers,
			]);

			$body = json_decode( $response['body'], true );

			if ( $response['response']['code'] === 200 ) {
				return [
					'success' => true,
					'message' => $body['inlineMessage'],
				];
			}

			return [
				'error_code' => $response['response']['code'],
				'error'      => $response['response']['message'],
				'messages'   => $body['errors'],
			];

		} catch ( \Exception $e ) {
			return [ 'error' => $e->messageBody() ];
		}
	}
}
