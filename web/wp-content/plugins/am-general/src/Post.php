<?php
/**
 * Post helpers.
 *
 * @package Am_General
 */

namespace Adrenalin;

/**
 * Class Post.
 */
class Post {

    private static function get_post_id_by_path($path)
    {
        if ( '/' === $path || '' === $path ) {
            $post_id = get_option( 'page_on_front' );
        } else {
            $post_id = url_to_postid( $path );
        }
        return $post_id;
    }

    private static function get_post( $post_id ) {
        if (empty($post_id)) {
            return;
        }

        $post = get_post( $post_id );

        /**
         * The following is taken from WP_Query->get_posts().
         * /wp-includes/class-wp-query.php.
         * Required parameters: preview_id, preview_nonce, preview, _wpnonce
         */
        // User must be logged in to view unpublished posts.
        if ( ! is_user_logged_in() ) {
            return $post;
        }

        // Check if the logged in user has permission to view the post.
        $status          = get_post_status( $post );
        $post_status_obj = get_post_status_object( $status );
        $edit_cap        = 'edit_post';
        $read_cap        = 'read_post';
        $allowed         = true;

        if ( $post_status_obj->protected ) {
            // User must have edit permissions on the draft to preview.
            if ( ! current_user_can( $edit_cap, $post->ID ) ) {
                $allowed = false;
            }
        } elseif ( $post_status_obj->private ) {
            if ( ! current_user_can( $read_cap, $post->ID ) ) {
                $allowed = false;
            }
        }

        if ( $allowed && current_user_can( $edit_cap, $post->ID ) ) {
            // Get preview if possible.
            return apply_filters( 'the_preview', $post );
        }

        return $post;
    }
    
	/**
	 * Get post by path.
	 *
	 * @param string $path (optional) The URL path.
	 * @return WP_Post|false Post object, false otherwise.
	 */
	public static function get_by_path( $path = '' ) {
		// Get post id from url, if no path found attempt getting homepage.
		$post_id = self::get_post_id_by_path($path);
		return self::get_post( $post_id );
	}

    /**
     * Get post by id.
     *
     * @param int $post_id
     * @return WP_Post|false Post object, false otherwise.
     */
    public static function get_by_id( $post_id ) {
        return self::get_post( $post_id );
    }

}
