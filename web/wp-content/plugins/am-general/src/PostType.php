<?php
/**
 * Post type base class.
 *
 * @package Am_General
 */

namespace Adrenalin;

/**
 * Class PostTypeBase.
 */
class PostType {

	/**
	 * The singular name.
	 *
	 * @var string
	 */
	public $name_single;

	/**
	 * The plural name.
	 *
	 * @var string
	 */
	public $name_plural;

	/**
	 * The slug.
	 *
	 * @var string
	 */
	public $slug;

	/**
	 * The menu icon.
	 *
	 * @var string
	 */
	public $menu_icon;

	/**
	 * Any additional values to replace defaults values.
	 *
	 * @var array
	 */
	public $args = [];

	/**
	 * Any additional values to replace defaults values.
	 *
	 * @param string $name The machine name of the post type.
	 */
	public function __construct( string $name ) {
		$this->slug        = $name;
		$this->name_single = ucfirst( $name );
		$this->name_plural = ucfirst( $name ) . 's';
	}

	/**
	 * Any additional values to replace defaults values.
	 */
	protected function get_labels() {
		$single = $this->name_single;
		$plural = $this->name_plural;

		return [
			'name'               => $plural,
			'singular_name'      => $single,
			'add_new'            => 'Add new',
			'add_new_item'       => "Add new $single",
			'edit_item'          => "Edit $single",
			'new_item'           => "New $single",
			'view_item'          => "New $plural",
			'search_items'       => "Search $plural",
			'not_found'          => 'Nothing found',
			'not_found_in_trash' => 'Nothing found in Trash',
			'parent_item_colon'  => '',
		];
	}

	/**
	 * Return values.
	 */
	protected function get_args() {
		$rewrite = [
			'slug'       => $this->slug,
			'with_front' => false,
		];

		$default = [
			'labels'                => $this->get_labels(),
			'public'                => true,
			'publicly_queryable'    => true,
			'exclude_from_search'   => false,
			'show_ui'               => true,
			'query_var'             => true,
			'rewrite'               => $rewrite,
			'capability_type'       => 'page', // 'page' vs 'post': Post is time based e.g newest to the top, whereas pages are more static.
			'menu_icon'             => $this->menu_icon,
			'has_archive'           => false,
			'hierarchical'          => true,
			'menu_position'         => null,
			'show_in_rest'          => true,
			'rest_base'             => $this->slug,
			'rest_controller_class' => 'WP_REST_Posts_Controller',
			'supports'              => [ 'title', 'thumbnail', 'author', 'page-attributes' ], // This is saying 'only use the Title field, remove content, featured images, etc'.
		];

		// Merge defaults with replacement values.
		return array_replace_recursive( $default, $this->args );
	}

	/**
	 * Registers a post type.
	 */
	public function register() {
		register_post_type( $this->name_plural, $this->get_args() );
		register_taxonomy_for_object_type( 'post_tag', sanitize_key($this->name_plural) );
	}

}
