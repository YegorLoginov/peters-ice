<?php
/**
 * Rest
 *
 * @package Am_General
 */

namespace Adrenalin;
use Adrenalin\fields\Fields;
use \WP_Rest_Yoast_Meta_Plugin\Frontend\Frontend;

/**
 * Class Rest.
 */
class Rest {

	/**
	 * The path to load and save JSON config.
	 *
	 * @var string
	 */
	protected $namespace = '/adrenalin';

	/**
	 * The name of the nonce.
	 *
	 * @var string
	 */
	public static $nonce_name = 'wp_rest';

	/**
	 * Register our routes.
	 */
	public function register_routes() {
		register_rest_route($this->namespace, '/menus', [
			'methods'  => 'GET',
			'callback' => [ $this, 'get_menus' ],
		]);

		register_rest_route($this->namespace, '/page', [
			'methods'  => 'GET',
			'callback' => [ $this, 'get_post_with_path' ],
		]);

		register_rest_route($this->namespace, '/forms', [
			'methods'  => 'GET',
			'callback' => [ $this, 'get_form' ],
		]);

		register_rest_route($this->namespace, '/files', [
			'methods'  => 'POST',
			'callback' => [ $this, 'post_files' ],
		]);

		register_rest_route($this->namespace, '/forms_submit', [
			'methods'  => 'POST',
			'callback' => [ $this, 'post_form' ],
		]);

		register_rest_route($this->namespace, '/cm_submit', [
			'methods'  => 'POST',
			'callback' => [ $this, 'post_cm_form' ],
		]);

	}

	/**
	 * Callback: Get HubSpot form by ID.
	 */
	public function get_form( $args ) {
		if ( $id = $args->get_param( 'id' ) ) {
			$hubspot = new Hubspot();
			return $hubspot->get_form_by_id( $id );
		}

		return false;
	}

	/**
	 * Callback: get menus data.
	 */
	public function get_menus() {
		return [
			'primary'   => wp_get_nav_menu_items( 'primary' ),
			'secondary' => wp_get_nav_menu_items( 'secondary' ),
			'footer'    => wp_get_nav_menu_items( 'footer' ),
		];
	}

	// Convert acf to array
	public function convert_acf_to_array( $postId ) {
	    $fields = get_fields( $postId );
        $fields = apply_filters( 'am_transform_acf_fields', $fields, $postId );
		return $fields;
	}

	/**
	 * Callback: get page data.
	 * Retrieves a page given its path with homepage fallback.
	 *
	 * @param string $arg Arguments.
	 */
	public function get_post_with_path( $arg ) {
		$path = $arg->get_param( 'path' );
		$post = Post::get_by_path( $path );
        if (empty($post) && current_user_can('edit_others_pages')) {
            $page_id = $arg->get_param('page_id');
            $post = Post::get_by_id($page_id);

            // transform data to preview post
            $post = _set_preview($post);            

            // get actual preview post to get acf field
            $preview_post = wp_get_post_autosave($page_id);
        }

        if (!$post) {
            return [];
        }

		// Extend base post object.
		$is_valid = true;
		switch ( $post->post_type ) {
			case 'revision':
				$is_valid = false;
				break;
			case 'nav_menu_item':
				break;
			case 'page':
				$permalink = get_page_link( $post->ID );
				break;
			case 'post':
				$permalink = get_permalink( $post->ID );
				break;
			case 'attachment':
				$permalink = get_attachment_link( $post->ID );
				break;
			default:
				$permalink = get_post_permalink( $post->ID );
				break;
		}

		$_post = $post->to_array();
        

        $response = $this->get_wp_response_by_post($post);
        $seo_data = CommonHelper::parse_document($response->data['yoast_head']);

//            $transient_key = 'yoast_meta_' . $post->ID;
//            $seo_data = get_transient( $transient_key );
//            if (empty($seo_data)) {
//                set_transient($transient_key, $seo_data, 5 * MINUTE_IN_SECONDS);
//            }

        $seo_data['title'] = html_entity_decode($seo_data['title']);
        $title = $seo_data['title'];


		$cache_key = 'breadcrumb-' . $post->ID;
		$breadcrumbs = wp_cache_get($cache_key);
		if ($breadcrumbs === FALSE) {
			foreach ($post->ancestors as $ancestor) {
				$breadcrumbs[] = ['id' => $ancestor, 'title' => get_the_title($ancestor), 'url' => get_permalink($ancestor)];
			}
			if (is_array($breadcrumbs)) {
				$breadcrumbs = array_reverse($breadcrumbs);
				wp_cache_set($cache_key, $breadcrumbs);
			}
		}

		$ancestors = $post->ancestors;
		$submenus = null;
		if ($ancestors) {
			$top_accestor = array_pop($ancestors);
			if ($top_accestor) {
				$top2_accestor = array_pop($ancestors);
				if ($top2_accestor) {
					$submenus['id'] = $top2_accestor;
					$submenus['title'] = get_the_title($top2_accestor);
					$submenus['url'] = get_page_link($top2_accestor);
					$submenus['children'] = $this->get_submenus($post->ID, $top2_accestor);
					$submenus['is_active'] = $post->ID == $top2_accestor;
				}
			}
		}

		if ( $is_valid ) {		    
			global $related;
			$response = array_merge( $_post, [
				'author_name' => get_the_author_meta( 'display_name', $post->post_author ),
				'url'         => $permalink,
				'is_front'    => (int) get_option( 'page_on_front' ) === $post->ID,
				'slug'        => str_replace( home_url(), '', $permalink ),
				'breadcrumbs' => $breadcrumbs,
				'submenus'    => $submenus,
				'seo'         => $seo_data ?? $seo_data,
				'acf'         => !empty($preview_post) ? $this->convert_acf_to_array( $preview_post->ID ) : $this->convert_acf_to_array( $post->ID ),
				'related'     => $related,
            ] );

            $response = apply_filters( 'adrenalin_rest_page_parse', $response );
            return $response;
		}

		return $_post;
	}

	/**
	 * Callback: Upload files.
	 */
	public function post_files( $file ) {
		$filesUpload = new FilesUpload( $file );

		if ( ! empty( $errors = $filesUpload->getUploadErrors() ) ) {
			return [ 'error' => $errors ];
		}

		// Check file type.
		if ( ! $filesUpload->isValidType() ) {
			return [ 'error' => 'Invalid file type' ];
		}

		// Check file size.
		if ( ! $filesUpload->isValidSize() ) {
			return [ 'error' => 'Maximum upload size is 2MB.' ];
		}

		$folder    = 'cv/';
		$file_name = $folder . $filesUpload->createRandomFileName();

		return AwsS3::upload( $filesUpload->file['tmp_name'], $file_name );
	}

	/**
	 * Callback: Post to HubSpot.
	 *
	 * @param  WP_REST_Request $request Full details about the request.
	 * @return WP_Error|boolean
	 */
	public function post_form( $request ) {
		$guid           = $request->get_param( 'guid' );
		$captchaInput   = $request->get_param( 'captcha' );
		$context        = $request->get_param( 'context' );
		$files          = $request->get_file_params();
		$form_data      = $request->get_params();

		// if ( ! defined( 'DEV_LOCAL' ) || DEV_LOCAL !== true ) {
		// 	$nonce_check = check_ajax_referer( self::$nonce_name, '_nonce', false );

		// 	if ( $nonce_check !== 1 ) {
		// 		return [ 'error' => 'Invalid token.' ];
		// 	}
		// }

		if ( empty( $guid ) ) {
			return [ 'error' => "guid was not sent." ];
		}

		if ( empty( $captchaInput ) ) {
			return [ 'error' => "Captcha was not sent." ];
		}

		// Validate captcha.
		$captcha          = new Captcha( __FUNCTION__ );
		$captcha_response = $captcha->verify( $captchaInput );

		if ( $captcha_response !== true ) {
			return [ 'error' => 'Captcha failed.' ];
		}

		// Get uploaded files.
		if ( $files ) {
			foreach ( $files as $name => $file ) {
				$uploaded_response = $this->post_files( $file );

				if ( is_array( $uploaded_response ) ) {
					// Error.
					return $uploaded_response;
				}

				$form_data[ $name ] = $uploaded_response;
			}
		}

		// Sanitise form data before sending it to Hubspot
		$form_data = array_map( 'strip_tags', $form_data );

		// Post to Hubspot.
		$hubspot = new Hubspot();

		return $hubspot->post_form( $guid, $form_data );
	}

	/**
	 * Callback: Post to Campaign Monitor.
	 *
	 * @param  WP_REST_Request $request Full details about the request.
	 * @return WP_Error|boolean
	 */
	public function post_cm_form($request){
		$form_data      = $request->get_params();
		$form_data = array_map( 'strip_tags', $form_data );
		$email = $form_data['email'];
		$name = $form_data['name'];

		if ($email && $name) {
			return CampaignMonitor::addSubscriber($email, $name);
		}
	}

	private function get_submenus($active, $parent = 0) {
		$pages = get_pages(['parent' => $parent]);
		$menus = [];

		foreach ($pages as $page) {
				$data = ['id' => $page->ID, 'title' => $page->post_title, 'url' => get_page_link( $page->ID )];
				$data['children'] = $this->get_submenus($active, $page->ID);
				$data['is_active'] = $active == $page->ID;
				$menus[] = $data;
		}
		return $menus;
	}

    private function get_wp_response_by_post($post) {
        global $wp_post_types;
        $wp_post_type = $wp_post_types[$post->post_type];
        if (empty($wp_post_type)) {
            throw new \Exception('Something wrong');
        }

        $url = '/wp/v2/' . $wp_post_type->rest_base . '/' . $post->ID;
        $request = new \WP_REST_Request('GET', $url);
        $response = rest_do_request($request);
        return $response;
    }
}
