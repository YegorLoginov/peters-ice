<?php
/**
 * Created by PhpStorm.
 * User: dion.tsai
 * Date: 3/03/2020
 * Time: 3:00 PM
 */

namespace Adrenalin\fields;

abstract class AcfTransformField
{
    public $acf_field_key;
    abstract function transform($field, $value, $postId);
}