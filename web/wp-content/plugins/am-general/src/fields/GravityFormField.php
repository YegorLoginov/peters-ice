<?php
/**
 * Created by PhpStorm.
 * User: dion.tsai
 * Date: 3/03/2020
 * Time: 3:00 PM
 */

namespace Adrenalin\fields;

use Adrenalin\CommonHelper;
use GFAPI;

class GravityFormField extends AcfTransformField
{
    public $acf_field_key = 'gravity_form';

    public function transform($field, $value, $postId)
    {
        $form = GFAPI::get_form($value);
        if (!empty($form)) {
            CommonHelper::keep_properties($form, ['id', 'fields', 'confirmations']);
            return $form;
        }

        return $value;
    }
}