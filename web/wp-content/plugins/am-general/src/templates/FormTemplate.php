<?php

namespace Adrenalin\templates;

use Adrenalin\CommonHelper;

/**
 * Created by PhpStorm.
 * User: dion.tsai
 * Date: 12/03/2020
 * Time: 4:21 PM
 */
class FormTemplate
{
    public function __construct()
    {
        add_filter('adrenalin_rest_page_parse', [$this, 'wp_add_additional_props_for_the_template'], 10, 1);
    }

    function wp_add_additional_props_for_the_template($response)
    {
        if ($response['page_template'] === 'templates/template-form-page.php') {
            $response['gform_upload_page_slug'] = get_option('gform_upload_page_slug');
            $response['gforms_captcha_public_key'] = get_option('rg_gforms_captcha_public_key');
        }
        return $response;
    }
}