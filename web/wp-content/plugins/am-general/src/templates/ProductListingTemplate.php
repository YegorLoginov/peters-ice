<?php

namespace Adrenalin\templates;

use Adrenalin\CommonHelper;

/**
 * Created by PhpStorm.
 * User: dion.tsai
 * Date: 12/03/2020
 * Time: 4:21 PM
 */
class ProductListingTemplate
{
    public function __construct()
    {
        add_filter('rest_products_query', [$this, 'wp_rest_brand_filters_for_product_post_type'], 10, 2);
        add_filter('adrenalin_rest_page_parse', [$this, 'wp_add_additional_props_for_the_template'], 10, 1);
    }

    // Rest customisation to support "brands" query string
    function wp_rest_brand_filters_for_product_post_type($args, $request)
    {
        $brand_ids = !empty($request['brands']) ? explode(',', $request['brands']) : null;
        if (!empty($brand_ids)) {
            $args['meta_query'] = [
                [
                    'key' => 'brand',
                    'value' => $brand_ids,
                ]
            ];
        }
        return $args;
    }

    function wp_add_additional_props_for_the_template($response)
    {
        if ($response['page_template'] === 'templates/template-product-listing.php') {
            $response['products_tags'] = $this->get_terms();
            $response['brands'] = $this->get_brand_data();
        }
        return $response;
    }

    private function get_brand_data()
    {
        $request = new \WP_REST_Request('GET', '/wp/v2/brands');
        $request->set_query_params([
                '_fields' => ['id', 'slug', 'title'],
                'per_page' => 100,
                'orderby' => 'title',
                'order' => 'asc'
            ]
        );
        $response = rest_do_request($request);
        $server = rest_get_server();
        $brands = $server->response_to_data($response, false);
        CommonHelper::keep_properties_for_array($brands, ['id', 'slug', 'title']);

        return $brands;
    }

    private function get_terms() {
        $terms = get_terms(['hide_empty' => false, 'orderby' => 'meta_value', 'meta_key' => 'order',]);
        foreach ($terms as $key => $term) {
            if ($term->name === 'Uncategorized') {
                unset($terms[$key]);
            }
            $term->name = html_entity_decode($term->name);
        }
        $terms = array_values($terms);
        return $terms;
    }
}