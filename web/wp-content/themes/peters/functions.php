<?php
/**
 * Peters Ice Cream functions and definitions
 */

__i('version.php');

// Add support
add_theme_support('post-thumbnails');
add_theme_support('menus');
add_theme_support( 'title-tag' );

// Remove generator meta tag from head
remove_action('wp_head', 'wp_generator');

function add_custom_scripts() {
	wp_enqueue_style( 'custom-styles', get_template_directory_uri() . '/static/css/main.'.CODE_VER.'.css' );
	wp_enqueue_script( 'custom-scripts', get_template_directory_uri() . '/static/js/main.'.CODE_VER.'.js', [], false, true );
}

add_action( 'wp_enqueue_scripts', 'add_custom_scripts' );

// Shorthand for including a file (once)
function __i($path)
{
	if (!empty($path)) {
		require_once get_template_directory() . '/' . $path;
	}
}

// Hooks into the MCE filters.
function adrenalin_add_mce_button_filters() {
	// check user permissions
	if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
		return;
	}
	// check if WYSIWYG is enabled.
	if ( 'true' == get_user_option( 'rich_editing' ) ) {
		add_filter( 'mce_buttons_2', 'add_tinymce_format_button' );
		add_filter( 'tiny_mce_before_init', 'add_bdi_text_styles_to_editor');
		add_filter( 'mce_buttons', 'add_the_table_button' );
		add_filter( 'mce_external_plugins', 'add_the_table_plugin' );
	}
}
add_action('admin_head', 'adrenalin_add_mce_button_filters');

function add_the_table_button( $buttons ) {
   array_push( $buttons, 'separator', 'table' );
   return $buttons;
}

function add_the_table_plugin( $plugins ) {
	$plugins['table'] = get_template_directory_uri() . '/mce/table/plugin.min.js';
	return $plugins;
}


add_filter('acf/rest_api/option/get_fields', array( 'Adrenalin\CampaignMonitor', 'fileterRestAPI'));

function add_bdi_colors_to_editor($init) {

	$custom_colours = '
		"000000", "Black",
		"ffffff", "White",
		"f43100", "Primary Orange",
		"476c7a", "Secondary Blue",
		"ffc72c", "Secondary Yellow",
	';

	$init['textcolor_map'] = '['.$custom_colours.']';

	// change the number of rows in the grid if the number of colors changes
	// 8 swatches per row
	$init['textcolor_rows'] = 1;

	return $init;
}

// Add the 'Format' button to tinymce
function add_tinymce_format_button( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}

// Add editor stylesheet
function wpdocs_theme_add_editor_styles() {
	add_editor_style(get_template_directory_uri() . '/static/css/editor.css');
}
add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );


// Add various text styles - CTA's, Buttons, etc.
function add_bdi_text_styles_to_editor($init) {
	// Define the style_formats array
	$style_formats = array_merge([], array(
		// Each array child is a format with it's own settings
		array(
			'title' => 'CTA',
			'selector' => 'a',
			'classes' => 'cta'
		),
		array(
			'title' => 'Primary Button',
			'selector' => 'a',
			'classes' => 'button button--primary'
		),
		array(
			'title' => 'Secondary Button',
			'selector' => 'a',
			'classes' => 'button button--secondary'
		),
		array(
			'title' => 'Disclaimer',
			'selector' => 'p',
			'classes' => 'disclaimer'
		),
	));
	
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init['style_formats'] = json_encode( $style_formats );
	return $init;
}

// Enable Options Page for ACF
if (function_exists('acf_add_options_page')) {
	acf_add_options_page();
	acf_add_options_sub_page('Global');
}

// remove emoji script
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'index_rel_link' ); // index link
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);
remove_action('template_redirect', 'rest_output_link_header', 11, 0);
remove_action('wp_head', 'rest_output_link_wp_head', 10);