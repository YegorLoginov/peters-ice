<!DOCTYPE html>
<html lang="en">

	<head>
		<base href="/"/>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
		<link rel="stylesheet" href="https://use.typekit.net/qgg0sln.css">


		<?php wp_head(); ?>

		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/meta/favicon.ico">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/meta/favicon-16x16.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/meta/favicon-32x32.png">
<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/meta/manifest.json">
<meta name="mobile-web-app-capable" content="yes">
<meta name="theme-color" content="#f43100">
<meta name="application-name" content="Peters Ice Cream">
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/meta/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/meta/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/meta/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/meta/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/meta/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/meta/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/meta/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/meta/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="167x167" href="<?php echo get_template_directory_uri(); ?>/meta/apple-touch-icon-167x167.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/meta/apple-touch-icon-180x180.png">
<link rel="apple-touch-icon" sizes="1024x1024" href="<?php echo get_template_directory_uri(); ?>/meta/apple-touch-icon-1024x1024.png">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="apple-mobile-web-app-title" content="Peters Ice Cream">
<link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 1)" href="<?php echo get_template_directory_uri(); ?>/meta/apple-touch-startup-image-320x460.png">
<link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/meta/apple-touch-startup-image-640x920.png">
<link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/meta/apple-touch-startup-image-640x1096.png">
<link rel="apple-touch-startup-image" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/meta/apple-touch-startup-image-750x1294.png">
<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 3)" href="<?php echo get_template_directory_uri(); ?>/meta/apple-touch-startup-image-1182x2208.png">
<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 3)" href="<?php echo get_template_directory_uri(); ?>/meta/apple-touch-startup-image-1242x2148.png">
<link rel="apple-touch-startup-image" media="(device-width: 768px) and (device-height: 1024px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 1)" href="<?php echo get_template_directory_uri(); ?>/meta/apple-touch-startup-image-748x1024.png">
<link rel="apple-touch-startup-image" media="(device-width: 768px) and (device-height: 1024px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 1)" href="<?php echo get_template_directory_uri(); ?>/meta/apple-touch-startup-image-768x1004.png">
<link rel="apple-touch-startup-image" media="(device-width: 768px) and (device-height: 1024px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/meta/apple-touch-startup-image-1496x2048.png">
<link rel="apple-touch-startup-image" media="(device-width: 768px) and (device-height: 1024px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/meta/apple-touch-startup-image-1536x2008.png">
<link rel="icon" type="image/png" sizes="228x228" href="<?php echo get_template_directory_uri(); ?>/meta/coast-228x228.png">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/meta/mstile-144x144.png">
<meta name="msapplication-config" content="<?php echo get_template_directory_uri(); ?>/meta/browserconfig.xml">
<link rel="yandex-tableau-widget" href="<?php echo get_template_directory_uri(); ?>/meta/yandex-browser-manifest.json">

		<!-- Google Tag Manager -->
		<script>
			(function (w, d, s, l, i) {
w[l] = w[l] || [];
w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'});
var f = d.getElementsByTagName(s)[0],
j = d.createElement(s),
dl = l != 'dataLayer' ? '&l=' + l : '';
j.async = true;
j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', '<?php echo get_field("google_tag_manager_id", "option"); ?>');
		</script>
		<!-- End Google Tag Manager -->

		<?php if (is_preview()) { ?>
			<script>
				var rest_nonce = '<?php echo wp_create_nonce('wp_rest') ?>';
			</script>
		<?php } ?>
	</head>

	<body>

			<div id="root"></div>

		<?php wp_footer(); ?>
	</body>

</html>
